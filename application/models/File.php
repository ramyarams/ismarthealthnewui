<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class File extends CI_Model{
    function __construct() {
        $this->tableName = 'files';
    }
    
    /*
     * Fetch files data from the database
     * @param id returns a single record if specified, otherwise all records
     */
    public function getRows($id = ''){
        $this->db->select('id,file_name,uploaded_on');
        $this->db->from('files');
        if($id){
            $this->db->where('id',$id);
            $query = $this->db->get();
            $result = $query->row_array();
        }else{
            $this->db->order_by('uploaded_on','desc');
            $query = $this->db->get();
            $result = $query->result_array();
        }
        return !empty($result)?$result:false;
    }


        function update_filestatus($rptd_id=null,$status=null)
{
  date_default_timezone_set('Asia/Kolkata'); # add your city to set local time zone
  $now = date('Y-m-d H:i:s');
  $this->db->query("UPDATE referral_patient_test_details SET labapproval_datetime='".$now."',labapproval_id='".$status."' WHERE ID='".$rptd_id."'");
  if ($this->db->affected_rows() >0)
    return true;
  else
    return false;
}

 public function insertreportfiles($data = array(),$referral_id){
        $insert = $this->db->insert_batch('files',$data);
        if($insert){
        $this->db->set('labapproval_id', 1); 
        $this->db->where('ID', $referral_id);
        $this->db->update('referral_patient_test_details'); 
        }
        return $insert?true:false;
    }

    
    /*
     * Insert file data into the database
     * @param array the data for inserting into the table
     */
    public function insert($data = array()){
        $insert = $this->db->insert_batch('files',$data);
        $inserted_num_rows = $this->db->affected_rows();
          date_default_timezone_set('Asia/kolkata');

          if($inserted_num_rows>0){
                $this->db->set('sample_collected_id', 2);
                $this->db->set('sample_datetime','NOW()', FALSE);
                $this->db->where('ID', $data[0]['referral_id']);
                $this->db->update('referral_patient_test_details');
                
                $status=2;
                $this->db->set('pat_status', $status); 
                $this->db->where('ID',$data[0]['referral_id']);
                $this->db->update('referral_patient_test_details');
          }

        return $insert?true:false;
    }

    function insert_samples($sample_value,$referral_id)
    {
    $data = array(
        'sample_value'=>$sample_value,
        'referral_id'=>$referral_id
    );

    $this->db->insert('sample_results',$data);
}

    //     public function getImages($med_num = ''){
    //         // $medical_num='sr921810220207';
    //     $this->db->select('*');
    //     $this->db->from('files');
    //     $this->db->join('referral_patient_test_details', 'files.referral_id=referral_patient_test_details.ID', 'right'); 
    //     $this->db->join('investigation_test_details', 'investigation_test_details.parse_id=referral_patient_test_details.laboratory_tests'); 
    //     if($med_num){
    //         $this->db->where('files.medical_num',$med_num);
    //         $this->db->group_by('files.referral_id'); 
    //         $query = $this->db->get();
    //         $result = $query->row_array();
    //     }else{
    //         $this->db->order_by('files.uploaded_on','desc');
    //         $this->db->group_by('files.referral_id'); 
    //         $query = $this->db->get();
    //         $result = $query->result_array();
    //     }
    //     return !empty($result)?$result:false;
    // }
    
    function getImages($med_num=null,$referral_id=null){
  $this->db->select('*');
  $this->db->from('files');
  $this->db->join('referral_patient_test_details', 'files.referral_id=referral_patient_test_details.ID', 'right'); 
  $this->db->join('investigation_test_details', 'investigation_test_details.parse_id=referral_patient_test_details.laboratory_tests'); 
   $this->db->where('files.medical_num',$med_num);
   $this->db->where('files.referral_id',$referral_id);
   $this->db->where('files.status',1);
  $query = $this->db->get();
return $query->result();
}
}