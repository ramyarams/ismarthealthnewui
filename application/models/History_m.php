<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
class History_m extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

        function get_referred_specialist()
	{
		$this->db->trans_start();
    $query = $this->db->query("SELECT DISTINCT pa.firstname,ssd.to_assign FROM superspeciality_details ssd INNER JOIN physician_appointment pa ON pa.physician_id=ssd.to_assign WHERE ssd.by_assign='".$_SESSION['physician_id']."'");
    $this->db->trans_complete();
    if($query->num_rows()>=1)
      return $query->result();
	}


	function get_referred_lab()
	{
		$this->db->trans_start();
    $query = $this->db->query("SELECT DISTINCT rptd.laboratory_id,ld.laboratory_name FROM referral_patient_test_details rptd INNER JOIN laboratory_details ld ON ld.laboratory_id=rptd.laboratory_id INNER JOIN physician_appointment pa ON pa.physician_id=rptd.physician_id WHERE pa.mail_id='".$_SESSION['mail_id']."'");
    $this->db->trans_complete();
    if($query->num_rows()>=1)
      return $query->result();

	}


	// function search($date1,$date2,$lab)
	// {
	// 	$phy_id=$_SESSION['physician_id'];
	// 	if($lab=='lab' || $lab=='selectlab')
	// 	{
	// 		$this->db->trans_start();
 //    $query = $this->db->query("(SELECT rptd.`medical_num`,rpd.firstname,ld.laboratory_name,rcd.refer_date,GROUP_CONCAT(DISTINCT rptd.`laboratory_tests`) lab_test FROM `referral_patient_test_details` rptd INNER JOIN referral_patient_details rpd ON rpd.patient_unique_id=rptd.patient_unique_id INNER JOIN laboratory_details ld ON ld.laboratory_id=rptd.`laboratory_id` INNER JOIN referral_confirmation_details rcd ON rcd.medical_num=rptd.medical_num WHERE rptd.date>='".$date1."' AND rptd.date<='".$date2."' AND rptd.physician_id = '".$phy_id."' GROUP BY medical_num) UNION (SELECT rptd.`medical_num`,pdd.firstname,ld.laboratory_name,rcd.refer_date,GROUP_CONCAT(DISTINCT rptd.`laboratory_tests`) lab_test FROM `referral_patient_test_details` rptd INNER JOIN patient_dep_details pdd ON pdd.patient_unique_id=rptd.patient_unique_id INNER JOIN laboratory_details ld ON ld.laboratory_id=rptd.`laboratory_id` INNER JOIN referral_confirmation_details rcd ON rcd.medical_num=rptd.medical_num WHERE rptd.date>='".$date1."' AND rptd.date<='".$date2."' AND rptd.physician_id = '".$phy_id."' GROUP BY medical_num)");
 //    $this->db->trans_complete();
 //    if($query->num_rows()>=1)
 //      return $query->result();
	// 	}
	// 	else
	// 	{
	// 		$this->db->trans_start();
 //    $query = $this->db->query("(SELECT rptd.`medical_num`,rpd.firstname,ld.laboratory_name,rcd.refer_date,GROUP_CONCAT(DISTINCT rptd.`laboratory_tests`) lab_test FROM `referral_patient_test_details` rptd INNER JOIN referral_patient_details rpd ON rpd.patient_unique_id=rptd.patient_unique_id INNER JOIN laboratory_details ld ON ld.laboratory_id=rptd.`laboratory_id` INNER JOIN referral_confirmation_details rcd ON rcd.medical_num=rptd.medical_num WHERE rptd.date>='".$date1."' AND rptd.date<='".$date2."' AND rptd.physician_id = '".$phy_id."' AND rptd.laboratory_id='".$lab."'GROUP BY medical_num) UNION (SELECT rptd.`medical_num`,pdd.firstname,ld.laboratory_name,rcd.refer_date,GROUP_CONCAT(DISTINCT rptd.`laboratory_tests`) lab_test FROM `referral_patient_test_details` rptd INNER JOIN patient_dep_details pdd ON pdd.patient_unique_id=rptd.patient_unique_id INNER JOIN laboratory_details ld ON ld.laboratory_id=rptd.`laboratory_id` INNER JOIN referral_confirmation_details rcd ON rcd.medical_num=rptd.medical_num WHERE rptd.date>='".$date1."' AND rptd.date<='".$date2."' AND rptd.physician_id = '".$phy_id."' AND rptd.laboratory_id='".$lab."'GROUP BY medical_num)");
 //    $this->db->trans_complete();
 //    if($query->num_rows()>=1)
 //      return $query->result();
	// 	}

	// }

	// function search($date1,$date2,$lab)
	// {
	// 	$phy_id=$_SESSION['physician_id'];
	// 	if($lab=='lab' || $lab=='selectlab')
	// 	{
	// 		$this->db->trans_start();
 //    $query = $this->db->query("(SELECT rptd.`medical_num`,rpd.firstname,ld.laboratory_name,rcd.refer_date,GROUP_CONCAT(DISTINCT rptd.`laboratory_tests`) lab_test FROM `referral_patient_test_details` rptd INNER JOIN referral_patient_details rpd ON rpd.patient_unique_id=rptd.patient_unique_id INNER JOIN laboratory_details ld ON ld.laboratory_id=rptd.`laboratory_id` INNER JOIN referral_confirmation_details rcd ON rcd.medical_num=rptd.medical_num WHERE rptd.date>='".$date1."' AND rptd.date<='".$date2."' AND rptd.physician_id = '".$phy_id."' GROUP BY medical_num) UNION (SELECT rptd.`medical_num`,pdd.firstname,ld.laboratory_name,rcd.refer_date,GROUP_CONCAT(DISTINCT rptd.`laboratory_tests`) lab_test FROM `referral_patient_test_details` rptd INNER JOIN patient_dep_details pdd ON pdd.patient_unique_id=rptd.patient_unique_id INNER JOIN laboratory_details ld ON ld.laboratory_id=rptd.`laboratory_id` INNER JOIN referral_confirmation_details rcd ON rcd.medical_num=rptd.medical_num WHERE rptd.date>='".$date1."' AND rptd.date<='".$date2."' AND rptd.physician_id = '".$phy_id."' GROUP BY medical_num)");
 //    $this->db->trans_complete();
 //    if($query->num_rows()>=1)
 //      return $query->result();
	// 	}
	// 	else
	// 	{
	// 		$this->db->trans_start();
 //    $query = $this->db->query("(SELECT rptd.`medical_num`,rpd.firstname,ld.laboratory_name,rcd.refer_date,GROUP_CONCAT(DISTINCT rptd.`laboratory_tests`) lab_test FROM `referral_patient_test_details` rptd INNER JOIN referral_patient_details rpd ON rpd.patient_unique_id=rptd.patient_unique_id INNER JOIN laboratory_details ld ON ld.laboratory_id=rptd.`laboratory_id` INNER JOIN referral_confirmation_details rcd ON rcd.medical_num=rptd.medical_num WHERE rptd.date>='".$date1."' AND rptd.date<='".$date2."' AND rptd.physician_id = '".$phy_id."' AND rptd.laboratory_id='".$lab."'GROUP BY medical_num) UNION (SELECT rptd.`medical_num`,pdd.firstname,ld.laboratory_name,rcd.refer_date,GROUP_CONCAT(DISTINCT rptd.`laboratory_tests`) lab_test FROM `referral_patient_test_details` rptd INNER JOIN patient_dep_details pdd ON pdd.patient_unique_id=rptd.patient_unique_id INNER JOIN laboratory_details ld ON ld.laboratory_id=rptd.`laboratory_id` INNER JOIN referral_confirmation_details rcd ON rcd.medical_num=rptd.medical_num WHERE rptd.date>='".$date1."' AND rptd.date<='".$date2."' AND rptd.physician_id = '".$phy_id."' AND rptd.laboratory_id='".$lab."'GROUP BY medical_num)");
 //    $this->db->trans_complete();
 //    if($query->num_rows()>=1)
 //      return $query->result();
	// 	}

	// }

	// 	function search($date1,$date2,$lab)
	// {
	// 	$phy_id=$_SESSION['physician_id'];
	// 	if($lab=='lab' || $lab=='selectlab')
	// 	{
	// 		$this->db->trans_start();
 //    $query = $this->db->query("(SELECT rptd.`medical_num`,rpd.firstname,ld.laboratory_name,rcd.refer_date,GROUP_CONCAT(DISTINCT rptd.`laboratory_tests`) lab_test FROM `referral_patient_test_details` rptd INNER JOIN referral_patient_details rpd ON rpd.patient_unique_id=rptd.patient_unique_id INNER JOIN laboratory_details ld ON ld.laboratory_id=rptd.`laboratory_id` INNER JOIN referral_confirmation_details rcd ON rcd.medical_num=rptd.medical_num WHERE rptd.date>='".$date1."' AND rptd.date<='".$date2."' AND rptd.physician_id = '".$phy_id."' GROUP BY medical_num) UNION (SELECT rptd.`medical_num`,pdd.firstname,ld.laboratory_name,rcd.refer_date,GROUP_CONCAT(DISTINCT rptd.`laboratory_tests`) lab_test FROM `referral_patient_test_details` rptd INNER JOIN patient_dep_details pdd ON pdd.patient_unique_id=rptd.patient_unique_id INNER JOIN laboratory_details ld ON ld.laboratory_id=rptd.`laboratory_id` INNER JOIN referral_confirmation_details rcd ON rcd.medical_num=rptd.medical_num WHERE rptd.date>='".$date1."' AND rptd.date<='".$date2."' AND rptd.physician_id = '".$phy_id."' GROUP BY medical_num)");
 //    $this->db->trans_complete();
 //    if($query->num_rows()>=1)
 //      return $query->result();
	// 	}
	// 	else
	// 	{
	// 		$this->db->trans_start();
 //    $query = $this->db->query("(SELECT rptd.`medical_num`,rpd.firstname,ld.laboratory_name,rcd.refer_date,GROUP_CONCAT(DISTINCT rptd.`laboratory_tests`) lab_test FROM `referral_patient_test_details` rptd INNER JOIN referral_patient_details rpd ON rpd.patient_unique_id=rptd.patient_unique_id INNER JOIN laboratory_details ld ON ld.laboratory_id=rptd.`laboratory_id` INNER JOIN referral_confirmation_details rcd ON rcd.medical_num=rptd.medical_num WHERE rcd.refer_date>='".$date1."' AND rcd.refer_date<='".$date2."' AND rptd.physician_id = '".$phy_id."' AND rptd.laboratory_id='".$lab."'GROUP BY medical_num) UNION (SELECT rptd.`medical_num`,pdd.firstname,ld.laboratory_name,rcd.refer_date,GROUP_CONCAT(DISTINCT rptd.`laboratory_tests`) lab_test FROM `referral_patient_test_details` rptd INNER JOIN patient_dep_details pdd ON pdd.patient_unique_id=rptd.patient_unique_id INNER JOIN laboratory_details ld ON ld.laboratory_id=rptd.`laboratory_id` INNER JOIN referral_confirmation_details rcd ON rcd.medical_num=rptd.medical_num WHERE rcd.refer_date>='".$date1."' AND rcd.refer_date<='".$date2."' AND rptd.physician_id = '".$phy_id."' AND rptd.laboratory_id='".$lab."'GROUP BY medical_num)");
 //    $this->db->trans_complete();
 //    if($query->num_rows()>=1)
 //      return $query->result();
	// 	}

	// }

	function search($date1,$date2,$lab)
	{
		$phy_id=$_SESSION['physician_id'];
		if($lab=='lab' || $lab=='selectlab')
		{
			$this->db->trans_start();
    $query = $this->db->query("(SELECT rptd.`medical_num`,rpd.firstname,ld.laboratory_name,rcd.refer_date,GROUP_CONCAT(DISTINCT itd.test_name) lab_test FROM `referral_patient_test_details` rptd INNER JOIN referral_patient_details rpd ON rpd.patient_unique_id=rptd.patient_unique_id INNER JOIN laboratory_details ld ON ld.laboratory_id=rptd.`laboratory_id` INNER JOIN referral_confirmation_details rcd ON rcd.medical_num=rptd.medical_num INNER JOIN investigation_test_details itd ON itd.parse_id=rptd.laboratory_tests WHERE rptd.date>='".$date1."' AND rptd.date<='".$date2."' AND rptd.physician_id = '".$phy_id."' GROUP BY medical_num) UNION (SELECT rptd.`medical_num`,pdd.firstname,ld.laboratory_name,rcd.refer_date,GROUP_CONCAT(DISTINCT rptd.`laboratory_tests`) lab_test FROM `referral_patient_test_details` rptd INNER JOIN patient_dep_details pdd ON pdd.patient_unique_id=rptd.patient_unique_id INNER JOIN laboratory_details ld ON ld.laboratory_id=rptd.`laboratory_id` INNER JOIN referral_confirmation_details rcd ON rcd.medical_num=rptd.medical_num INNER JOIN investigation_test_details itd ON itd.parse_id=rptd.laboratory_tests WHERE rptd.date>='".$date1."' AND rptd.date<='".$date2."' AND rptd.physician_id = '".$phy_id."' GROUP BY medical_num)");
    $this->db->trans_complete();
    if($query->num_rows()>=1)
      return $query->result();
		}
		else
		{
			$this->db->trans_start();
    $query = $this->db->query("(SELECT rptd.`medical_num`,rpd.firstname,ld.laboratory_name,rcd.refer_date,GROUP_CONCAT(DISTINCT itd.test_name) lab_test FROM `referral_patient_test_details` rptd INNER JOIN referral_patient_details rpd ON rpd.patient_unique_id=rptd.patient_unique_id INNER JOIN laboratory_details ld ON ld.laboratory_id=rptd.`laboratory_id` INNER JOIN referral_confirmation_details rcd ON rcd.medical_num=rptd.medical_num INNER JOIN investigation_test_details itd ON itd.parse_id=rptd.laboratory_tests WHERE rcd.refer_date>='".$date1."' AND rcd.refer_date<='".$date2."' AND rptd.physician_id = '".$phy_id."' AND rptd.laboratory_id='".$lab."'GROUP BY medical_num) UNION (SELECT rptd.`medical_num`,pdd.firstname,ld.laboratory_name,rcd.refer_date,GROUP_CONCAT(DISTINCT rptd.`laboratory_tests`) lab_test FROM `referral_patient_test_details` rptd INNER JOIN patient_dep_details pdd ON pdd.patient_unique_id=rptd.patient_unique_id INNER JOIN laboratory_details ld ON ld.laboratory_id=rptd.`laboratory_id` INNER JOIN referral_confirmation_details rcd ON rcd.medical_num=rptd.medical_num INNER JOIN investigation_test_details itd ON itd.parse_id=rptd.laboratory_tests WHERE rcd.refer_date>='".$date1."' AND rcd.refer_date<='".$date2."' AND rptd.physician_id = '".$phy_id."' AND rptd.laboratory_id='".$lab."'GROUP BY medical_num)");
    $this->db->trans_complete();
    if($query->num_rows()>=1)
      return $query->result();
		}

	}


}

