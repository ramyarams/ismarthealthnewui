<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(0);
class Physician_model extends CI_Model {

public function __construct()
    {
        parent::__construct();
    }

    public function get_timing_of_physician(){ 
        $user_id = $this->session->userdata('physician_id');
        $query=$this->db->query("SELECT * from physician_timings WHERE physician_id = '".$user_id."' order by id ASC ");
        return $query->result_array();
    }

    public function get_timing_of_physician_new(){ 
        $user_id = $this->session->userdata('physician_id');
        $query=$this->db->query("SELECT * from physician_clinic_timings WHERE physician_id = '".$user_id."' order by id ASC ");
        return $query->result_array();
    }

    

    public function get_physician_appointment()
    {
    	$user_id = $this->session->userdata('physician_id');
        $query=$this->db->query("SELECT * from physician_appointment WHERE physician_id = '".$user_id."' order by physician_id ASC ");
        return $query->result_array();
    }




}