<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
class Patient_m extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

 function report_date($medical_num=null){
   $this->db->trans_start();
  $query = $this->db->query("(SELECT MAX(labapproval_datetime) as report_date FROM `referral_patient_test_details` WHERE medical_num='".$medical_num."')");
 $this->db->trans_complete();
    if($query->num_rows()>=1)
      return $query->result();
  }
 function clinicalparameter($blood_pressure=null,$sugar=null,$pulse=null,$weight=null,$temperature=null,$referral_patient_id=null,$dependent_id=null){

   $this->db->query("INSERT INTO `clinical_parameters`(`blood_pressure`,`sugar`,`pulse`,`weight`,`temperature`,`referral_patient_id`,`dependent_id`,`created_on`) VALUES('".$blood_pressure."','".$sugar."','".$pulse."','".$weight."','".$temperature."','".$referral_patient_id."','".$dependent_id."','".$now."')");
    $this->db->trans_complete();
    if ($this->db->trans_status() === FALSE)
    {
      return null;
    }
    else
    {
      return true;
    }
 }

  function patientparameter($allergy=null,$medical_history=null,$bloodgroup=null,$diabetic=null,$hypertension=null,$referral_patient_id=null,$dependent_id=null){

  $this->db->from('patient_parameter');
  $this->db->where('referral_patient_id',$referral_patient_id);
  $this->db->where('dependent_id',$dependent_id);
  $query = $this->db->get();
 if($query->num_rows()>0)
 {

      $data = array(
            'known_allergies' => $allergy,
            'previous_medical_history' => $medical_history,
            'blood_group'=> $bloodgroup,
            'diabetic'=> $diabetic,
            'hypertension'=> $hypertension
         );
  
      $this->db->where("referral_patient_id",$referral_patient_id);
  $this->db->where('dependent_id',$dependent_id);
      
      $this->db->update("patient_parameter",$data);
       if($this->db->affected_rows() >=0){
 

      return "update"; 
  }else{
    return false; 
  }

 } else{
   $this->db->query("INSERT INTO `patient_parameter`(`known_allergies`,`previous_medical_history`,`blood_group`,`diabetic`,`hypertension`,`referral_patient_id`,`dependent_id`,`created_on`) VALUES('".$allergy."','".$medical_history."','".$bloodgroup."','".$diabetic."','".$hypertension."','".$referral_patient_id."','".$dependent_id."','".$now."')");
    $this->db->trans_complete();
    if ($this->db->trans_status() === FALSE)
    {
      return null;
    }
    else
    {
       return "insert";
    }
 }
 }
function get_bloodgroupdetails(){
  $this->db->select("bloodgroup_id,bloodgroup_name");
  $this->db->from('bloodgroup_details');
  $this->db->where('active',1);
  $query = $this->db->get();
  return $query->result();
}

// function pui_patientdetails($patient_unique_id=null){
//   $this->db->select("referral_patient_id");
//   $this->db->from('referral_patient_details');
//   $this->db->where('patient_unique_id',$patient_unique_id);
//   $query = $this->db->get();
// return $query->result();
// }

function pui_patientdetails($patient_unique_id=null){
  // $this->db->select("referral_patient_id");
  // $this->db->from('referral_patient_details');
  // $this->db->where('patient_unique_id',$patient_unique_id);
  // $query = $this->db->get();
  $this->db->trans_start();
  $query = $this->db->query("(SELECT referral_patient_id FROM referral_patient_details rpd WHERE rpd.patient_unique_id='".$patient_unique_id."') UNION (SELECT patient_dep_id FROM patient_dep_details pdd WHERE pdd.patient_unique_id='".$patient_unique_id."')");
 $this->db->trans_complete();
    if($query->num_rows()>=1)
      return $query->result();
}

function get_patientdetails($patient_id=null){
    $this->db->select("referral_patient_details.*,physician_appointment.firstname AS phyfname,physician_appointment.lastname AS phylname,state.*,city.*");
  $this->db->from('referral_patient_details'); 
  $this->db->join('state', 'state.state_id = referral_patient_details.state','left');
  $this->db->join('city', 'city.city_id = referral_patient_details.city','left');
  $this->db->join('physician_appointment', 'physician_appointment.physician_id = referral_patient_details.physician_id','left');
  $this->db->where('referral_patient_id',$patient_id);
  $query = $this->db->get();
return $query->result();
}

 function fetch_patient_records($phone_num,$referral_patient_id,$dependent_id){

  $this->db->trans_start();
    $firstquery=$this->db->query("SELECT c.city_name,p.date_of_birth,p.firstname, p.lastname, p.gender, p.age, p.address, p.city, p.pincode, p.mailid, p.phonenum,pp.known_allergies,pp.previous_medical_history,pp.diabetic,pp.hypertension,bgd.bloodgroup_name FROM patient_parameter pp INNER JOIN bloodgroup_details bgd ON bgd.bloodgroup_id=pp.blood_group AND pp.dependent_id=0 RIGHT JOIN referral_patient_details p ON pp.referral_patient_id = p.referral_patient_id LEFT JOIN city c ON c.city_id=p.city WHERE p.phonenum ='".$phone_num."'"); 
    $json= $firstquery->result();

     
     $secondquery = $this->db->query("SELECT * FROM `clinical_parameters` WHERE referral_patient_id= '".$referral_patient_id."' AND dependent_id= '".$dependent_id."' ORDER BY created_on DESC LIMIT 10"); 
     $json2 = array();
     foreach($secondquery->result_array() as $row){
       $json2[] = array( 
        'blood_pressure' => $row['blood_pressure'],
        'sugar' => $row['sugar'],
        'pulse' => $row['pulse'],
        'weight' => $row['weight'],
        'temperature' => $row['temperature'],
        'created_on' => $row['created_on']
    );
}

   $this->db->trans_complete();
     $json['parameters'] = $json2;
$date_of_birth=$json[0]->date_of_birth;
    $dateOfBirth = new DateTime($date_of_birth);
    $today = new DateTime();
    $diff = $today->diff($dateOfBirth);
    $diffString=$diff->format("%Y Years, %M Months");
    $json['age']=$diffString;
     echo json_encode($json);

  }


   function fetch_dependent_records($phone_num,$referral_patient_id,$dependent_id){

  $this->db->trans_start();
     $firstquery= $this->db->query("SELECT c.city_name,p.patient_dep_id, p.firstname, p.lastname, p.gender, p.age, p.address, p.city, p.pincode, p.mailid, p.phonenum, p.main_pat_id, p.created_on, p.updated_on, p.active, p.role_id, p.patient_unique_id, p.physician_id,relationship.rel_name,p.alt_phonenum,p.date_of_birth,pp.known_allergies,pp.previous_medical_history,pp.diabetic,pp.hypertension,bgd.bloodgroup_name FROM patient_dep_details p INNER JOIN relationship ON relationship.rel_id = p.relationship LEFT JOIN referral_patient_details ON p.main_pat_id = referral_patient_details.referral_patient_id LEFT JOIN city c ON c.city_id=p.city LEFT JOIN patient_parameter pp ON pp.dependent_id=p.patient_dep_id LEFT JOIN bloodgroup_details bgd ON bgd.bloodgroup_id=pp.blood_group  WHERE p.main_pat_id = '".$referral_patient_id."' AND p.patient_dep_id='".$dependent_id."'");
      $json= $firstquery->result();

     
     $secondquery = $this->db->query("SELECT * FROM `clinical_parameters` WHERE referral_patient_id= '".$referral_patient_id."' AND dependent_id= '".$dependent_id."' ORDER BY created_on DESC LIMIT 10"); 
     $json2 = array();
     foreach($secondquery->result_array() as $row){
       $json2[] = array( 
        'blood_pressure' => $row['blood_pressure'],
        'sugar' => $row['sugar'],
        'pulse' => $row['pulse'],
        'weight' => $row['weight'],
        'temperature' => $row['temperature'],
        'created_on' => $row['created_on']
    );
}

   $this->db->trans_complete();
     $json['parameters'] = $json2;
      $date_of_birth=$json[0]->date_of_birth;
    $dateOfBirth = new DateTime($date_of_birth);
    $today = new DateTime();
    $diff = $today->diff($dateOfBirth);
    $diffString=$diff->format("%Y Years, %M Months");
    $json['age']=$diffString;
     echo json_encode($json);

  }


function fetch_parameter_records($referral_patient_id,$dependent_id){

  $this->db->trans_start();
    $query = $this->db->query("SELECT pp.date_of_birth,pp.known_allergies,pp.previous_medical_history,pp.diabetic,pp.hypertension,pp.blood_group,bgd.bloodgroup_name FROM patient_parameter pp INNER JOIN bloodgroup_details bgd ON bgd.bloodgroup_id=pp.blood_group WHERE pp.referral_patient_id ='".$referral_patient_id."' AND pp.dependent_id='".$dependent_id."'"); 
   $this->db->trans_complete();
    if($query->num_rows()>=1)
      return $query->result();

  }


function update_patientdetails($patient_firstname,$patient_lastname,$patient_gender,$patient_age,$patient_address,$city,$pincode,$phonenum){
$ref_pat_id=$_SESSION['patient_id'];

			$data = array(
		        'firstname' => $patient_firstname,
		        'lastname' => $patient_lastname,
		        'gender' => $patient_gender,
		        'age'=> $patient_age,
		        'address'=> $patient_address,
		        'city'=> $city,
		        'pincode'=> $pincode,
		        'phonenum'=> $phonenum

		     );
	
			$this->db->where("referral_patient_id",$ref_pat_id);
			$this->db->update("referral_patient_details",$data);

			if($this->db->affected_rows() >=0){
  return true; 
}else{
  return false; 
}
}

function get_patient_details($phone_num=null){

  
$this->db->trans_start();
$query = $this->db->query("(SELECT DISTINCT rcd.medical_num,rpd.firstname, rpd.date_of_birth, rel_name, mailid, phonenum,rcd.refer_date, rpd.patient_unique_id, pa.firstname as phyfname FROM referral_patient_details rpd INNER JOIN referral_patient_test_details rptd ON rptd.patient_unique_id=rpd.patient_unique_id INNER JOIN laboratory_details ld ON ld.laboratory_id=rptd.laboratory_id INNER JOIN physician_appointment pa ON pa.physician_id=rptd.physician_id INNER JOIN referral_confirmation_details rcd ON rcd.medical_num=rptd.medical_num INNER JOIN relationship rel ON rel.rel_id=rcd.relationship WHERE rpd.phonenum='".$phone_num."') UNION (SELECT DISTINCT rcd.medical_num,pdd.firstname, pdd.date_of_birth, rel_name, mailid, phonenum,rcd.refer_date, pdd.patient_unique_id, pa.firstname as phyfname FROM patient_dep_details pdd INNER JOIN referral_patient_test_details rptd ON rptd.patient_unique_id=pdd.patient_unique_id INNER JOIN laboratory_details ld ON ld.laboratory_id=rptd.laboratory_id INNER JOIN physician_appointment pa ON pa.physician_id=rptd.physician_id INNER JOIN referral_confirmation_details rcd ON rcd.medical_num=rptd.medical_num INNER JOIN relationship rel ON rel.rel_id=rcd.relationship WHERE pdd.phonenum='".$phone_num."')");
$this->db->trans_complete();
            if ($query->num_rows() > 0)    
            {
              return $query->result();
            }
}

function get_patient_history($patient_unique_id=null){

  
$this->db->trans_start();
$query = $this->db->query("(SELECT rpd.firstname,rpd.gender,rpd.date_of_birth,rpd.phonenum,pa.firstname AS phyfname,pa.lastname AS phylname FROM referral_patient_details rpd INNER JOIN physician_appointment pa ON pa.physician_id=rpd.physician_id WHERE rpd.patient_unique_id='".$patient_unique_id."') UNION (SELECT pdd.firstname,pdd.gender,pdd.date_of_birth,pdd.phonenum,pa.firstname AS phyfname,pa.lastname AS phylname FROM patient_dep_details pdd INNER JOIN physician_appointment pa ON pa.physician_id=pdd.physician_id WHERE pdd.patient_unique_id='".$patient_unique_id."')");
$this->db->trans_complete();
            if ($query->num_rows() > 0)    
            {
              return $query->result();
            }
}

}

