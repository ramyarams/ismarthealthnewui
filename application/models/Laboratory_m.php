<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
class Laboratory_m extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

function get_labdetails($laboratory_id=null){
  $this->db->select("*");
  $this->db->from('laboratory_details');
  $this->db->join('state', 'state.state_id = laboratory_details.lab_state');
  $this->db->join('city', 'city.city_id = laboratory_details.lab_city');
  $this->db->where('laboratory_id',$laboratory_id);
  $query = $this->db->get();
  return $query->result();
}

public function get_labs(){
  $this->db->select("*");
  $this->db->from('laboratory_details');
  $this->db->join('state', 'state.state_id = laboratory_details.lab_state', 'left');
  $this->db->join('city', 'city.city_id = laboratory_details.lab_city', 'left');
  $this->db->order_by("laboratory_id", "desc");
  $query = $this->db->get();
  return $query->result_array();
}


public function get_physician_list(){
  $this->db->select("*");
  $this->db->from('physician_appointment');
  $this->db->join('state', 'state.state_id = physician_appointment.state', 'left');
  $this->db->join('city', 'city.city_id = physician_appointment.city', 'left');
  $this->db->join('super_speciality', 'super_speciality.superspeciality_id = physician_appointment.specialization', 'left');
  $this->db->order_by("physician_id", "desc");
  $query = $this->db->get();
  return $query->result_array();
}

public function get_physician_admin_list(){
  $this->db->select("*");
  $this->db->from('phy_admin');
  $this->db->join('physician_appointment', 'physician_appointment.physician_id = phy_admin.physician_id', 'left');
  $this->db->order_by("phy_admin_id", "desc");
  $query = $this->db->get();
  return $query->result_array();
}

public function get_patient_list(){
  $this->db->select("*,referral_patient_details.firstname as patient_firstname, referral_patient_details.lastname as patient_lastname");
  $this->db->from('referral_patient_details');
  $this->db->join('physician_appointment', 'physician_appointment.physician_id = referral_patient_details.physician_id', 'left');
  $this->db->order_by("referral_patient_id", "desc");
  $query = $this->db->get();
  return $query->result_array();
}

public function labcount()
{
	$query = $this->db->query('SELECT * FROM laboratory_details');
    return $query->num_rows();
}

public function physiciancount()
{
	$query = $this->db->query('SELECT * FROM physician_appointment');
    return $query->num_rows();
}

public function physicianadmincount()
{
	$query = $this->db->query('SELECT * FROM phy_admin');
    return $query->num_rows();
}


public function patientscount()
{
	$query = $this->db->query('SELECT * FROM referral_patient_details');
    return $query->num_rows();
}

public function totaluserscount()
{
	$query = $this->db->query('SELECT * FROM login_details');
    return $query->num_rows();
}


public function get_packages()
{
  $query = $this->db->query("SELECT *, count(test_id) as testcounts, p.id as package_id FROM package as p 
                                LEFT JOIN package_contains as pc ON pc.package_id=p.id 
                                GROUP BY pc.package_id
                                ORDER BY p.id DESC");
  return $query->result_array();
}

public function get_packages_details()
{
  $package_id = $this->uri->segment('3');
  $query = $this->db->query("SELECT * FROM package as p 
                                LEFT JOIN package_contains as pc ON pc.package_id=p.id 
                                LEFT JOIN investigation_test_details as itd ON itd.parse_id=pc.test_id
                                WHERE p.id = '".$package_id."'
                                ORDER BY p.id DESC");
  return $query->result_array();
}




function update_labdetails($laboratory_id,$laboratory_name,$laboratory_address,$laboratory_phone,$state,$city,$pincode){

			$data = array(
		        'laboratory_name' => $laboratory_name,
		        'laboratory_address' => $laboratory_address,
		        'laboratory_phone' => $laboratory_phone,
            'lab_state' => $state,
		        'lab_city' => $city,
		        'lab_pincode' => $pincode
		     );
	
			$this->db->where("laboratory_id",$laboratory_id);
			$this->db->update("laboratory_details",$data);

			if($this->db->affected_rows() >=0){
  return true; 
}else{
  return false; 
}
}


function downloadexcelsheet()
 {
  $lab_id = $this->uri->segment('3');
  $query = $this->db->query('SELECT itd.test_name as laboratory_tests_name, ltd.laboratory_tests, ltd.laboratory_testid, ltd.test_price, ltd.custom_test_name FROM laboratory_test_details as ltd LEFT JOIN investigation_test_details as itd ON itd.parse_id = ltd.laboratory_tests where ltd.laboratory_id ="'.$lab_id.'" AND ltd.status = "ACTIVE" ORDER BY ltd.laboratory_testid DESC');
    return $query->result();
 }




}

