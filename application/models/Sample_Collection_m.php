<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
class Sample_Collection_m extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
  function update_samples(){
    $sample_value=$this->input->post('sample_value');
    $test_ID=$this->input->post('test_ID');

    $this->db->set('sample_value', $sample_value);
    $this->db->where('referral_test_ID', $test_ID);
    $result=$this->db->update('sample_results');
    $this->db->set('labapproval_id', 1);
    $this->db->set('labapproval_datetime',$now);
    $this->db->where('ID', $test_ID);
    $this->db->update('referral_patient_test_details');
    
    $status=2;
    $this->db->set('pat_status', $status); 
    $this->db->where('medical_num', $this->input->post('medical_id1'));
    $this->db->where('ID', $this->input->post('lab_testname'));
    $this->db->where('patient_unique_id', $this->input->post('patient_unique_id1'));
    $this->db->update('referral_patient_test_details');

    return $result;
    
  }
// function get_labdetails($laboratory_id=null){
//   $this->db->select("*");
//   $this->db->from('laboratory_details');
// $this->db->join('state', 'state.state_id = laboratory_details.lab_zstate');
//   $this->db->join('city', 'city.city_id = laboratory_details.lab_city');
//   $this->db->where('laboratory_id',$laboratory_id);
//   $query = $this->db->get();
//   return $query->result();
// }

// function update_labdetails($laboratory_id,$laboratory_name,$laboratory_address,$laboratory_phone,$state,$city,$pincode){

// 			$data = array(
// 		        'laboratory_name' => $laboratory_name,
// 		        'laboratory_address' => $laboratory_address,
// 		        'laboratory_phone' => $laboratory_phone,
//                         'lab_state' => $state,
// 		        'lab_city' => $city,
// 		        'lab_pincode' => $pincode
// 		     );
	
// 			$this->db->where("laboratory_id",$laboratory_id);
// 			$this->db->update("laboratory_details",$data);

// 			if($this->db->affected_rows() >=0){
//   return true; 
// }else{
//   return false; 
// }
// }

function update_sample_collected_status($patient_unique_id1=null,$medical_id1=null,$lab_testname=null,$sample_collected_status=null)
{
  $this->db->set('sample_collected_id', $sample_collected_status); 
  $this->db->where('medical_num', $medical_id1);
  $this->db->where('ID', $lab_testname);
  $this->db->where('patient_unique_id', $patient_unique_id1);
  $this->db->update('referral_patient_test_details');
  if ($this->db->affected_rows() >0)
    return true;
  else
    return false;
}

function get_labtest_details($patient_unique_id1,$medical_num)
{
   $this->db->trans_start();
   $query = $this->db->query("(SELECT rptd.labapproval_id,sr.sample_value,sr.referral_test_ID,ltd.unit,ltd.reference_range,rptd.sample_collected_id,rptd.medical_num,rptd.patient_unique_id,id.investigation_name,rptd.ID AS referral_test_ID,rptd.billing_id,rptd.sample_collected_id,itd.test_name,rptd.ID,rptd.report_filename,rcd.brief_history,rcd.phy_advice,rptd.laboratory_tests,date,time,rptd.created_on, sm.status, sm.status_id, rptd.pat_status,itd.investigation_id FROM referral_patient_test_details rptd INNER JOIN referral_patient_details rpd ON rptd.patient_unique_id=rpd.patient_unique_id INNER JOIN investigation_test_details itd ON itd.parse_id=rptd.laboratory_tests INNER JOIN laboratory_details ld ON ld.laboratory_id=rptd.laboratory_id INNER JOIN investigation_details id on id.investigation_id=itd.investigation_id INNER JOIN physician_appointment pa ON pa.physician_id=rptd.physician_id INNER JOIN status_master sm on sm.status_id=rptd.pat_status INNER JOIN referral_confirmation_details rcd ON rcd.patient_unique_id=rptd.patient_unique_id AND rcd.medical_num=rptd.medical_num LEFT JOIN laboratory_test_details ltd ON rptd.laboratory_tests=ltd.laboratory_tests AND rptd.laboratory_id=ltd.laboratory_id LEFT JOIN sample_results sr ON sr.referral_test_ID=rptd.ID WHERE rpd.patient_unique_id='".$patient_unique_id1."' AND rptd.medical_num='".$medical_num."'  AND rptd.sample_collected_id=1  GROUP BY rptd.ID) UNION (SELECT rptd.labapproval_id,sr.sample_value,sr.referral_test_ID,ltd.unit,ltd.reference_range,rptd.sample_collected_id,rptd.medical_num,rptd.patient_unique_id,id.investigation_name,rptd.ID AS referral_test_ID,rptd.billing_id,rptd.sample_collected_id,itd.test_name,rptd.ID,rptd.report_filename,rcd.brief_history,rcd.phy_advice,rptd.laboratory_tests,date,time,rptd.created_on, sm.status, sm.status_id, rptd.pat_status,itd.investigation_id FROM referral_patient_test_details rptd INNER JOIN patient_dep_details pdd ON rptd.patient_unique_id=pdd.patient_unique_id INNER JOIN investigation_test_details itd ON itd.parse_id=rptd.laboratory_tests INNER JOIN laboratory_details ld ON ld.laboratory_id=rptd.laboratory_id INNER JOIN investigation_details id on id.investigation_id=itd.investigation_id INNER JOIN physician_appointment pa ON pa.physician_id=rptd.physician_id INNER JOIN status_master sm on sm.status_id=rptd.pat_status INNER JOIN referral_confirmation_details rcd ON rcd.patient_unique_id=rptd.patient_unique_id AND rcd.medical_num=rptd.medical_num LEFT JOIN laboratory_test_details ltd ON rptd.laboratory_tests=ltd.laboratory_tests AND rptd.laboratory_id=ltd.laboratory_id LEFT JOIN sample_results sr ON sr.referral_test_ID=rptd.ID WHERE pdd.patient_unique_id='".$patient_unique_id1."' AND rptd.medical_num='".$medical_num."'  AND rptd.sample_collected_id=1  GROUP BY rptd.ID)");
   
    $this->db->trans_complete();
    if($query->num_rows()>=1)
    {
      return $query->result();
    }
    else
    {
      return "no content";
    }
}


    function add_sampleresults($referral_test_ID=null,$sample_value=null)
  {

    $tt1= explode(",", $sample_value);
    $tt2= explode(",", $referral_test_ID);
    $query_string="INSERT into `sample_results`(`sample_value`,`referral_test_ID`,`created_on`) VALUES";
    for($i=0;$i<sizeof($tt1);$i++)
    {
    $query_string.="('".$tt1[$i]."','".$tt2[$i]."',now()),";
    }
    $query_string=rtrim($query_string,",");
    $this->db->trans_start();
    $query = $this->db->query($query_string);
    $this->db->trans_complete();

    if ($this->db->trans_status() === FALSE) return null;
  else
    return true;
  }

      public function upload_path($patient_unique_id1,$medical_id1,$lab_testname,$status,$uploadfilename){
       //create array to load to database
     $image_data = $this->upload->data();
   
     $insert_data = array(
        'report_filename' => $uploadfilename,                  
        'report_fullpath' => $image_data['full_path']
      );

           $this->db->where('medical_num', $medical_id1);
           $this->db->where('ID', $lab_testname);
           $this->db->where('patient_unique_id', $patient_unique_id1);
           $this->db->update('referral_patient_test_details', $insert_data);



}

 public function getRows($file_id = ''){
        $this->db->select('file_id,investigation_file,referral_test_ID,uploaded_on');
        $this->db->from('investigation_file');
        if($file_id){
            $this->db->where('file_id',$file_id);
            $query = $this->db->get();
            $result = $query->row_array();
        }else{
            $this->db->order_by('uploaded_on','desc');
            $query = $this->db->get();
            $result = $query->result_array();
        }
        return !empty($result)?$result:false;
    }
    
    /*
     * Insert file data into the database
     * @param array the data for inserting into the table
     */
    public function insert($data = array()){
        $insert = $this->db->insert_batch('investigation_file',$data);
        return $insert?true:false;
    }

    function getSampleResults(){
        $query=$this->db->query("SELECT * FROM `referral_patient_test_details` AS rptd  INNER JOIN `sample_results` AS sr ON sr.referral_test_ID=rptd.ID");
        return $query->result();
        //returns from this string in the db, converts it into an array
    }
}

