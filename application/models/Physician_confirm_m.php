<?php

/**
* 
*/
class Physician_confirm_m extends CI_Model
{
  
  function __construct()
  {
    parent::__construct();
  }


function get_search_details($search)
{

$phy_id=$_SESSION['physician_id'];
  $this->db->trans_start();
$query = $this->db->query("(SELECT  pdd.firstname,pdd.mailid,pdd.phonenum,pdd.alt_phonenum,rel_name,rpd.firstname as main_pat_name,pdd.main_pat_id,pdd.patient_dep_id  FROM patient_dep_details pdd INNER JOIN relationship r on r.rel_id=pdd.relationship INNER JOIN referral_patient_details rpd ON rpd.referral_patient_id=pdd.main_pat_id where (pdd.firstname LIKE '%".$search."%' OR pdd.mailid LIKE '%".$search."%' OR pdd.phonenum LIKE '%".$search."%' OR pdd.alt_phonenum LIKE '%".$search."%') AND  pdd.physician_id='".$phy_id."') UNION (SELECT rpd1.firstname,rpd1.mailid,rpd1.phonenum,rpd1.alt_phonenum,rel_name,rpd1.alt_phonenum AS main_pat_name,rpd1.referral_patient_id,rpd1.patient_dep_id
FROM referral_patient_details rpd1 INNER JOIN relationship r ON r.rel_id = rpd1.relationship WHERE (rpd1.firstname LIKE  '%".$search."%' OR rpd1.mailid LIKE  '%".$search."%' OR rpd1.alt_phonenum LIKE '%".$search."%' OR rpd1.phonenum LIKE '%".$search."%') AND rpd1.physician_id ='".$phy_id."')");
$this->db->trans_complete();
            if ($query->num_rows() > 0)    
            {
              return $query->result();
            }
}


function referral_confirmation_details($referral_pat_id){
  $this->db->trans_start();
$query = $this->db->query("SELECT * FROM `referral_patient_details` INNER JOIN `referral_test_details` ON `referral_patient_details`.`referral_patient_id`=`referral_test_details`.`diagnostic_id` WHERE `referral_patient_details`.`referral_patient_id`='".$referral_pat_id."' order by `referral_test_id` desc");
$this->db->trans_complete();
            if ($query->num_rows() > 0)    
            {
              return $query->result();
            }
}

function Physician_referral_patient($diagnostic_id=null,$firstname=null,$phonenum=null,$gender=null,$age=null,$address=null,$city=null,$diagnostic_name=null,$diagnostic_address=null,$diagnostic_phone_number=null,$test_details=null,$test_date=null){
      $this->db->trans_start();
      $query=$this->db->query("INSERT INTO `physician_patient_appointment` (`firstname`, `phonenum`, `gender`,`age`,`address`,`city`, `diagnostic_name`, `diagnostic_address`,`diagnostic_phone_number`,`test_details`,`test_date`, `created_on`) values('".$firstname."','".$phonenum."','".$gender."','".$age."','".$address."','".$city."','".$diagnostic_name."','".$diagnostic_address."','".$diagnostic_phone_number."','".$test_details."','".$test_date."',now())");
      $this->db->trans_complete();
     if ($this->db->trans_status() === FALSE) return null;

          else
          return true;
  }



  function get_confirmation_details($referral_patient_id){
  $this->db->select("*");
  $this->db->from('referral_patient_details');
  $this->db->where('referral_patient_id',$referral_patient_id);
  
  $query = $this->db->get();
  return $query->result();
}

 function get_dep_confirmation_details($referral_patient_id,$patient_dep_id){
  $this->db->select("*");
  $this->db->from('patient_dep_details');
  $this->db->join('relationship', 'patient_dep_details.relationship = relationship.rel_id');

  $this->db->where('patient_dep_id',$patient_dep_id);
  $this->db->where('main_pat_id',$referral_patient_id);
  
  $query = $this->db->get();
  return $query->result();
}
 function get_laboratory_details($laboratory_id){
  $this->db->select("*");
  $this->db->from('laboratory_details');
  $this->db->where('laboratory_id',$laboratory_id);
  
  $query = $this->db->get();
  return $query->result();
}


function get_test_details($parse_id){
 $this->db->trans_start();
    $query = $this->db->query("SELECT GROUP_CONCAT(DISTINCT test_name SEPARATOR ' , ') test_name,GROUP_CONCAT(DISTINCT parse_id)parse_id FROM `investigation_test_details` WHERE parse_id in ($parse_id)");
    $this->db->trans_complete();
    if($query->num_rows()>=1)

      return $query->result();
}

function fetch_history_records($phone,$referral_patient_id){

  $this->db->trans_start();
    $query = $this->db->query("(SELECT DISTINCT rpd.date_of_birth,rcd.referred_id,rcd.medical_num,rpd.firstname, rpd.age, rel_name, mailid, phonenum, rpd.patient_unique_id, pa.firstname as phyfname FROM referral_patient_details rpd INNER JOIN referral_patient_test_details rptd ON rptd.patient_unique_id=rpd.patient_unique_id INNER JOIN laboratory_details ld ON ld.laboratory_id=rptd.laboratory_id INNER JOIN physician_appointment pa ON pa.physician_id=rptd.physician_id INNER JOIN referral_confirmation_details rcd ON rcd.medical_num=rptd.medical_num INNER JOIN relationship rel ON rel.rel_id=rcd.relationship WHERE (pa.mail_id='".$_SESSION['mail_id']."' OR  pa.phone_num='".$_SESSION['phone_num']."') AND rpd.phonenum='".$phone."' AND rpd.referral_patient_id='".$referral_patient_id."' ) UNION (SELECT DISTINCT pdd.date_of_birth,rcd.referred_id,rcd.medical_num,pdd.firstname, pdd.age, rel_name, mailid, phonenum, pdd.patient_unique_id, pa.firstname as phyfname FROM patient_dep_details pdd INNER JOIN referral_patient_test_details rptd ON rptd.patient_unique_id=pdd.patient_unique_id INNER JOIN laboratory_details ld ON ld.laboratory_id=rptd.laboratory_id INNER JOIN physician_appointment pa ON pa.physician_id=rptd.physician_id INNER JOIN referral_confirmation_details rcd ON rcd.medical_num=rptd.medical_num INNER JOIN relationship rel ON rel.rel_id=rcd.relationship WHERE (pa.mail_id='".$_SESSION['mail_id']."' OR  pa.phone_num='".$_SESSION['phone_num']."') AND pdd.phonenum='".$phone."' AND pdd.main_pat_id='".$referral_patient_id."')");
    $this->db->trans_complete();
    if($query->num_rows()>=1)
      return $query->result();

  }

  function fetch_dependent_records($referral_patient_id){

  $this->db->trans_start();
    $query = $this->db->query("SELECT p.date_of_birth,p.patient_dep_id, p.firstname, p.lastname, p.gender, p.age, p.address, p.city, p.pincode, p.mailid, p.phonenum, p.main_pat_id, p.created_on, p.updated_on, p.active, p.role_id, p.patient_unique_id, p.physician_id,relationship.rel_name,p.alt_phonenum FROM patient_dep_details p LEFT JOIN relationship ON relationship.rel_id = p.relationship LEFT JOIN referral_patient_details ON p.main_pat_id = referral_patient_details.referral_patient_id WHERE p.main_pat_id = '".$referral_patient_id."'");
    $this->db->trans_complete();
    if($query->num_rows()>=1)
      return $query->result();

  }


  function password_change_lab($username,$current_password,$new_password)
  {
    $this->db->select('username');
  $this->db->from('login_details');
  $this->db->where('username',$username);
  $this->db->where('password',md5($current_password));
  $query = $this->db->get();
  if($query->num_rows() > 0)
  {
    $query = $this->db->query("UPDATE laboratory_details SET laboratory_password='".md5($new_password)."', updated_on=now() WHERE laboratory_email='".$username."'");
    $query = $this->db->query("UPDATE `login_details` SET `count` = count+1 WHERE `login_details`.`username` = '".$username."';");

return true;
  }
  else
  {
    return null;
  }
}

 function password_change_patient($username,$current_password,$new_password)
  {
    $this->db->select('username');
  $this->db->from('login_details');
  $this->db->where('username',$username);
  $this->db->where('password',md5($current_password));
  $query = $this->db->get();
  if($query->num_rows() > 0)
  {
    $query = $this->db->query("UPDATE referral_patient_details SET password='".md5($new_password)."', updated_on=now() WHERE mailid='".$username."'");
    $query = $this->db->query("UPDATE `login_details` SET `count` = count+1 WHERE `login_details`.`username` = '".$username."';");

return true;
  }
  else
  {
    return null;
  }
}

function password_change_phy($username,$current_password,$new_password)
  {
    $this->db->select('username');
  $this->db->from('login_details');
  $this->db->where('username',$username);
  $this->db->where('password',md5($current_password));
  $query = $this->db->get();
  if($query->num_rows() > 0)
  {
    $query = $this->db->query("UPDATE physician_appointment SET password='".md5($new_password)."', updated_on=now() WHERE mail_id='".$username."'");
    $query = $this->db->query("UPDATE `login_details` SET `count` = count+1 WHERE `login_details`.`username` = '".$username."';");

return true;
  }
  else
  {
    return null;
  }
}

function get_confirm_labtest($confirm_labtest){

 $this->db->select('investigation_test_details.test_name');
 $this->db->FROM ('investigation_test_details');
 $this->db->where('parse_id',$confirm_labtest);
  $query = $this->db->get();
  return $query->result();

}


}

