<?php


/**
* 
*/
class Physician_appointment_m extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

  function insert_confirmation_testdetailslab($medical_num,$testname,$testtime,$testdate,$patient_unique_id,$physician_id,$laboratory_id,$dependent_id,$referral_pat_id){

 $date=array_combine($testname,$testdate);
$time=array_combine($testname,$testtime);
             foreach ($time as $id => $names) {
               $data = array(
                       'medical_num'=>$medical_num,
                       'laboratory_tests' => $id,
                       'time' => $time[$id],
                       'date' => $date[$id],
                       'patient_unique_id' => $patient_unique_id,
                       'physician_id' => $physician_id,
                       'laboratory_id'=> $laboratory_id,
                       'dependent_id' => $dependent_id,
                       'main_patient_id' => $referral_pat_id

);
$this->db->insert('referral_patient_test_details', $data); 
             }
}

function get_doc_details(){

  $this->db->select("*");
  $this->db->from('physician_appointment');
  $this->db->join('super_speciality', 'super_speciality.superspeciality_id = physician_appointment.specialization');
  $query = $this->db->get();
  return $query->result();

}


function save_appointment_details($firstname=null,$lastname=null,$phone_num=null,$mail_id=null,$pincode=null,$clinic_name=null,$clinic_phonenum=null,$specialization=null,$address=null,$city=null,$state=null,$physician_password=null)
  {
    $this->db->select('username');
  $this->db->from('login_details');
  $this->db->where('username',$mail_id);
  $this->db->or_where('phone_num',$phone_num);
  $query = $this->db->get();
  if($query->num_rows() > 0)
  {
    $this->db->query("UPDATE physician_appointment SET mail_id='".$mail_id."' where mail_id='".$mail_id."'");
    return NULL;
  }
  else{
    
      $this->db->trans_start();
       $query = $this->db->query("INSERT INTO `physician_appointment` (`firstname`, `lastname` , `phone_num`,`mail_id`, `pincode`, `clinic_name` ,`clinic_phonenum`, `specialization`,`address`, `city` , `state`,`password`,`created_by`,`created_on`) values('".$firstname."','".$lastname."','".$phone_num."','".$mail_id."','".$pincode."','".$clinic_name."','".$clinic_phonenum."','".$specialization."','".$address."','".$city."','".$state."','".md5($physician_password)."','".$firstname."',now())");
       $this->db->trans_complete();
   
       if ($this->db->trans_status() === FALSE) return null;
   
       else
         return true;
     }
 
}
 
 function get_phydetails($physician_id){
  $this->db->select("*");
  $this->db->from('physician_appointment');
  $this->db->join('super_speciality','physician_appointment.specialization=super_speciality.superspeciality_id');
  $this->db->where('physician_appointment.physician_id',$physician_id);
  $query = $this->db->get();
  return $query->result();
//$this->db->trans_start();
//$query = $this->db->query("SELECT * FROM `physician_appointment` INNER JOIN super_speciality ON //physician_appointment.specialization=super_speciality.superspeciality_id where physician_appointment.physician_id='".$physician_id."'");
//$this->db->trans_complete();
 // if ($query->num_rows() > 0)    
 // {
 //     return $query->result();
 // }
}

function get_test_referred_details($lab_tests,$lab_id)
{
  $laboratory_tests=implode(',',$lab_tests);
 $state=$_SESSION['state'];
 $city=$_SESSION['city'];
$this->db->trans_start();
$query = $this->db->query("SELECT ld.laboratory_name,ld.laboratory_id,ld.laboratory_name,ld.laboratory_address,ld.laboratory_email,ld.laboratory_phone,ltd.laboratory_tests,itd.test_name,itd.parse_id, GROUP_CONCAT(DISTINCT itd.test_name) test_name,GROUP_CONCAT(DISTINCT itd.parse_id) parse_id FROM laboratory_test_details ltd INNER JOIN `laboratory_details` ld ON ld.laboratory_id=ltd.laboratory_id INNER JOIN investigation_test_details itd ON itd.parse_id=ltd.laboratory_tests WHERE ld.lab_state='".$state."' AND ld.lab_city='".$city."'  AND ld.laboratory_id='".$lab_id."'   AND ltd.laboratory_tests IN ($laboratory_tests) GROUP BY ld.laboratory_name");
$this->db->trans_complete();
  if ($query->num_rows() > 0)    
  {
      return $query->result();
  }

}


function getphyname($physician_id){
  $this->db->select("firstname");
  $this->db->from('physician_appointment');
  $this->db->where('physician_id',$physician_id);
  $query = $this->db->get();
return $query->result();
}

function getprofile($mail_id){
  $this->db->select("profile_filename,profile_fullpath");
  $this->db->from('laboratory_details');
  $this->db->where('laboratory_email',$mail_id);
  $query = $this->db->get();
return $query->result();
}



	function get_specialization_details(){
 $this->db->select("superspeciality_id,superspeciality_name");
  $this->db->from('super_speciality');
  $query = $this->db->get();
  return $query->result();
}

function get_test_details(){
  $this->db->distinct();
  $this->db->select("laboratory_tests");
  $this->db->from('laboratory_test_details');
  $query = $this->db->get();
  return $query->result();
}

function get_clinic_details(){
  $this->db->select("clinic_id,clinic_name");
  $this->db->from('clinic_details');
  $query = $this->db->get();
  return $query->result();
}

function getstates()
     {
        $this -> db -> select('*');
        $this->db->where('active',1);
        $this->db->where('country_id',100);
        $this->db->order_by('state_name',"ASC");
        $query = $this ->db-> get('state');
        return $query->result();
     }


    
function getcity($state_id)
     {
        $this -> db -> select('*');
        $this -> db -> where('state_id', $state_id);
        $this->db->order_by("city_name", "asc");
        $query = $this -> db -> get('city');
        return $query->result();
}




function get_diagnostics_name_details(){
 $this->db->select("diagnostic_id,diagnostic_name,diagnostic_address,diagnostic_phone_number");
  $this->db->from('diagnostic_details');
  $query = $this->db->get();
  return $query->result();
}
function get_laboratory_details($laboratory_tests){
$laboratory_tests=implode(',',$laboratory_tests);
 $state=$_SESSION['state'];
 $city=$_SESSION['city'];
$this->db->trans_start();
$query = $this->db->query("SELECT ld.laboratory_name,ld.laboratory_id,ld.laboratory_name,ld.laboratory_address,ld.laboratory_phone,ltd.laboratory_tests,itd.test_name,itd.parse_id, GROUP_CONCAT(DISTINCT itd.test_name) test_name,GROUP_CONCAT(DISTINCT itd.parse_id) parse_id FROM laboratory_test_details ltd INNER JOIN `laboratory_details` ld ON ld.laboratory_id=ltd.laboratory_id INNER JOIN investigation_test_details itd ON itd.parse_id=ltd.laboratory_tests WHERE ld.lab_state='".$state."' AND ld.lab_city='".$city."' AND ltd.laboratory_tests IN ($laboratory_tests) GROUP BY ld.laboratory_name");
$this->db->trans_complete();
            if ($query->num_rows() > 0)    
            {
              return $query->result();
            }

}

function get_diagnostics_test_details($diagnostic_id){
 $this->db->select("test_name");
  $this->db->from('diagnostic_tests');
  $this->db->where('diagnostic_id',$diagnostic_id);
  $query = $this->db->get();
  return $query->result();
}


function insert_patient_details($firstname=null,$lastname=null,$gender=null,$age=null,$address=null,$pincode=null,$state=null,$city=null,$emailid=null,$phonenum=null,$patient_password=null,$physician_id=null)
{
  $this->db->select('mailid');
  $this->db->from('referral_patient_details');
  // $this->db->where('mailid',$emailid);
  $this->db->where('phonenum',$phonenum);
  $query = $this->db->get();
  if($query->num_rows() > 0)
  {
    $this->db->query("UPDATE referral_patient_details SET phonenum='".$phonenum."' where phonenum='".$phonenum."'");
    return NULL;
  }
  else
  {
 
    $this->db->query("INSERT INTO `referral_patient_details`(`firstname`,`lastname`,`gender`,`age`,`address`,`pincode`,`state`,`city`,`mailid`,`phonenum`,`password`,`created_on`,`physician_id`) VALUES('".$firstname."','".$lastname."','".$gender."','".$age."','".$address."','".$pincode."','".$state."','".$city."','".$emailid."','".$phonenum."','".md5($patient_password)."',now(),'".$physician_id."')");
 
    $this->db->trans_complete();
    if ($this->db->trans_status() === FALSE)
    {
      return null;
    }
    else
    {
      return $this->db->insert_id();
    }
  }
}



function get_patient_details(){
  $physician_id=$_SESSION['physician_id'];
//echo $physician_id;
 $this->db->select("rpd.referral_patient_id,rpd.firstname,rpd.mailid,rpd.phonenum,rpd.age,rpd.created_on,rpd.status,rpd.r_count,pa.state,pa.city");
  $this->db->from('referral_patient_details as rpd');
  $this->db->join('physician_appointment as pa','pa.physician_id=rpd.physician_id ');
 $this->db->where("pa.physician_id",$physician_id);
$this->db->where("rpd.active","1");
$this->db->where("relationship","1");
  // $this->db->order_by("referral_patient_id", "desc");
  $query = $this->db->get();
  return $query->result();
}

function get_selected_diagnostics_details($diagnostic_id){
  $this->db->select("diagnostic_id,diagnostic_name,diagnostic_address,diagnostic_phone_number");
  $this->db->from('diagnostic_details');
  $this->db->where("diagnostic_id",$diagnostic_id);

  $query = $this->db->get();
  return $query->result();
}

function get_diagnostic_details($diagnostic_name=null,$diagnostic_address=null,$diagnostic_phone_number=null,$test_details=null,$test_date=null,$referral_pat_id=NULL){
       $this->db->trans_start();
       $this->db->query("INSERT INTO `referral_test_details`(`diagnostic_name`,`diagnostic_address`,`diagnostic_phone_number`,`test_details`,`test_date`,`created_on`,`diagnostic_id`) VALUES('".$diagnostic_name."','".$diagnostic_address."','".$diagnostic_phone_number."','".$test_details."','".$test_date."',now(),'".$referral_pat_id."')");
       $this->db->trans_complete();
       if ($this->db->trans_status() === FALSE) return null;

          else
          return true;
}

function login_check($username,$pass){
  $this->db->trans_start();
      $query = $this->db->query("SELECT * from `login_details` where (phone_num='".$username."' OR username='".$username."') AND password='".$pass."' AND active=1");
      $this->db->trans_complete();
      if($query->num_rows()>=1)
      {
      	date_default_timezone_set('Asia/kolkata');
      	$date=date('Y-m-d H:i:s');
        $this->db->query("UPDATE login_details SET last_login='".$date."' WHERE username='".$username."'");
        return $query->result();
      }
      else
       {
         return "no content";
       }
}


function laboratory_details($laboratory_name=null,$lab_address=null,$lab_phone_number=null,$lab_email=null,$laboratory_password=null,$lab_state=null,$lab_city=null,$lab_pincode=null)
{
  $this->db->select('username');
  $this->db->from('login_details');
  $this->db->where('username',$lab_email);
  $this->db->or_where('phone_num',$lab_phone_number);
  $query = $this->db->get();
  if($query->num_rows() > 0)
  {
    $this->db->query("UPDATE laboratory_details SET laboratory_email='".$lab_email."' where laboratory_email='".$lab_email."'");
    return NULL;
  }
  else
  {
    $this->db->trans_start();


    $query = $this->db->query("INSERT INTO `laboratory_details` (`laboratory_name`, `laboratory_address`, `laboratory_phone` , `laboratory_email`, `laboratory_password`, `lab_state`,`lab_city`,`lab_pincode`,`created_on`) values('".$laboratory_name."','".$lab_address."','".$lab_phone_number."','".$lab_email."','".md5($laboratory_password)."','".$lab_state."','".$lab_city."','".$lab_pincode."',now())");
  $this->db->trans_complete();
  if ($this->db->trans_status() === FALSE) return null;
  else
    return true;
  }
}
function get_labtest_deatails($referral_id,$lab_id)
{
 $this->db->trans_start();
    $query = $this->db->query("SELECT patient_email,phone_number,gender,age,address,city,physician_firstname,test_date,(select GROUP_CONCAT(test_name SEPARATOR ', ') from investigation_test_details as itd where FIND_IN_SET (itd.parse_id,rfc.tests)) as test_name FROM `referral_confirmation_details` as rfc INNER JOIN relationship ON relationship.rel_id=rfc.relationship WHERE `ID`='".$referral_id."' AND `rfc`.`laboratory_id`='".$lab_id."'");
    $this->db->trans_complete();
    if($query->num_rows()>=1)
    {
      return $query->result();
    }
    else
    {
      return "no content";
    }
}

//to get the labid from the table laboratory_details
  function get_labid()
  {
    $this->db->trans_start();
    $query = $this->db->query("SELECT * from `laboratory_details` ORDER BY laboratory_id DESC");
    $this->db->trans_complete();
    if($query->num_rows()>=1)
      return $query->result()[0]->laboratory_id;
    }

  //   function laboratory_test_details($laboratory_tests,$labid){

  //          foreach ( $laboratory_tests as $row){
  //           $data[] = array(
  //                   'laboratory_tests' => $row['0'],
  //                   'laboratory_id'=>$laboratory_id,
  //           );
  //       }
  //   $this->db->insert_batch('laboratory_test_details', $data);
  // }

  function laboratory_test_details($laboratory_tests=null,$labid=null)
  {
    $tt1= explode(",", $laboratory_tests);
    $query_string="INSERT into `laboratory_test_details`(`laboratory_tests`,laboratory_id) VALUES";
    for($i=0;$i<sizeof($tt1);$i++)
    {
    $query_string.="('".$tt1[$i]."','".$labid."'),";
    }
    $query_string=rtrim($query_string,",");
    $this->db->trans_start();
    $query = $this->db->query($query_string);
    $this->db->trans_complete();

    if ($this->db->trans_status() === FALSE) return null;
  else
    return true;
  }

  function relationship_details($address=null,$city=null,$pincode=null,$mailid=null,$phonenum=null,$altphone=null,$name=null,$age=null,$gender=null,$relationship=null, $patient_unique_id=null,$physician_id=null,$referral_patient_id=null){
      $query = $this->db->query("INSERT INTO `patient_dep_details`(`firstname`, `gender`, `age`, `address`, `city`, `pincode`, `mailid`, `phonenum`,`alt_phonenum`, `relationship`, `created_on`, `physician_id`,`main_pat_id`) values('".$name."','".$gender."','".$age."','".$address."','".$city."','".$pincode."','".$mailid."','".$phonenum."','".$altphone."','".$relationship."',now(),'".$physician_id."','".$referral_patient_id."')");
      $insert_id = $this->db->insert_id();
      return  $insert_id;
}

function update_relationship_details($physician_id=null,$patient_unique_id=null,$rel_id=null,$referral_patient_id=null)
{
   $value=array('physician_id'=>$physician_id,'patient_unique_id'=>$patient_unique_id,'main_pat_id'=>$referral_patient_id);
  // $this->db->set('patient_unique_id', $patient_unique_id); //value that used to update column  
$this->db->where('patient_dep_id', $rel_id); //which row want to upgrade  
// $this->db->update('referral_patient_details');  
if( $this->db->update('patient_dep_details',$value)){
      return true;
     }else{
      return false;
     }

}

// function update_dep_details($physician_id,$patient_unique_id,$rel_id,$referral_patient_id)
// {
//     $query = "UPDATE patient_dep_details SET main_pat_id=1 WHERE patient_dep_id=1";

//     $data = array(
//     array(
//         'ID' => 1,
//         'Settings Name' => 'Hello',
//         'Settings Value' => 'True'
//     )
// );
// $this->db->update_batch('tableName', $data, 'id'); 
// }


  function insert_confirmation_details($firstname=null,$mailid=null,$phonenum=null,$gender=null,$age=null,$relationship=null,$address=null,$city=null,$laboratory_name=null,$laboratory_address=null,$laboratory_phone=null,$laboratory_tests=null,$referral_pat_id=null,$laboratory_id=null,$password=null,$patient_unique_id=null,$physician_firstname=null,$physician_comments=null){
    $physician_id=$_SESSION['physician_id'];
  $this->db->trans_start();
  $this->db->query("INSERT INTO `referral_confirmation_details` (`patient_name`, `patient_email`, `phone_number`, `gender`, `age`,`relationship`,`address`, `city`, `tests`, `laboratory_name`, `laboratory_address`, `laboratory_phone`,`referral_pat_id`,`laboratory_id`, `created_on`, `created_by`,`password`,`patient_unique_id`,`physician_firstname`,`physician_id`,`physician_comments`) VALUES ('".$firstname."','".$mailid."','".$phonenum."','".$gender."','".$age."','".$relationship."','".$address."','".$city."','".$laboratory_tests."','".$laboratory_name."','".$laboratory_address."','".$laboratory_phone."','".$referral_pat_id."','".$laboratory_id."',now(),'".$firstname."','".$password."','".$patient_unique_id."','".$physician_firstname."','".$physician_id."','".$physician_comments."')");
  $this->db->trans_complete();
  if ($this->db->trans_status() === FALSE) return null;

          else
          return true;
}


function insert_confirmation_testdetails($medical_num,$testname,$testtime,$testdate,$patient_unique_id,$laboratory_id,$dependent_id,$referral_pat_id){

 $physician_id=$_SESSION['physician_id'];
 $date=array_combine($testname,$testdate);
$time=array_combine($testname,$testtime);
             foreach ($time as $id => $names) {
               $data = array(
                       'medical_num'=>$medical_num,
                       'laboratory_tests' => $id,
                       'time' => $time[$id],
                       'date' => $date[$id],
                       'patient_unique_id' => $patient_unique_id,
                       'physician_id' => $physician_id,
                       'laboratory_id'=> $laboratory_id,
                       'dependent_id' => $dependent_id,
                       'main_patient_id' => $referral_pat_id

);
$this->db->insert('referral_patient_test_details', $data); 
             }
}

function get_labtest_details($patient_unique_id1,$medical_num)
{
   $this->db->trans_start();
    // $query = $this->db->query("(SELECT rptd.ID,rptd.report_filename,rcd.brief_history,rcd.phy_advice,laboratory_tests,date,time,rptd.created_on, sm.status, sm.status_id, rptd.pat_status FROM referral_patient_test_details rptd INNER JOIN referral_patient_details rpd ON rptd.patient_unique_id=rpd.patient_unique_id INNER JOIN laboratory_details ld ON ld.laboratory_id=rptd.laboratory_id INNER JOIN physician_appointment pa ON pa.physician_id=rptd.physician_id INNER JOIN status_master sm on sm.status_id=rptd.pat_status INNER JOIN referral_confirmation_details rcd ON rcd.patient_unique_id=rptd.patient_unique_id AND rcd.medical_num=rptd.medical_num WHERE rpd.patient_unique_id='".$patient_unique_id1."' AND rptd.medical_num='".$medical_num."') UNION (SELECT rptd.ID,rptd.report_filename,rcd.brief_history,rcd.phy_advice,laboratory_tests,date,time,rptd.created_on, sm.status, sm.status_id, rptd.pat_status FROM referral_patient_test_details rptd INNER JOIN patient_dep_details pdd ON rptd.patient_unique_id=pdd.patient_unique_id INNER JOIN laboratory_details ld ON ld.laboratory_id=rptd.laboratory_id INNER JOIN physician_appointment pa ON pa.physician_id=rptd.physician_id INNER JOIN status_master sm on sm.status_id=rptd.pat_status INNER JOIN referral_confirmation_details rcd ON rcd.patient_unique_id=rptd.patient_unique_id AND rcd.medical_num=rptd.medical_num WHERE pdd.patient_unique_id='".$patient_unique_id1."' AND rptd.medical_num='".$medical_num."')");

   $query = $this->db->query("(SELECT rptd.billing_id,itd.test_name,rptd.ID,rptd.report_filename,rcd.brief_history,rcd.phy_advice,laboratory_tests,date,time,rptd.created_on, sm.status, sm.status_id, rptd.pat_status FROM referral_patient_test_details rptd INNER JOIN referral_patient_details rpd ON rptd.patient_unique_id=rpd.patient_unique_id INNER JOIN investigation_test_details itd ON itd.parse_id=rptd.laboratory_tests INNER JOIN laboratory_details ld ON ld.laboratory_id=rptd.laboratory_id INNER JOIN physician_appointment pa ON pa.physician_id=rptd.physician_id INNER JOIN status_master sm on sm.status_id=rptd.pat_status INNER JOIN referral_confirmation_details rcd ON rcd.patient_unique_id=rptd.patient_unique_id AND rcd.medical_num=rptd.medical_num WHERE rpd.patient_unique_id='".$patient_unique_id1."' AND rptd.medical_num='".$medical_num."') UNION (SELECT rptd.billing_id,itd.test_name,rptd.ID,rptd.report_filename,rcd.brief_history,rcd.phy_advice,laboratory_tests,date,time,rptd.created_on, sm.status, sm.status_id, rptd.pat_status FROM referral_patient_test_details rptd INNER JOIN patient_dep_details pdd ON rptd.patient_unique_id=pdd.patient_unique_id INNER JOIN investigation_test_details itd ON itd.parse_id=rptd.laboratory_tests INNER JOIN laboratory_details ld ON ld.laboratory_id=rptd.laboratory_id INNER JOIN physician_appointment pa ON pa.physician_id=rptd.physician_id INNER JOIN status_master sm on sm.status_id=rptd.pat_status INNER JOIN referral_confirmation_details rcd ON rcd.patient_unique_id=rptd.patient_unique_id AND rcd.medical_num=rptd.medical_num WHERE pdd.patient_unique_id='".$patient_unique_id1."' AND rptd.medical_num='".$medical_num."')");
   
    $this->db->trans_complete();
    if($query->num_rows()>=1)
    {
      return $query->result();
    }
    else
    {
      return "no content";
    }
}
 function get_laboratorydetails($mail_id)
  {
    $this->db->trans_start();
    // $query = $this->db->query("SELECT * FROM  `referral_confirmation_details` INNER JOIN  `laboratory_details` ON laboratory_details.laboratory_id = referral_confirmation_details.laboratory_id WHERE laboratory_details.laboratory_email =  '".$mail_id."'"); old one

    $query = $this->db->query("(SELECT DISTINCT rcd.referred_id,rcd.medical_num,rpd.firstname, mailid, phonenum,rcd.refer_date, rpd.patient_unique_id, pa.firstname as phyfname FROM referral_patient_details rpd INNER JOIN referral_patient_test_details rptd ON rptd.patient_unique_id=rpd.patient_unique_id INNER JOIN laboratory_details ld ON ld.laboratory_id=rptd.laboratory_id INNER JOIN physician_appointment pa ON pa.physician_id=rptd.physician_id INNER JOIN referral_confirmation_details rcd ON rcd.medical_num=rptd.medical_num WHERE ld.laboratory_email='".$mail_id."' ORDER BY rptd.date DESC) UNION (SELECT DISTINCT rcd.referred_id,rcd.medical_num,pdd.firstname, mailid, phonenum,rcd.refer_date,pdd.patient_unique_id, pa.firstname as phyfname FROM patient_dep_details pdd INNER JOIN referral_patient_test_details rptd ON rptd.patient_unique_id=pdd.patient_unique_id INNER JOIN laboratory_details ld ON ld.laboratory_id=rptd.laboratory_id INNER JOIN physician_appointment pa ON pa.physician_id=rptd.physician_id INNER JOIN referral_confirmation_details rcd ON rcd.medical_num=rptd.medical_num WHERE ld.laboratory_email='".$mail_id."' ORDER BY rptd.date DESC)");
    $this->db->trans_complete();
    if($query->num_rows()>=1)
      return $query->result();
    }

    function get_referred_details()
  {
    $this->db->trans_start();
    $query = $this->db->query("SELECT * FROM `referral_confirmation_details` INNER JOIN `referral_patient_details` ON referral_confirmation_details.referral_pat_id=referral_patient_details.referral_patient_id");
    $this->db->trans_complete();
    if($query->num_rows()>=1)
      return $query->result();
    }

  //    function get_passworddetails($password)
  // {
  //   $this->db->select("*");
  // $this->db->from('referral_confirmation_details');
  // $this->db->where('password',$password);
  // $query = $this->db->get();
  // return $query->result();  
  //   }



function get_passworddetails($referral_id,$password,$lab_id){

  $this->db->trans_start();
    $query = $this->db->query("SELECT patient_email,phone_number,gender,age,address,city,physician_firstname,test_date,(select GROUP_CONCAT(test_name SEPARATOR ', ') from investigation_test_details as itd where FIND_IN_SET (itd.parse_id,rfc.tests)) as test_name FROM `referral_confirmation_details` as rfc INNER JOIN relationship ON relationship.rel_id=rfc.relationship WHERE `password` = '".$password."' AND `ID`='".$referral_id."' AND `rfc`.`laboratory_id`='".$lab_id."'");
    $this->db->trans_complete();
    if($query->num_rows()>=1)
    {
      return $query->result();
    }
    else
    {
      return "no content";
    }
  }

    function wrong_attempt($referral_id,$lab_id)
    {      
      $this->db->trans_start();
      $query = $this->db->query("UPDATE referral_confirmation_details SET attempts=attempts+1 WHERE ID='".$referral_id."' AND laboratory_id='".$lab_id."'");
    $this->db->trans_complete();

    }

    function right_attempt($referral_id,$lab_id)
    {      
      $this->db->trans_start();
      $query = $this->db->query("UPDATE referral_confirmation_details SET attempts=0 WHERE ID='".$referral_id."' AND laboratory_id='".$lab_id."'");
    $this->db->trans_complete();

    }

    function update_patient_details($referral_patient_id=null,$patient_unique_id=null){

       $this->db->set('patient_unique_id', $patient_unique_id); 
       $this->db->where('referral_patient_id', $referral_patient_id);
       $this->db->update('referral_patient_details');

        // $query=$this->db->query("UPDATE `referral_patient_details` SET `patient_unique_id`='".$patient_unique_id."',`physician_id`='".$physician_id."' WHERE `referral_patient_id`='".$referral_patient_id."'");

        if ($this->db->affected_rows() >0)
          return true;
        else
          return false;
}

function retrievedata($referral_patient_id)
{
 $this->db->select("*");
  $this->db->from('referral_patient_details');
  $this->db->where('referral_patient_id',$referral_patient_id);
  $query = $this->db->get();
  return $query->result();
}


function update_details($status,$referral_patient_id){
  $this->db->set('status', $status); //value that used to update column  
$this->db->where('referral_patient_id', $referral_patient_id); //which row want to upgrade  
$this->db->update('referral_patient_details');
}

function update_count_details($r_count=null,$referral_pat_id=null)
{
$this->db->set('r_count', $r_count); //value that used to update column  
$this->db->where('referral_patient_id', $referral_pat_id); //which row want to upgrade  
$this->db->update('referral_patient_details');
}

function update_dep_count_details($r_count=null,$referral_pat_id=null)
{
$this->db->set('r_count', $r_count); //value that used to update column  
$this->db->where('referral_patient_id', $referral_pat_id); //which row want to upgrade  
$this->db->update('referral_patient_details');
}

function update_lab_test_status($patient_unique_id1=null,$medical_id1=null,$lab_testname=null,$status=null)
{
  $this->db->set('pat_status', $status); 
  $this->db->where('medical_num', $medical_id1);
  $this->db->where('ID', $lab_testname);
  $this->db->where('patient_unique_id', $patient_unique_id1);
  $this->db->update('referral_patient_test_details');
  if ($this->db->affected_rows() >0)
    return true;
  else
    return false;
}


function get_physiciandetails($physician_id){
  $this->db->select("*");
  $this->db->from('physician_appointment');
  $this->db->join('super_speciality', 'super_speciality.superspeciality_id= physician_appointment.specialization');
  $this->db->join('state', 'state.state_id = physician_appointment.state');
  $this->db->join('city', 'city.city_id = physician_appointment.city');
  $this->db->where('mail_id',$physician_id);
  $query = $this->db->get();
  return $query->result();
}

 
function update_physiciandetails($physician_firstname=null,$physician_lastname=null,$phone_num=null,$clinic_name=null,$clinic_address=null,$state=null,$city=null,$pincode=null){

      $data = array(
            'firstname' => $physician_firstname,
            'lastname' => $physician_lastname,
            'phone_num' => $phone_num,
            'pincode'=> $pincode,
            'clinic_name'=> $clinic_name,
            'address'=> $clinic_address,
            'city'=> $city,
            'state'=> $state,
            'updated_on'=>date("Y-m-d H:i:s")
           );

    
      $this->db->where("mail_id",$_SESSION['mail_id']);
      $this->db->update("physician_appointment",$data);

  // $this->db->query("UPDATE physician_appointment SET firstname='".$physician_firstname."',lastname='".$physician_lastname."',phone_num='".$phone_num."',clinic_name='".$clinic_name."',address='".$."' where mail_id='".$_SESSION['mail_id']."'");

      if($this->db->affected_rows() >=0){
  return true; 
}else{
  return false; 
}
}

function get_adminphysiciandetails($physician_id){
  $this->db->select("*");
  $this->db->from('physician_appointment');
  $this->db->where('physician_id',$physician_id);
  $query = $this->db->get();
  return $query->result();
}
}



