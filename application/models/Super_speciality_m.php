<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
class Super_speciality_m extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

  function get_superspeciality_details()
  {
    $this->db->select("*");
    $this->db->from('super_speciality');
    $this->db->order_by("superspeciality_name", "asc");
  
    $query = $this->db->get();
    return $query->result();
  }

function speciality_patient($referral_patient_id){
  $this->db->select("firstname,phonenum");
  $this->db->from('referral_patient_details');
  $this->db->where('referral_patient_details.referral_patient_id',$referral_patient_id);
  $query = $this->db->get();
  return $query->result();
}

function speciality_dependent($patient_dep_id){
  $this->db->select("patient_dep_details.firstname,phonenum,alt_phonenum");
  $this->db->from('patient_dep_details');
  $this->db->where('patient_dep_details.patient_dep_id',$patient_dep_id);
  $query = $this->db->get();
  return $query->result();
}

function phy_name($byassign){
  $this->db->select("firstname");
  $this->db->from('physician_appointment');
  $this->db->where('physician_appointment.physician_id',$byassign);
  $query = $this->db->get();
  return $query->result();
}

function patspeciality_name($toassign){
  $this->db->select("firstname,phone_num");
  $this->db->from('physician_appointment');
  $this->db->where('physician_appointment.physician_id',$toassign);
  $query = $this->db->get();
  return $query->result();
}


function get_ssdetails($date1,$date2,$phy)
  {
    $physician_id=$_SESSION['physician_id'];

    if($phy=='phy' || $phy=='selectphy')
    {
       $this->db->trans_start();
    $query = $this->db->query("(SELECT ssd.patient_dep_id,ssd.referdate,rpd.firstname AS patient_firstname,rpd.phonenum AS patient_phone,pa.phone_num AS phy_phone,pa.mail_id,ss.superspeciality_name,pa.firstname as phy_firstname,ssd.referdate AS phy_name FROM referral_patient_details rpd INNER JOIN superspeciality_details ssd ON rpd.referral_patient_id=ssd.referral_patient_id INNER JOIN super_speciality ss on ss.superspeciality_id=ssd.superspeciality_id INNER JOIN physician_appointment pa ON pa.physician_id=ssd.to_assign WHERE ssd.by_assign='".$physician_id."' AND ssd.referdate>='".$date1."' AND referdate<='".$date2."' AND ssd.patient_dep_id=0) UNION (SELECT ssd.patient_dep_id,ssd.referdate,pdd.firstname AS patient_dep_firstname,pdd.phonenum AS patient_phone,pa.phone_num AS phy_phone,pa.mail_id,ss.superspeciality_name,pa.firstname as phy_firstname,ssd.referdate AS phy_name FROM patient_dep_details pdd INNER JOIN superspeciality_details ssd ON pdd.patient_dep_id=ssd.patient_dep_id INNER JOIN super_speciality ss on ss.superspeciality_id=ssd.superspeciality_id INNER JOIN physician_appointment pa ON pa.physician_id=ssd.to_assign WHERE ssd.by_assign='".$physician_id."' AND ssd.referdate>='".$date1."' AND referdate<='".$date2."')");
      if($query->num_rows()>=1)
      return $query->result();
    }
    else
    {
       $this->db->trans_start();
        $query = $this->db->query("(SELECT ssd.patient_dep_id,ssd.referdate,rpd.firstname AS patient_firstname,rpd.phonenum AS patient_phone,pa.phone_num AS phy_phone,pa.mail_id,ss.superspeciality_name,pa.firstname as phy_firstname,ssd.referdate AS phy_name FROM referral_patient_details rpd INNER JOIN superspeciality_details ssd ON rpd.referral_patient_id=ssd.referral_patient_id INNER JOIN super_speciality ss on ss.superspeciality_id=ssd.superspeciality_id INNER JOIN physician_appointment pa ON pa.physician_id=ssd.to_assign WHERE ssd.by_assign='".$physician_id."' AND ssd.referdate>='".$date1."' AND referdate<='".$date2."' AND ssd.to_assign='".$phy."' AND ssd.patient_dep_id=0) UNION (SELECT ssd.patient_dep_id,ssd.referdate,pdd.firstname AS patient_dep_firstname,pdd.phonenum AS patient_phone,pa.phone_num AS phy_phone,pa.mail_id,ss.superspeciality_name,pa.firstname as phy_firstname,ssd.referdate AS phy_name FROM patient_dep_details pdd INNER JOIN superspeciality_details ssd ON pdd.patient_dep_id=ssd.patient_dep_id INNER JOIN super_speciality ss on ss.superspeciality_id=ssd.superspeciality_id INNER JOIN physician_appointment pa ON pa.physician_id=ssd.to_assign WHERE ssd.by_assign='".$physician_id."' AND ssd.referdate>='".$date1."' AND referdate<='".$date2."' AND ssd.to_assign='".$phy."')");
      if($query->num_rows()>=1)
      return $query->result();
    }
   
  }
  

 //  function get_superspecialityreferred_details(){
 // $physician_id=$_SESSION['physician_id'];
 //    $this->db->trans_start();
 //    $query = $this->db->query("SELECT * FROM `superspeciality_details` INNER JOIN `referral_patient_details` on superspeciality_details.referral_patient_id=referral_patient_details.referral_patient_id where superspeciality_details.physician_id=2");
 //    $this->db->trans_complete();
 //    if($query->num_rows()>=1)
 //      return $query->result();
 //    else
 //      return false;
 //    //  $this->db->select("*");
 //    // $this->db->from('superspeciality_details');
 //    // $this->db->join('referral_patient_details','superspeciality_details.referral_patient_id=referral_patient_details.referral_patient_id');
 //    // $this->db->where('superspeciality_details.physician_id', 2);
  
 //    // $query = $this->db->get();
 //    // return $query->result();
 //  }


//function getphysician_details($superspeciality)
  //   {

    //    $this->db->trans_start();
   // $query = $this->db->query(" SELECT * FROM `physician_appointment` inner join `super_speciality` on physician_appointment.specialization=super_speciality.superspeciality_id where super_speciality.superspeciality_id='".$superspeciality."'");
 //   $this->db->trans_complete();
   // if($query->num_rows()>=1)
     // return $query->result();
    //else
     // return false;
//}

function getphysician_details($superspeciality)
     {
      $phy_id=$_SESSION['physician_id'];
        $this->db->trans_start();
    $query = $this->db->query(" SELECT * FROM `physician_appointment` inner join `super_speciality` on physician_appointment.specialization=super_speciality.superspeciality_id where super_speciality.superspeciality_id='".$superspeciality."' AND physician_id NOT IN ('".$phy_id."')");
    $this->db->trans_complete();
    if($query->num_rows()>=1)
      return $query->result();
    else
      return false;
}

function getphyhistory_details($physician_id)
     {

    $this->db->trans_start();
    $query = $this->db->query("SELECT * FROM `physician_appointment` inner join `state` on physician_appointment.state=state.state_id inner join `city` on physician_appointment.city=city.city_id where physician_id='".$physician_id."'");
    $this->db->trans_complete();
    if($query->num_rows()>=1)
      return $query->result();
    else
      return false;
}

function insert_specialityDetails($referral_patient_id, $superspeciality_id,$byassign,$toassign,$referdate=null,$comments=null,$refertime=null,$referclinic_name=null){
   $this->db->trans_start();
   $patient_dep_id=0;
   $this->db->query("INSERT INTO `superspeciality_details`(`referral_patient_id`,`patient_dep_id`,`superspeciality_id`,`comments`,`by_assign`,`to_assign`,`referdate`,`refertime`,`referclinic_name`) VALUES('".$referral_patient_id."','".$patient_dep_id."','".$superspeciality_id."','".$comments."','".$byassign."','".$toassign."','".$referdate."','".$refertime."','".$referclinic_name."')");
    $this->db->trans_complete();
    if ($this->db->trans_status() === FALSE)
    {
      return null;
    }
    else
    {
      return true;
    }
  }

function insert_depspecialityDetails($referral_patient_id,$patient_dep_id, $superspeciality_id,$byassign,$toassign,$referdate,$comments,$refertime,$referclinic_name){
    $this->db->trans_start();
   $this->db->query("INSERT INTO `superspeciality_details`(`referral_patient_id`,`patient_dep_id`,`superspeciality_id`,`comments`,`by_assign`,`to_assign`,`referdate`,`refertime`,`referclinic_name`) VALUES('".$referral_patient_id."','".$patient_dep_id."','".$superspeciality_id."','".$comments."','".$byassign."','".$toassign."','".$referdate."','".$refertime."','".$referclinic_name."')");
    $this->db->trans_complete();
    if ($this->db->trans_status() === FALSE)
    {
      return null;
    }
    else
    {
      return true;
    }
  }
}

