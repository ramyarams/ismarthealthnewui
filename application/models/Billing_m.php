<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
class Billing_m extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
	function get_billingdetails($laboratory_id)
	{
		$this->db->trans_start();
    // $query = $this->db->query("SELECT * FROM  `referral_confirmation_details` INNER JOIN  `laboratory_details` ON laboratory_details.laboratory_id = referral_confirmation_details.laboratory_id WHERE laboratory_details.laboratory_email =  '".$mail_id."'"); old one

		//$query = $this->db->query("(SELECT DISTINCT rcd.referred_id,rcd.medical_num,rpd.firstname, mailid, phonenum,rcd.refer_date, rpd.patient_unique_id, pa.firstname as phyfname FROM referral_patient_details rpd INNER JOIN referral_patient_test_details rptd ON rptd.patient_unique_id=rpd.patient_unique_id INNER JOIN laboratory_details ld ON ld.laboratory_id=rptd.laboratory_id INNER JOIN physician_appointment pa ON pa.physician_id=rptd.physician_id INNER JOIN referral_confirmation_details rcd ON rcd.medical_num=rptd.medical_num WHERE ld.laboratory_id='".$laboratory_id."' AND rcd.ref_type='I' ORDER BY rptd.date DESC) UNION (SELECT DISTINCT rcd.referred_id,rcd.medical_num,pdd.firstname, mailid, phonenum,rcd.refer_date,pdd.patient_unique_id, pa.firstname as phyfname FROM patient_dep_details pdd INNER JOIN referral_patient_test_details rptd ON rptd.patient_unique_id=pdd.patient_unique_id INNER JOIN laboratory_details ld ON ld.laboratory_id=rptd.laboratory_id INNER JOIN physician_appointment pa ON pa.physician_id=rptd.physician_id INNER JOIN referral_confirmation_details rcd ON rcd.medical_num=rptd.medical_num WHERE ld.laboratory_id='".$laboratory_id."' AND rcd.ref_type='I' ORDER BY rptd.date DESC)");

$query = $this->db->query("(SELECT DISTINCT rptd.labapproval_id,b.final_balance,b.balance_amt,rcd.referred_id,rcd.medical_num,rpd.firstname, mailid, phonenum,rcd.refer_date, rpd.patient_unique_id, pa.firstname as phyfname FROM referral_patient_details rpd INNER JOIN referral_patient_test_details rptd ON rptd.patient_unique_id=rpd.patient_unique_id INNER JOIN laboratory_details ld ON ld.laboratory_id=rptd.laboratory_id INNER JOIN physician_appointment pa ON pa.physician_id=rptd.physician_id INNER JOIN referral_confirmation_details rcd ON rcd.medical_num=rptd.medical_num LEFT JOIN billing b ON b.medical_num=rcd.medical_num WHERE ld.laboratory_id='".$laboratory_id."' AND rcd.ref_type='I' ORDER BY rptd.date DESC) UNION (SELECT DISTINCT rptd.labapproval_id,b.final_balance,b.balance_amt,rcd.referred_id,rcd.medical_num,pdd.firstname, mailid, phonenum,rcd.refer_date,pdd.patient_unique_id, pa.firstname as phyfname FROM patient_dep_details pdd INNER JOIN referral_patient_test_details rptd ON rptd.patient_unique_id=pdd.patient_unique_id INNER JOIN laboratory_details ld ON ld.laboratory_id=rptd.laboratory_id INNER JOIN physician_appointment pa ON pa.physician_id=rptd.physician_id INNER JOIN referral_confirmation_details rcd ON rcd.medical_num=rptd.medical_num LEFT JOIN billing b ON b.medical_num=rcd.medical_num WHERE ld.laboratory_id='".$laboratory_id."' AND rcd.ref_type='I' ORDER BY rptd.date DESC)");

		// $query = $this->db->query("(SELECT DISTINCT rcd.referred_id,rcd.medical_num,rpd.firstname, mailid, phonenum,rcd.refer_date, rpd.patient_unique_id, pa.firstname as phyfname FROM referral_patient_details rpd INNER JOIN referral_patient_test_details rptd ON rptd.patient_unique_id=rpd.patient_unique_id INNER JOIN laboratory_details ld ON ld.laboratory_id=rptd.laboratory_id INNER JOIN physician_appointment pa ON pa.physician_id=rptd.physician_id INNER JOIN referral_confirmation_details rcd ON rcd.medical_num=rptd.medical_num WHERE ld.laboratory_id='".$laboratory_id."' AND rptd.date >= DATE(NOW()) + INTERVAL -6 DAY AND rptd.date < NOW() + INTERVAL 0 DAY  ORDER BY rptd.date DESC) UNION (SELECT DISTINCT rcd.referred_id,rcd.medical_num,pdd.firstname, mailid, phonenum,rcd.refer_date,pdd.patient_unique_id, pa.firstname as phyfname FROM patient_dep_details pdd INNER JOIN referral_patient_test_details rptd ON rptd.patient_unique_id=pdd.patient_unique_id INNER JOIN laboratory_details ld ON ld.laboratory_id=rptd.laboratory_id INNER JOIN physician_appointment pa ON pa.physician_id=rptd.physician_id INNER JOIN referral_confirmation_details rcd ON rcd.medical_num=rptd.medical_num WHERE ld.laboratory_id='".$laboratory_id."' AND rptd.date >= DATE(NOW()) + INTERVAL -6 DAY AND rptd.date < NOW() + INTERVAL 0 DAY ORDER BY rptd.date DESC)");
		$this->db->trans_complete();
		if($query->num_rows()>=1)
			return $query->result();
	}

	function get_samplesbillingdetails($laboratory_id)
	{
		$this->db->trans_start();
    // $query = $this->db->query("SELECT * FROM  `referral_confirmation_details` INNER JOIN  `laboratory_details` ON laboratory_details.laboratory_id = referral_confirmation_details.laboratory_id WHERE laboratory_details.laboratory_email =  '".$mail_id."'"); old one

		$query = $this->db->query("(SELECT DISTINCT rptd.sample_collected_id,rcd.referred_id,rcd.medical_num,rpd.firstname, mailid, phonenum,rcd.refer_date, rpd.patient_unique_id, pa.firstname as phyfname FROM referral_patient_details rpd INNER JOIN referral_patient_test_details rptd ON rptd.patient_unique_id=rpd.patient_unique_id INNER JOIN laboratory_details ld ON ld.laboratory_id=rptd.laboratory_id INNER JOIN physician_appointment pa ON pa.physician_id=rptd.physician_id INNER JOIN referral_confirmation_details rcd ON rcd.medical_num=rptd.medical_num WHERE ld.laboratory_id='".$laboratory_id."' AND rcd.ref_type='I' AND rptd.billing_id=1 OR rptd.billing_id=2 ORDER BY rptd.date DESC) UNION (SELECT DISTINCT rptd.sample_collected_id,rcd.referred_id,rcd.medical_num,pdd.firstname, mailid, phonenum,rcd.refer_date,pdd.patient_unique_id, pa.firstname as phyfname FROM patient_dep_details pdd INNER JOIN referral_patient_test_details rptd ON rptd.patient_unique_id=pdd.patient_unique_id INNER JOIN laboratory_details ld ON ld.laboratory_id=rptd.laboratory_id INNER JOIN physician_appointment pa ON pa.physician_id=rptd.physician_id INNER JOIN referral_confirmation_details rcd ON rcd.medical_num=rptd.medical_num WHERE ld.laboratory_id='".$laboratory_id."' AND rcd.ref_type='I' AND rptd.billing_id=1  OR rptd.billing_id=2 ORDER BY rptd.date DESC)");

		// $query = $this->db->query("(SELECT DISTINCT rcd.referred_id,rcd.medical_num,rpd.firstname, mailid, phonenum,rcd.refer_date, rpd.patient_unique_id, pa.firstname as phyfname FROM referral_patient_details rpd INNER JOIN referral_patient_test_details rptd ON rptd.patient_unique_id=rpd.patient_unique_id INNER JOIN laboratory_details ld ON ld.laboratory_id=rptd.laboratory_id INNER JOIN physician_appointment pa ON pa.physician_id=rptd.physician_id INNER JOIN referral_confirmation_details rcd ON rcd.medical_num=rptd.medical_num WHERE ld.laboratory_id='".$laboratory_id."' AND rptd.date >= DATE(NOW()) + INTERVAL -6 DAY AND rptd.date < NOW() + INTERVAL 0 DAY  ORDER BY rptd.date DESC) UNION (SELECT DISTINCT rcd.referred_id,rcd.medical_num,pdd.firstname, mailid, phonenum,rcd.refer_date,pdd.patient_unique_id, pa.firstname as phyfname FROM patient_dep_details pdd INNER JOIN referral_patient_test_details rptd ON rptd.patient_unique_id=pdd.patient_unique_id INNER JOIN laboratory_details ld ON ld.laboratory_id=rptd.laboratory_id INNER JOIN physician_appointment pa ON pa.physician_id=rptd.physician_id INNER JOIN referral_confirmation_details rcd ON rcd.medical_num=rptd.medical_num WHERE ld.laboratory_id='".$laboratory_id."' AND rptd.date >= DATE(NOW()) + INTERVAL -6 DAY AND rptd.date < NOW() + INTERVAL 0 DAY ORDER BY rptd.date DESC)");
		$this->db->trans_complete();
		if($query->num_rows()>=1)
			return $query->result();
	}

	function get_reportbillingdetails($laboratory_id)
	{
		$this->db->trans_start();
    // $query = $this->db->query("SELECT * FROM  `referral_confirmation_details` INNER JOIN  `laboratory_details` ON laboratory_details.laboratory_id = referral_confirmation_details.laboratory_id WHERE laboratory_details.laboratory_email =  '".$mail_id."'"); old one

		$query = $this->db->query("(SELECT DISTINCT rcd.referred_id,rcd.medical_num,rpd.firstname, mailid, phonenum,rcd.refer_date, rpd.patient_unique_id, pa.firstname as phyfname FROM referral_patient_details rpd INNER JOIN referral_patient_test_details rptd ON rptd.patient_unique_id=rpd.patient_unique_id INNER JOIN laboratory_details ld ON ld.laboratory_id=rptd.laboratory_id INNER JOIN physician_appointment pa ON pa.physician_id=rptd.physician_id INNER JOIN referral_confirmation_details rcd ON rcd.medical_num=rptd.medical_num WHERE ld.laboratory_id='".$laboratory_id."' AND rcd.ref_type='I' AND rptd.sample_collected_id=1 ORDER BY rptd.date DESC) UNION (SELECT DISTINCT rcd.referred_id,rcd.medical_num,pdd.firstname, mailid, phonenum,rcd.refer_date,pdd.patient_unique_id, pa.firstname as phyfname FROM patient_dep_details pdd INNER JOIN referral_patient_test_details rptd ON rptd.patient_unique_id=pdd.patient_unique_id INNER JOIN laboratory_details ld ON ld.laboratory_id=rptd.laboratory_id INNER JOIN physician_appointment pa ON pa.physician_id=rptd.physician_id INNER JOIN referral_confirmation_details rcd ON rcd.medical_num=rptd.medical_num WHERE ld.laboratory_id='".$laboratory_id."' AND rcd.ref_type='I' AND rptd.sample_collected_id=1 ORDER BY rptd.date DESC)");

		// $query = $this->db->query("(SELECT DISTINCT rcd.referred_id,rcd.medical_num,rpd.firstname, mailid, phonenum,rcd.refer_date, rpd.patient_unique_id, pa.firstname as phyfname FROM referral_patient_details rpd INNER JOIN referral_patient_test_details rptd ON rptd.patient_unique_id=rpd.patient_unique_id INNER JOIN laboratory_details ld ON ld.laboratory_id=rptd.laboratory_id INNER JOIN physician_appointment pa ON pa.physician_id=rptd.physician_id INNER JOIN referral_confirmation_details rcd ON rcd.medical_num=rptd.medical_num WHERE ld.laboratory_id='".$laboratory_id."' AND rptd.date >= DATE(NOW()) + INTERVAL -6 DAY AND rptd.date < NOW() + INTERVAL 0 DAY  ORDER BY rptd.date DESC) UNION (SELECT DISTINCT rcd.referred_id,rcd.medical_num,pdd.firstname, mailid, phonenum,rcd.refer_date,pdd.patient_unique_id, pa.firstname as phyfname FROM patient_dep_details pdd INNER JOIN referral_patient_test_details rptd ON rptd.patient_unique_id=pdd.patient_unique_id INNER JOIN laboratory_details ld ON ld.laboratory_id=rptd.laboratory_id INNER JOIN physician_appointment pa ON pa.physician_id=rptd.physician_id INNER JOIN referral_confirmation_details rcd ON rcd.medical_num=rptd.medical_num WHERE ld.laboratory_id='".$laboratory_id."' AND rptd.date >= DATE(NOW()) + INTERVAL -6 DAY AND rptd.date < NOW() + INTERVAL 0 DAY ORDER BY rptd.date DESC)");
		$this->db->trans_complete();
		if($query->num_rows()>=1)
			return $query->result();
	}

	function getprofile($laboratory_id){
		$this->db->select("profile_filename,profile_fullpath");
		$this->db->from('laboratory_details');
		$this->db->where('laboratory_id',$laboratory_id);
		$query = $this->db->get();
		return $query->result();
	}

	function update_billing_status($patient_unique_id1=null,$medical_id1=null,$lab_testname=null,$billing_status=null)
{
  $this->db->set('billing_id', $billing_status); 
  $this->db->where('medical_num', $medical_id1);
  $this->db->where('ID', $lab_testname);
  $this->db->where('patient_unique_id', $patient_unique_id1);
  $this->db->update('referral_patient_test_details');
  if ($this->db->affected_rows() >0)
    return true;
  else
    return false;
}

	function update_partialbilling_status($patient_unique_id1=null,$medical_id1=null,$lab_testname=null,$billing_status=null)
{
  $this->db->set('billing_id', $billing_status); 
  $this->db->where('medical_num', $medical_id1);
  $this->db->where('ID', $lab_testname);
  $this->db->where('patient_unique_id', $patient_unique_id1);
  $this->db->update('referral_patient_test_details');

   $this->db->set('balance_amt', 0); 
  $this->db->where('medical_num', $medical_id1);
  $this->db->where('patient_unique_id', $patient_unique_id1);
  $this->db->update('billing');


  if ($this->db->affected_rows() >0)
    return true;
  else
    return false;
}


		function save_billing(){
			$laboratory_id=$_SESSION['laboratory_id'];
			if($this->input->post('balance_amt')==0.00){
			$this->db->set('billing_id', 2); 
	  		$this->db->where('medical_num', $this->input->post('medical_num'));
	        $this->db->where('patient_unique_id',  $this->input->post('patient_unique_id'));
	        $this->db->update('referral_patient_test_details');
		}
		else
		{
			$this->db->set('billing_id', 1); 
	  		$this->db->where('medical_num', $this->input->post('medical_num'));
	        $this->db->where('patient_unique_id',  $this->input->post('patient_unique_id'));
	        $this->db->update('referral_patient_test_details');
		}

		$this->db->trans_start();
		$query=$this->db->query("SELECT `unique_billid` from  billing where lab_id = '".$laboratory_id."' ORDER BY `billing_id` DESC LIMIT 1");
		$this->db->trans_complete();
		if($query->num_rows()>=1)
		{
			date_default_timezone_set('Asia/Kolkata'); # add your city to set local time zone
			$now = date('Y-m-d H:i:s');
			// $oub=$query->result()[0]->unique_billid;
			// $this->db->trans_start();
			// $ubd1=$oub+1;
			$date = date("Ym");
			// $unique_billid=$date.$ubd1;
			$old_unique_billid=$query->result()[0]->unique_billid;
			$this->db->trans_start();
			$unique_billid=$old_unique_billid+1;
			$query = $this->db->query("INSERT INTO `billing`(`laboratory_tests`,`tot_amt`,`discount`,`net_amt`,`adv_amt`,`balance_amt`,`patient_unique_id`,`medical_num`,`lab_id`,`unique_billid`,`created_on`) VALUES('".$this->input->post('laboratory_tests')."','".$this->input->post('tot_val')."','".$this->input->post('discount')."','".$this->input->post('net_amt')."','".$this->input->post('adv_amt')."','".$this->input->post('balance_amt')."','".$this->input->post('patient_unique_id')."','".$this->input->post('medical_num')."','".$laboratory_id."','".$unique_billid."','".$now."')");
			$this->db->trans_complete();
			return "Success";
		}
		else
		{
			date_default_timezone_set('Asia/Kolkata'); # add your city to set local time zone
			$now = date('Y-m-d H:i:s');
			$this->db->trans_start();	
			$date = date("Ym");
			$unique_billid=$date.'0001';	
			$query = $this->db->query("INSERT INTO `billing`(`laboratory_tests`,`tot_amt`,`discount`,`net_amt`,`adv_amt`,`balance_amt`,`patient_unique_id`,`medical_num`,`lab_id`,`unique_billid`,`created_on`) VALUES('".$this->input->post('laboratory_tests')."','".$this->input->post('tot_val')."','".$this->input->post('discount')."','".$this->input->post('net_amt')."','".$this->input->post('adv_amt')."','".$this->input->post('balance_amt')."','".$this->input->post('patient_unique_id')."','".$this->input->post('medical_num')."','".$laboratory_id."','".$unique_billid."','".$now."')");
			$this->db->trans_complete();
			return "Success";

		}
	}

	function savepartial(){
 			$this->db->set('balance_pymnt2', $this->input->post('adv_amt1')); 
	  		$this->db->where('billing_id', $this->input->post('bill_id'));
	        $this->db->update('billing');

 			$this->db->set('final_balance', $this->input->post('final_balance')); 
	  		$this->db->where('billing_id', $this->input->post('bill_id'));
	        $this->db->update('billing');

	        $this->db->set('billing_id', 2); 
  			$this->db->where('medical_num', $this->input->post('medical_id1'));
  			$this->db->update('referral_patient_test_details');
		return "Success";
		}

}


