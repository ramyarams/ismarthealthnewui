<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
class Upload_model extends CI_Model {

// this function will be called instantenneously

    public function __construct()
    {
        parent::__construct();

        $this->load->database();
    }

    public function get_path($file)
    {
         $this->db->trans_start();
         $query = $this->db->query("SELECT `report_fullpath` FROM `referral_patient_test_details` WHERE `report_filename` ='".$file."'");
         $this->db->trans_complete();
            if ($query->num_rows() > 0)    
            {
              return $query->result();
            }
    }

   function get_profilepath($laboratory_id){
    $this->db->select("profile_filename,profile_fullpath");
        $this->db->from('laboratory_details');
        $this->db->where('laboratory_details.laboratory_id',$laboratory_id);

        $query = $this->db->get();
        return $query->result();
  }

    public function upload_path($patient_unique_id1,$medical_id1,$lab_testname,$status,$uploadfilename){
       //create array to load to database
     $image_data = $this->upload->data();
   
     $insert_data = array(
        'report_filename' => $uploadfilename,                  
        'report_fullpath' => $image_data['full_path']
      );

           $this->db->where('medical_num', $medical_id1);
           $this->db->where('ID', $lab_testname);
           $this->db->where('patient_unique_id', $patient_unique_id1);
           $this->db->update('referral_patient_test_details', $insert_data);

    }

    function update_patientdetails($patient_id,$patient_firstname,$patient_lastname,$patient_gender,$patient_address,$state,$city,$pincode,$mailid){
   
      $data = array(
            'firstname' => $patient_firstname,
            'lastname' => $patient_lastname,
            'gender' => $patient_gender,
            'address'=> $patient_address,
            'state'=> $state,
            'city'=> $city,
            'pincode'=> $pincode,
            'mailid'=> $mailid
         );
  
      $this->db->where("referral_patient_id",$patient_id);
      $this->db->update("referral_patient_details",$data);

      if($this->db->affected_rows() >=0){
  return true; 
}else{
  return false; 
}
}


function update_patientfiledetails($patient_id,$patient_firstname,$patient_lastname,$patient_gender,$patient_address,$state,$city,$pincode,$mailid,$file_name,$full_path){
   
      $data = array(
            'firstname' => $patient_firstname,
            'lastname' => $patient_lastname,
            'gender' => $patient_gender,
            'address'=> $patient_address,
            'state'=> $state,
            'city'=> $city,
            'pincode'=> $pincode,
            'mailid'=> $mailid,
            'profile_filename' => $file_name,                  
            'profile_fullpath' => $full_path
         );
  
      $this->db->where("referral_patient_id",$patient_id);
      $this->db->update("referral_patient_details",$data);

      if($this->db->affected_rows() >=0){
  return true; 
}else{
  return false; 
}
}

function update_labdetails($laboratory_id,$laboratory_name,$laboratory_address,$laboratory_phone,$file_name,$full_path){

      $data = array(
            'laboratory_name' => $laboratory_name,
            'laboratory_address' => $laboratory_address,
            'laboratory_phone' => $laboratory_phone,
            'profile_filename' => $file_name,                  
            'profile_fullpath' => $full_path          
         );
  
      $this->db->where("laboratory_id",$laboratory_id);
      $this->db->update("laboratory_details",$data);

      if($this->db->affected_rows() >=0){
  return true; 
}else{
  return false; 
}
}

function update_physiciandetails($physician_firstname=null,$physician_lastname=null,$phone_num=null,$clinic_name=null,$clinic_address=null,$state=null,$city=null,$pincode=null,$file_name,$full_path){

      $data = array(
            'firstname' => $physician_firstname,
            'lastname' => $physician_lastname,
            'phone_num' => $phone_num,
            'pincode'=> $pincode,
            'clinic_name'=> $clinic_name,
            'address'=> $clinic_address,
            'city'=> $city,
            'state'=> $state,
            'updated_on'=>date("Y-m-d H:i:s"),
            'profile_filename' => $file_name,                  
            'profile_fullpath' => $full_path 
           );

    
      $this->db->where("mail_id",$_SESSION['mail_id']);
      $this->db->update("physician_appointment",$data);

  // $this->db->query("UPDATE physician_appointment SET firstname='".$physician_firstname."',lastname='".$physician_lastname."',phone_num='".$phone_num."',clinic_name='".$clinic_name."',address='".$."' where mail_id='".$_SESSION['mail_id']."'");

      if($this->db->affected_rows() >=0){
  return true; 
}else{
  return false; 
}
}

}