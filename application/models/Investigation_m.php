<?php

class Investigation_m extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}
	function get_echo_tmt_details(){
		// $this->db->distinct();
		$this->db->select("*");
        $this->db->from('investigation_test_details');
        $this->db->join('investigation_details','investigation_test_details.investigation_id=investigation_details.investigation_id');
        $this->db->where('investigation_test_details.investigation_id',6);

        $query = $this->db->get();
        return $query->result();
	}

	function get_ultrasound_details(){
		$this->db->select("*");
        $this->db->from('investigation_test_details');
        $this->db->join('investigation_details','investigation_test_details.investigation_id=investigation_details.investigation_id');
        $this->db->where('investigation_test_details.investigation_id',2);

        $query = $this->db->get();
        return $query->result();
	}

	function get_xray_details(){
		$this->db->select("*");
        $this->db->from('investigation_test_details');
        $this->db->join('investigation_details','investigation_test_details.investigation_id=investigation_details.investigation_id');
        $this->db->where('investigation_test_details.investigation_id',3);

        $query = $this->db->get();
        return $query->result();
	}

	function get_doppler_details(){
		$this->db->select("*");
		$this->db->from('investigation_test_details');
        $this->db->join('investigation_details','investigation_test_details.investigation_id=investigation_details.investigation_id');
        $this->db->where('investigation_test_details.investigation_id',7);

		$query = $this->db->get();
        return $query->result();
	}

	function get_ctscan_details(){
		$this->db->select("*");
		$this->db->from('investigation_test_details');
        $this->db->join('investigation_details','investigation_test_details.investigation_id=investigation_details.investigation_id');
        $this->db->where('investigation_test_details.investigation_id',8);

		$query = $this->db->get();
        return $query->result();
	}

	function get_mri_details(){
		$this->db->select("*");
		$this->db->from('investigation_test_details');
                $this->db->join('investigation_details','investigation_test_details.investigation_id=investigation_details.investigation_id');
                $this->db->where('investigation_test_details.investigation_id',1);
		$query = $this->db->get();
        return $query->result();
	}
	
	  function get_physio_details(){
       $this->db->select("*");
        $this->db->from('investigation_test_details');
        $this->db->join('investigation_details','investigation_test_details.investigation_id=investigation_details.investigation_id');
        $this->db->where('investigation_test_details.investigation_id',12);

        $query = $this->db->get();
        return $query->result();
    }

    function get_doctor_details(){

        $this->db->select("*");
        $this->db->from("physician_appointment");
        $query=$this->db->get();
        return $query->result();

    }
    
    	function get_ecg_details(){
		$this->db->select("*");
		$this->db->from('investigation_test_details');
        $this->db->join('investigation_details','investigation_test_details.investigation_id=investigation_details.investigation_id');
        $this->db->where('investigation_test_details.investigation_id',13);

		$query = $this->db->get();
        return $query->result();
	}

	function get_lab_details(){
		$this->db->select("*");
		$this->db->from('investigation_test_details');
        $this->db->join('investigation_details','investigation_test_details.investigation_id=investigation_details.investigation_id');
        $this->db->where('investigation_test_details.investigation_id',4);

		$query = $this->db->get();
        return $query->result();
	}

	function get_mri_details_add(){
		$this->db->select("*");
		$this->db->from('investigation_test_details');
         $this->db->join('investigation_details','investigation_test_details.investigation_id=investigation_details.investigation_id');
        $this->db->where("investigation_test_details.parse_id NOT IN (SELECT ltd.laboratory_tests FROM laboratory_test_details ltd INNER JOIN laboratory_details ld ON ltd.laboratory_id=ld.laboratory_id WHERE ld.laboratory_email='".$_SESSION['mail_id']."' AND ltd.status = 'ACTIVE')");
        $this->db->where('investigation_test_details.investigation_id',1);
		$query = $this->db->get();
        return $query->result();
    }

    function get_echo_tmt_details_add(){
		// $this->db->distinct();
		$this->db->select("*");
        $this->db->from('investigation_test_details');
        $this->db->join('investigation_details','investigation_test_details.investigation_id=investigation_details.investigation_id');
        $this->db->where("investigation_test_details.parse_id NOT IN (SELECT ltd.laboratory_tests FROM laboratory_test_details ltd INNER JOIN laboratory_details ld ON ltd.laboratory_id=ld.laboratory_id WHERE ld.laboratory_email='".$_SESSION['mail_id']."' AND ltd.status = 'ACTIVE')");
        $this->db->where('investigation_test_details.investigation_id',6);

        $query = $this->db->get();
        return $query->result();
	}

	function get_lab_details_add(){
		$this->db->select("*");
		$this->db->from('investigation_test_details');
        $this->db->join('investigation_details','investigation_test_details.investigation_id=investigation_details.investigation_id');
        $this->db->where("investigation_test_details.parse_id NOT IN (SELECT ltd.laboratory_tests FROM laboratory_test_details ltd INNER JOIN laboratory_details ld ON ltd.laboratory_id=ld.laboratory_id WHERE ld.laboratory_email='".$_SESSION['mail_id']."' AND ltd.status = 'ACTIVE')");
        $this->db->where('investigation_test_details.investigation_id',4);
		$query = $this->db->get();
        return $query->result();
	}

	function get_ultrasound_details_add(){
		$this->db->select("*");
        $this->db->from('investigation_test_details');
        $this->db->join('investigation_details','investigation_test_details.investigation_id=investigation_details.investigation_id');
        $this->db->where("investigation_test_details.parse_id NOT IN (SELECT ltd.laboratory_tests FROM laboratory_test_details ltd INNER JOIN laboratory_details ld ON ltd.laboratory_id=ld.laboratory_id WHERE ld.laboratory_email='".$_SESSION['mail_id']."' AND ltd.status = 'ACTIVE')");

        $this->db->where('investigation_test_details.investigation_id',2);

        $query = $this->db->get();
        return $query->result();
	}
function get_xray_details_add(){
		$this->db->select("*");
        $this->db->from('investigation_test_details');
        $this->db->join('investigation_details','investigation_test_details.investigation_id=investigation_details.investigation_id');
        $this->db->where("investigation_test_details.parse_id NOT IN (SELECT ltd.laboratory_tests FROM laboratory_test_details ltd INNER JOIN laboratory_details ld ON ltd.laboratory_id=ld.laboratory_id WHERE ld.laboratory_email='".$_SESSION['mail_id']."' AND ltd.status = 'ACTIVE')");

        $this->db->where('investigation_test_details.investigation_id',3);

        $query = $this->db->get();
        return $query->result();
	}

	function get_doppler_details_add(){
		$this->db->select("*");
		$this->db->from('investigation_test_details');
        $this->db->join('investigation_details','investigation_test_details.investigation_id=investigation_details.investigation_id');
        $this->db->where("investigation_test_details.parse_id NOT IN (SELECT ltd.laboratory_tests FROM laboratory_test_details ltd INNER JOIN laboratory_details ld ON ltd.laboratory_id=ld.laboratory_id WHERE ld.laboratory_email='".$_SESSION['mail_id']."' AND ltd.status = 'ACTIVE')");

        $this->db->where('investigation_test_details.investigation_id',7)
        ;

		$query = $this->db->get();
        return $query->result();
	}

	function get_ctscan_details_add(){
		$this->db->select("*");
		$this->db->from('investigation_test_details');
        $this->db->join('investigation_details','investigation_test_details.investigation_id=investigation_details.investigation_id');
        $this->db->where("investigation_test_details.parse_id NOT IN (SELECT ltd.laboratory_tests FROM laboratory_test_details ltd INNER JOIN laboratory_details ld ON ltd.laboratory_id=ld.laboratory_id WHERE ld.laboratory_email='".$_SESSION['mail_id']."' AND ltd.status = 'ACTIVE')");

        $this->db->where('investigation_test_details.investigation_id',8);

		$query = $this->db->get();
        return $query->result();
	}

        function get_physio_details_add(){
        $this->db->select("*");
        $this->db->from('investigation_test_details');
        $this->db->join('investigation_details','investigation_test_details.investigation_id=investigation_details.investigation_id');
        $this->db->where("investigation_test_details.parse_id NOT IN (SELECT ltd.laboratory_tests FROM laboratory_test_details ltd INNER JOIN laboratory_details ld ON ltd.laboratory_id=ld.laboratory_id WHERE ld.laboratory_email='".$_SESSION['mail_id']."')");

        $this->db->where('investigation_test_details.investigation_id',12);

        $query = $this->db->get();
        return $query->result();
    }

        function get_ecg_details_add(){
        $this->db->select("*");
        $this->db->from('investigation_test_details');
        $this->db->join('investigation_details','investigation_test_details.investigation_id=investigation_details.investigation_id');
        $this->db->where("investigation_test_details.parse_id NOT IN (SELECT ltd.laboratory_tests FROM laboratory_test_details ltd INNER JOIN laboratory_details ld ON ltd.laboratory_id=ld.laboratory_id WHERE ld.laboratory_email='".$_SESSION['mail_id']."')");

        $this->db->where('investigation_test_details.investigation_id',13);

        $query = $this->db->get();
        return $query->result();
    }


}
