<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
class Admin_m extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

    function get_adminname($physician_id)
  {
     $this->db->select("*");
  $this->db->from('phy_admin');
  $this->db->where('physician_id',$physician_id);
  
  $query = $this->db->get();
  return $query->result();
  }

	function insert_phyadmindetails($admin_name=null,$admin_phone_number=null,$admin_email=null,$admin_password=null,$phy_id=null){
    $this->db->select('username');
  $this->db->from('login_details');
  $this->db->where('username',$admin_email);
  $this->db->or_where('phone_num',$admin_phone_number);
  $query = $this->db->get();
  if($query->num_rows() > 0)
  {
    $this->db->query("UPDATE phy_admin SET phy_admin_mail_id='".$admin_email."' where phy_admin_mail_id='".$admin_email."'");
    return NULL;
  }
  else{
    
      $this->db->trans_start();
      $this->db->query("INSERT INTO `phy_admin`(`phy_admin_name`,`phy_admin_phone`,`phy_admin_mail_id`,`phy_admin_password`,`created_on`,`created_by`,`physician_id`) VALUES('".$admin_name."','".$admin_phone_number."','".$admin_email."','".md5($admin_password)."',now(),'".$_SESSION['firstname']."','".$phy_id."')");
       $this->db->trans_complete();
   
       if ($this->db->trans_status() === FALSE) return null;
   
       else
         return true;
     }
 
}

  function insert_confirmation_details($relationship=null,$referral_pat_id=null,$password=null,$patient_unique_id=null,$brief_history=null,$phy_advice=null){
    $physician_id=$_SESSION['physician_id'];
     date_default_timezone_set('Asia/Kolkata'); # add your city to set local time zone
$now = date('Y-m-d H:i:s');
  $this->db->query("INSERT INTO `referral_confirmation_details` (`relationship`, `referral_pat_id`, `password`, `patient_unique_id`, `brief_history`,`phy_advice`, `refer_date`,`created_by`,`login_id`) VALUES ('".$relationship."', '".$referral_pat_id."', '".$password."', '".$patient_unique_id."', '".$brief_history."','".$phy_advice."','".$now."','".$_SESSION['firstname']."','".$_SESSION['login_id']."')");
   return $this->db->insert_id();
}

function update_medicalnum($medical_num=null,$record_id=null)
{
$this->db->set('medical_num', $medical_num); //value that used to update column  
$this->db->where('referred_id', $record_id); //which row want to upgrade  
$this->db->update('referral_confirmation_details');
}
}

