		<div class="content-wrapper">
            <section class="content-header">
                <span style="color: #000;"><b style="font-size: 25px;">Welcome</b> Dr. Subrat Pal</span>
            </section>
            <section class="content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="box-body box-profile">
                                <img class="profile-user-img img-responsive img-circle" style="width:114px; margin-bottom:2%; border: 1px solid #004a75;" src="<?=base_url(); ?>Common_1/images/Subrat.png" alt="User profile picture">
                                <form class="form-horizontal">
                                    <div class="control-group">
                                        <div class="controls">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label class="control-label">First Name</label>
                                                    <input type="text" name="firstname" class="form-control" value="" id="firstname" placeholder="First Name">
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="control-label">Last Name</label>
                                                    <input type="text" name="lastname" class="form-control" value="" id="lastname" placeholder="Last Name">
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="control-label">Gender</label>
                                                    <div class="form-check">
                                                        <select class="form-control" id="gender" name="gender">
                                                            <option data-hidden="true" style="font-weight:700" value="0">Select</option>
                                                            <option value="1">Male</option>
                                                            <option value="2">Female</option>
                                                            <option value="3">Others</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <div class="controls">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label class="control-label">Age</label>
                                                    <input type="text" name="firstname" class="form-control" value="" id="firstname" placeholder="First Name">
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="control-label">Address</label>
                                                    <input type="text" name="firstname" class="form-control" value="" id="firstname" placeholder="First Name">
                                                </div>
                                                <div class="col-md-4">
                                                    <label for="current_password" class="control-label">State</label>
                                                    <div class="form-check">
                                                        <select class="form-control" id="state" name="state">
                                                            <option data-hidden="true" style="font-weight:700" value="0">Select</option>
                                                            <option value="1">Karnataka</option>
                                                            <option value="2">Tamilnadu</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <div class="controls">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label class="control-label">City</label>
                                                    <div class="form-check">
                                                        <select class="form-control" id="city" name="city">
                                                            <option data-hidden="true" style="font-weight:700" value="0">Select</option>
                                                            <option value="1">Bangalore</option>
                                                            <option value="2">Chennai</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <label for="current_password" class="control-label">Postal Code</label>
                                                    <input type="text" name="zip" class="form-control" value="" id="zip" placeholder="Postal Code">
                                                </div>
                                                <div class="col-md-4">
                                                    <label for="current_password" class="control-label">Email Address</label>
                                                    <input type="text" name="zip" class="form-control" value="" id="zip" placeholder="Postal Code">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <div class="controls">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label for="current_password" class="control-label">Phone Number</label>
                                                    <input type="text" name="zip" class="form-control" value="" id="zip" placeholder="Postal Code">
                                                </div>
                                                <div class="col-md-4">
                                                    <label for="current_password" class="control-label">Upload Photo</label>
                                                    <input type="file" name="userfile" size="20" class="form-control editfield">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="text-align:right; margin-top:1%;">
                                        <div class="col-md-12">
                                            <button type="button" class="btn btn-success" data-dismiss="modal">Submit</button>
                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>