	<div class="content-wrapper">
            <section class="content-header">
                <div class="login-logo">
                    <span style="color: #000;"><b>Test</b> Reports</span>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li>Patient Lookup</li>
                </ol>
            </section>
            <section class="content">
                <div class="box box-primary">
                    <div class="box-body">
                        <a href="PatientSearch.html" id="link"><img type="image" src="<?=base_url(); ?>Common_1/Images/Back.png" alt="Submit"></a>
                        <div class="row">
                            <div class="col-md-4 thumb">
                                <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                                   data-image="<?=base_url(); ?>Common_1/Images/lights.jpg"
                                   data-target="#image-gallery">
                                    <div class="caption">
                                        <p>24 Hours Urine Protine</p>
                                        <img type="image" src="<?=base_url(); ?>Common_1/Images/Download.png" class="downloadbtn" alt="Submit">
                                    </div>
                                    <img class="img-thumbnail"
                                         src="<?=base_url(); ?>Common_1/Images/lights.jpg"
                                         alt="Another alt text">
                                </a>
                            </div>
                            <div class="col-md-4 thumb">
                                <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                                   data-image="<?=base_url(); ?>Common_1/Images/nature.jpg"
                                   data-target="#image-gallery">
                                    <div class="caption">
                                        <p>Absolute Eosinophil Count</p>
                                        <img type="image" src="<?=base_url(); ?>Common_1/Images/Download.png" class="downloadbtn" alt="Submit">
                                    </div>
                                    <img class="img-thumbnail"
                                         src="<?=base_url(); ?>Common_1/Images/nature.jpg"
                                         alt="Another alt text">
                                </a>
                            </div>
                            <div class="col-md-4 thumb">
                                <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                                   data-image="<?=base_url(); ?>Common_1/Images/floods.jpg"
                                   data-target="#image-gallery">
                                    <div class="caption">
                                        <p>Activated Partial Thromboplastin Time Panel</p>
                                        <img type="image" src="<?=base_url(); ?>Common_1/Images/Download.png" class="downloadbtn" alt="Submit">
                                    </div>
                                    <img class="img-thumbnail"
                                         src="<?=base_url(); ?>Common_1/Images/floods.jpg"
                                         alt="Another alt text">
                                </a>
                            </div>
                        </div>
                        <hr style="margin-top: 2px;" />
                        <div class="row">
                            <div class="col-xs-12" style="text-align: right;">
                                <button type="submit" class="btn btn-primary">Download</button>
                            </div>
                        </div>
                        <div class="modal fade" id="image-gallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-md">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="image-gallery-title"></h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                    </div>
                                    <div class="modal-body">
                                        <img id="image-gallery-image" class="img-responsive col-md-12" src="" style="float: none!important;">
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary float-left" id="show-previous-image">
                                            <i class="fa fa-arrow-left"></i>
                                        </button>
                                        <button type="button" id="show-next-image" class="btn btn-secondary float-right">
                                            <i class="fa fa-arrow-right"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>