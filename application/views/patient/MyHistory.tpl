 <div class="content-wrapper">
            <section class="content-header">
                <div class="login-logo">
                    <span style="color: #000;"><b>Lab</b> Investigation Records</span>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li>Patient Lookup</li>
                </ol>
            </section>
            <section class="content">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th class="no-sort">Prid</th>
                                        <th class="no-sort">Patient Name</th>
                                        <th class="no-sort">Phone</th>
                                        <th class="no-sort">Age</th>
                                        <th>Relationship</th>
                                        <th class="no-sort">Reper Date</th>
                                        <th class="no-sort">Reports</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>P0016767</td>
                                        <td>Subrat Pal</td>
                                        <td>9036809357</td>
                                        <td>30</td>
                                        <td>Brother</td>
                                        <td>20-05-1989</td>
                                        <td><a data-title="view" data-toggle="modal" data-target="#view"><img type="image" src="<?=base_url(); ?>Common_1/Images/Eye.png" alt="Submit"></a></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="view" tabindex="-1" role="dialog" aria-labelledby="view" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                <h4 class="modal-title custom_align" id="Heading">Test Details</h4>
                            </div>
                            <div class="modal-body">
                                <form id="registrationForm" method="post" class="form-horizontal">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="form-horizontal">
                                                <div class="table-responsive">
                                                    <table id="example1" class="table table-bordered table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th style="width: 10px">#</th>
                                                                <th>Tests</th>
                                                                <th>Date</th>
                                                                <th>Status</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>1</td>
                                                                <td>Doppler & Ultrasound scrotum</td>
                                                                <td>12-11-2018</td>
                                                                <td><a href="<?=base_url(); ?>Patient/ViewTestReports" id="link"><img type="image" src="<?=base_url(); ?>Common_1/Images/Eye.png" alt="Submit"></a></td>
                                                            </tr>
                                                            <tr>
                                                                <td>1</td>
                                                                <td>Doppler & Ultrasound scrotum</td>
                                                                <td>12-11-2018</td>
                                                                <td><a href="<?=base_url(); ?>Patient/ViewTestReports" id="link"><img type="image" src="<?=base_url(); ?>Common_1/Images/Eye.png" alt="Submit"></a></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
		  