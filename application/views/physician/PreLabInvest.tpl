﻿        <div class="content-wrapper">
            <section class="content-header">
                <div class="login-logo">
                    <span style="color: #000;"><b>Previous</b> Lab Investigation</span>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li>Patient Lookup</li>
                </ol>
            </section>
            <section class="content">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-3 form-group">
                                <a href="PatientSearch.html" id="link"><img type="image" src="../Common_1/Images/Back.png" alt="Submit"></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 form-group">
                                <label>Start Date</label>
                                <input type="text" id="inputEmail" class="form-control" required autofocus>
                            </div>
                            <div class="col-md-3 form-group">
                                <label>End Date</label>
                                <input type="text" id="inputEmail" class="form-control" required autofocus>
                            </div>
                            <div class="col-md-3 form-group">
                                <label>Select Lab</label>
                                <div class="form-check">
                                    <select class="form-control" id="state" name="state">
                                        <option data-hidden="true" style="font-weight:700" value="0">Select Lab</option>
                                        <option value="1">All</option>
                                        <option value="2">iNet Labs</option>
                                        <option value="3">Prema Labs</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3 form-group">
                                <button type="submit" class="btn btn-success" style="margin-top:24px;">Submit</button>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th width="8%">Referral ID</th>
                                        <th width="15%">Patient Name</th>
                                        <th width="8%">Refer Date</th>
                                        <th width="15%">Lab</th>
                                        <th>Test Name</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1000000</td>
                                        <td>Subrat Kumar Pal</td>
                                        <td>2018-10-11</td>
                                        <td>Inet labs</td>
                                        <td>Anti Streptolysin O Antibody, Ultrasound thyroid</td>
                                    </tr>
                                    <tr>
                                        <td>1000000</td>
                                        <td>Subrat Kumar Pal</td>
                                        <td>2018-10-11</td>
                                        <td>Inet labs</td>
                                        <td>Doppler both arms arterial,Ultra sound cranium,Doppler & Ultrasound scrotum,Doppler both arms venous,Ultrasound abdomen & pelvis</td>
                                    </tr>
                                    <tr>
                                        <td>1000000</td>
                                        <td>Subrat Kumar Pal</td>
                                        <td>2018-10-11</td>
                                        <td>Inet labs</td>
                                        <td>Ultrasound endorectal(N), US Chest ,Ultrasound abdomen & pelvis,Ultrasound musculoskeletal system ,Ultrasound Follicular Study ,Ultrasound Early pregnancy scan,Ultrasound thyroid ,Ultra sound cranium,Ultrasound guided FNAC ,Ultrasound endovaginal(N) ,Ultrasound Breast ,Ultrasound neck ,Ultrasound guided aspiration(N)</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <p>
                            Previous site visit:
                            <strong>09-11-2018 10:20:38</strong>
                        </p>
                    </div>
                </div>
                <div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="add" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                <h4 class="modal-title custom_align" id="Heading">Test Details</h4>
                            </div>
                            <div class="modal-body">
                                <form id="registrationForm" method="post" class="form-horizontal">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="form-horizontal">
                                                <div class="table-responsive">
                                                    <table id="example1" class="table table-bordered table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th style="width: 10px">#</th>
                                                                <th>Tests</th>
                                                                <th>Date</th>
                                                                <th>Status</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>1</td>
                                                                <td>Doppler & Ultrasound scrotum</td>
                                                                <td>12-11-2018</td>
                                                                <td><a href="ViewTestReports.html" id="link"><img type="image" src="../Common_1/Images/Eye.png" alt="Submit"></a></td>
                                                            </tr>
                                                            <tr>
                                                                <td>1</td>
                                                                <td>Doppler & Ultrasound scrotum</td>
                                                                <td>12-11-2018</td>
                                                                <td><a href="ViewTestReports.html" id="link"><img type="image" src="../Common_1/Images/Eye.png" alt="Submit"></a></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#example1').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': false,
                'info': true,
                'autoWidth': false,
                'lengthMenu': [[5, 10, 15, 25, 50, 100, -1], [5, 10, 15, 25, 50, 100, "All"]],
                'pageLength': 5,
                columnDefs: [{
                    orderable: false,
                    targets: "no-sort"
                }]
            });
        });
    </script>