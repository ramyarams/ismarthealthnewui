﻿
        <div class="content-wrapper">
            <section class="content-header">
                <div class="login-logo">
                    <span style="color: #000;"><b>Test</b> Reports</span>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li>Patient Lookup</li>
                </ol>
            </section>
            <section class="content">
                <div class="box box-primary">
                    <div class="box-body">
                        <a href="<?=base_url(); ?>Physician/PatientSearch" id="link"><img type="image" src="<?=base_url(); ?>Common_1/Images/Back.png" alt="Submit"></a>
                        <div class="row">
                            <div class="col-md-4 thumb">
                                <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                                   data-image="<?=base_url(); ?>Common_1/Images/lights.jpg"
                                   data-target="#image-gallery">
                                    <div class="caption">
                                        <p>24 Hours Urine Protine</p>
                                        <img type="image" src="<?=base_url(); ?>Common_1/Images/Download.png" class="downloadbtn" alt="Submit">
                                    </div>
                                    <img class="img-thumbnail"
                                         src="<?=base_url(); ?>Common_1/Images/lights.jpg"
                                         alt="Another alt text">
                                </a>
                            </div>
                            <div class="col-md-4 thumb">
                                <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                                   data-image="<?=base_url(); ?>Common_1/Images/nature.jpg"
                                   data-target="#image-gallery">
                                    <div class="caption">
                                        <p>Absolute Eosinophil Count</p>
                                        <img type="image" src="<?=base_url(); ?>Common_1/Images/Download.png" class="downloadbtn" alt="Submit">
                                    </div>
                                    <img class="img-thumbnail"
                                         src="<?=base_url(); ?>Common_1/Images/nature.jpg"
                                         alt="Another alt text">
                                </a>
                            </div>
                            <div class="col-md-4 thumb">
                                <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                                   data-image="<?=base_url(); ?>Common_1/Images/floods.jpg"
                                   data-target="#image-gallery">
                                    <div class="caption">
                                        <p>Activated Partial Thromboplastin Time Panel</p>
                                        <img type="image" src="<?=base_url(); ?>Common_1/Images/Download.png" class="downloadbtn" alt="Submit">
                                    </div>
                                    <img class="img-thumbnail"
                                         src="<?=base_url(); ?>Common_1/Images/floods.jpg"
                                         alt="Another alt text">
                                </a>
                            </div>
                        </div>
                        <hr style="margin-top: 2px;" />
                        <div class="row">
                            <div class="col-xs-12" style="text-align: right;">
                                <button type="submit" class="btn btn-primary">Download</button>
                            </div>
                        </div>
                        <div class="modal fade" id="image-gallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-md">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="image-gallery-title"></h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                    </div>
                                    <div class="modal-body">
                                        <img id="image-gallery-image" class="img-responsive col-md-12" src="" style="float: none!important;">
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary float-left" id="show-previous-image">
                                            <i class="fa fa-arrow-left"></i>
                                        </button>
                                        <button type="button" id="show-next-image" class="btn btn-secondary float-right">
                                            <i class="fa fa-arrow-right"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    <script src="<?=base_url(); ?>Common_1/Js/jquery.min.js"></script>
    <script>
        let modalId = $('#image-gallery');

        $(document)
          .ready(function () {

              loadGallery(true, 'a.thumbnail');

              //This function disables buttons when needed
              function disableButtons(counter_max, counter_current) {
                  $('#show-previous-image, #show-next-image')
                    .show();
                  if (counter_max === counter_current) {
                      $('#show-next-image')
                        .hide();
                  } else if (counter_current === 1) {
                      $('#show-previous-image')
                        .hide();
                  }
              }

              /**
               *
               * @param setIDs        Sets IDs when DOM is loaded. If using a PHP counter, set to false.
               * @param setClickAttr  Sets the attribute for the click handler.
               */

              function loadGallery(setIDs, setClickAttr) {
                  let current_image,
                    selector,
                    counter = 0;

                  $('#show-next-image, #show-previous-image')
                    .click(function () {
                        if ($(this)
                          .attr('id') === 'show-previous-image') {
                            current_image--;
                        } else {
                            current_image++;
                        }

                        selector = $('[data-image-id="' + current_image + '"]');
                        updateGallery(selector);
                    });

                  function updateGallery(selector) {
                      let $sel = selector;
                      current_image = $sel.data('image-id');
                      $('#image-gallery-title')
                        .text($sel.data('title'));
                      $('#image-gallery-image')
                        .attr('src', $sel.data('image'));
                      disableButtons(counter, $sel.data('image-id'));
                  }

                  if (setIDs == true) {
                      $('[data-image-id]')
                        .each(function () {
                            counter++;
                            $(this)
                              .attr('data-image-id', counter);
                        });
                  }
                  $(setClickAttr)
                    .on('click', function () {
                        updateGallery($(this));
                    });
              }
          });

        // build key actions
        $(document)
          .keydown(function (e) {
              switch (e.which) {
                  case 37: // left
                      if ((modalId.data('bs.modal') || {})._isShown && $('#show-previous-image').is(":visible")) {
                          $('#show-previous-image')
                            .click();
                      }
                      break;

                  case 39: // right
                      if ((modalId.data('bs.modal') || {})._isShown && $('#show-next-image').is(":visible")) {
                          $('#show-next-image')
                            .click();
                      }
                      break;

                  default:
                      return; // exit this handler for other keys
              }
              e.preventDefault(); // prevent the default action (scroll / move caret)
          });

    </script>
</body>
</html>