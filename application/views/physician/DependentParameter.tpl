﻿        <div class="content-wrapper">
            <section class="content-header">
                <div class="login-logo">
                    <span style="color: #000;"><b>Dependent</b> Parameters</span>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li>Patient Lookup</li>
                </ol>
            </section>
            <section class="content">
                <div class="row">
                    <div class="col-md-5">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title"><b>General Parameters</b></h3>
                                <a class="btn btn-success btn-xs" style="border-radius:50%; margin-right:5px;" data-title="Edit" data-toggle="modal" data-target="#edit"><span class="fa fa-pencil"></span></a>
                                <a class="btn btn-danger btn-xs" style="border-radius:50%; margin-right:5px;" href="<?=base_url(); ?>Physician/PatientDependent"><span class="fa fa-remove"></span></a>
                            </div>
                            <div class="box-body">
                                <table class="table table-bordered">
                                    <tr>
                                        <th>Name</th>
                                        <td>Subrat Kumar Pal</td>
                                    </tr>
                                    <tr>
                                        <th>Age / Sex</th>
                                        <td>30</td>
                                    </tr>
                                    <tr>
                                        <th>Email Address</th>
                                        <td>Subrat.Pal@inetframe.com</td>
                                    </tr>
                                    <tr>
                                        <th>Phonenum</th>
                                        <td>9036809357</td>
                                    </tr>
                                    <tr>
                                        <th>Address</th>
                                        <td>Marathalli, Bangalore</td>
                                    </tr>
                                    <tr>
                                        <th>Date Of Birth</th>
                                        <td>20-05-1989</td>
                                    </tr>
                                    <tr>
                                        <th>Known Allergies</th>
                                        <td>No</td>
                                    </tr>
                                    <tr>
                                        <th>Previous Medical History</th>
                                        <td>NA</td>
                                    </tr>
                                    <tr>
                                        <th>Blood Group</th>
                                        <td>O-ve</td>
                                    </tr>
                                    <tr>
                                        <th>Diabetic</th>
                                        <td>NA</td>
                                    </tr>
                                    <tr>
                                        <th>Hypertension</th>
                                        <td>NA</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title"><b>Clinical Parameters</b></h3><small> (Most Recent at the Top)</small>
                                <a class="btn btn-primary btn-xs" style="border-radius:50%; margin-right:5px;" data-title="Add" data-toggle="modal" data-target="#add"><span class="fa fa-plus"></span></a>
                            </div>
                            <div class="box-body">
                                <div class="table-responsive">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th style="width: 10px">#</th>
                                                <th>Blood pressure</th>
                                                <th>Sugar</th>
                                                <th>Pulse</th>
                                                <th>Weight</th>
                                                <th>Temperature</th>
                                                <th>Date</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>80/120</td>
                                                <td>140mg/Dl</td>
                                                <td>88bpm</td>
                                                <td>68Kg</td>
                                                <td>90F</td>
                                                <td>5-11-2018</td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>80/120</td>
                                                <td>140mg/Dl</td>
                                                <td>88bpm</td>
                                                <td>68Kg</td>
                                                <td>90F</td>
                                                <td>5-11-2018</td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>80/120</td>
                                                <td>140mg/Dl</td>
                                                <td>88bpm</td>
                                                <td>68Kg</td>
                                                <td>90F</td>
                                                <td>5-11-2018</td>
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td>80/120</td>
                                                <td>140mg/Dl</td>
                                                <td>88bpm</td>
                                                <td>68Kg</td>
                                                <td>90F</td>
                                                <td>5-11-2018</td>
                                            </tr>
                                            <tr>
                                                <td>5</td>
                                                <td>80/120</td>
                                                <td>140mg/Dl</td>
                                                <td>88bpm</td>
                                                <td>68Kg</td>
                                                <td>90F</td>
                                                <td>5-11-2018</td>
                                            </tr>
                                            <tr>
                                                <td>6</td>
                                                <td>80/120</td>
                                                <td>140mg/Dl</td>
                                                <td>88bpm</td>
                                                <td>68Kg</td>
                                                <td>90F</td>
                                                <td>5-11-2018</td>
                                            </tr>
                                            <tr>
                                                <td>7</td>
                                                <td>80/120</td>
                                                <td>140mg/Dl</td>
                                                <td>88bpm</td>
                                                <td>68Kg</td>
                                                <td>90F</td>
                                                <td>5-11-2018</td>
                                            </tr>
                                            <tr>
                                                <td>8</td>
                                                <td>80/120</td>
                                                <td>140mg/Dl</td>
                                                <td>88bpm</td>
                                                <td>68Kg</td>
                                                <td>90F</td>
                                                <td>5-11-2018</td>
                                            </tr>
                                            <tr>
                                                <td>9</td>
                                                <td>80/120</td>
                                                <td>140mg/Dl</td>
                                                <td>88bpm</td>
                                                <td>68Kg</td>
                                                <td>90F</td>
                                                <td>5-11-2018</td>
                                            </tr>
                                            <tr>
                                                <td>10</td>
                                                <td>80/120</td>
                                                <td>140mg/Dl</td>
                                                <td>88bpm</td>
                                                <td>68Kg</td>
                                                <td>90F</td>
                                                <td>5-11-2018</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="add" aria-hidden="true">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="btn-xs close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                    <h4 class="modal-title custom_align" id="Heading">Add Clinical Parameters</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="blood_pressure">Systolic Blood Pressure</label>
                                        <input type="number" id="blood_pressure" placeholder="Systolic Blood Pressure" class="form-control" name="blood_pressure">
                                    </div>
                                    <div class="form-group">
                                        <label for="blood_pressure">Diastolic Blood Pressure</label>
                                        <input type="number" id="blood_pressure" placeholder="Diastolic Blood Pressure" class="form-control" name="blood_pressure">
                                    </div>
                                    <div class="form-group">
                                        <label for="blood_pressure">Sugar</label>
                                        <input type="number" id="blood_pressure" placeholder="Sugar" class="form-control" name="blood_pressure">
                                    </div>
                                    <div class="form-group">
                                        <label for="blood_pressure">Pulse</label>
                                        <input type="number" id="blood_pressure" placeholder="Pulse" class="form-control" name="blood_pressure">
                                    </div>
                                    <div class="form-group">
                                        <label for="blood_pressure">Height</label>
                                        <input type="number" id="blood_pressure" placeholder="Height" class="form-control" name="blood_pressure">
                                    </div>
                                    <div class="form-group">
                                        <label for="blood_pressure">Weight</label>
                                        <input type="number" id="blood_pressure" placeholder="Weight" class="form-control" name="blood_pressure">
                                    </div>
                                    <div class="form-group">
                                        <label for="blood_pressure">Temperature (in Farenheit)</label>
                                        <input type="number" id="blood_pressure" placeholder="Temperature" class="form-control" name="blood_pressure">
                                    </div>
                                    <div class="box-footer" style="text-align:center;">
                                        <button type="button" class="btn btn-success">Submit</button>
                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                    <h4 class="modal-title custom_align" id="Heading">Edit General Parameters</h4>
                                </div>
                                <div class="modal-body">
                                    <form id="registrationForm" method="post" class="form-horizontal">
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                <div class="form-horizontal">
                                                    <div class="control-group">
                                                        <div class="controls">
                                                            <div class="row">
                                                                <div class="col-lg-6 col-sm-6">
                                                                    <label class="control-label">Date of Birth</label>
                                                                    <input type="date" name="dateofbirth" class="form-control" value="" id="dob" placeholder="dd-mm-yyyy">
                                                                </div>
                                                                <div class="col-lg-6 col-sm-6">
                                                                    <label class="control-label">Blood Group</label>
                                                                    <select class="form-control removedisable" name="bloodgroup" id="b_group">
                                                                        <option data-hidden="true" style="font-weight:700" value="0">Select</option>
                                                                        <option value="1">A RhD positive (A+)</option>
                                                                        <option value="2">A RhD negative (A-)</option>
                                                                        <option value="3">B RhD positive (B+)</option>
                                                                        <option value="4">B RhD negative (B-)</option>
                                                                        <option value="5">O RhD positive (O+)</option>
                                                                        <option value="6">O RhD negative (O-)</option>
                                                                        <option value="7">AB RhD positive (AB+)</option>
                                                                        <option value="8">AB RhD negative (AB-)</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div><br />
                                                    <div class="control-group">
                                                        <label for="new_password" class="control-label">Previous Medical History</label>
                                                        <div class="controls">
                                                            <div class="row">
                                                                <div class="col-lg-12 col-sm-12">
                                                                    <textarea class="form-control editfield" rows="3" cols="12" name="medical_history" id="prv_medicalhistory"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div><br />
                                                    <div class="control-group">
                                                        <div class="controls">
                                                            <div class="row">
                                                                <div class="col-lg-6 col-sm-6">
                                                                    <label for="current_password" class="control-label editfield">Known Allergies</label>
                                                                    <input type="text" class="form-control editfield" id="inputallergy" name="allergy">
                                                                </div>
                                                                <div class="col-lg-3 col-sm-3">
                                                                    <label for="current_password" class="control-label">Diabetic?</label>
                                                                    <div class="form-check">
                                                                        <select class="form-control" id="diabetic" name="diabetic">
                                                                            <option data-hidden="true" style="font-weight:700" value="2">Select</option>
                                                                            <option value="1">Yes</option>
                                                                            <option value="0">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3 col-sm-3">
                                                                    <label for="new_password" class="control-label">Hypertension?</label>
                                                                    <div class="form-check">
                                                                        <select class="form-control" id="hypertension" name="hypertension">
                                                                            <option data-hidden="true" style="font-weight:700" value="2">Select</option>
                                                                            <option value="1">Yes</option>
                                                                            <option value="0">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br />
                                                    <div class="control-group">
                                                        <div class="controls">
                                                            <div class="row">
                                                                <div class="col-lg-6 col-sm-6">
                                                                    <label class="control-label">Diabetic Date</label>
                                                                    <input type="month" name="dateofbirth" class="form-control" value="" id="dob" placeholder="mm-yyyy">
                                                                </div>
                                                                <div class="col-lg-6 col-sm-6">
                                                                    <label class="control-label">Hypertension Date</label>
                                                                    <input type="month" name="dateofbirth" class="form-control" value="" id="dob" placeholder="mm-yyyy">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="box-footer" style="text-align:center;">
                                            <button type="button" class="btn btn-success">Submit</button>
                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
 