﻿        <div class="content-wrapper">
            <section class="content-header">
                <div class="login-logo">
                    <span style="color: #000;"><b>Dependent</b> Details</span>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li>Patient Lookup</li>
                </ol>
            </section>
            <section class="content">
                <!--Main Content Starts-->
                <div class="box box-primary">
                    <div class="box-body">
                        <a href="PatientSearch.html" id="link"><img type="image" src="../Common_1/Images/Back.png" alt="Submit"></a>
                        <a class="btn btn-primary btn-sm pull-right" data-title="Add" data-toggle="modal" data-target="#add">Add</a>
                        <div class="table-responsive">

                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email Address</th>
                                        <th>Phone</th>
                                        <th>Alt. Phone</th>
                                        <th>Gender</th>
                                        <th>Age</th>
                                        <th>Relationship</th>
                                        <th class="no-sort" width="112px">Refer For Diagnosis</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><a class="btn btn-primary btn-xs" style="border-radius:50%; margin-right:5px;" href="<?=base_url(); ?>Physician/DependentParameter"><span class="glyphicon glyphicon-plus"></span></a>Ramesh Chandra Pal</td>
                                        <td>Subrat.Pal@inetframe.com</td>
                                        <td>9036809351</td>
                                        <td>9036809350</td>
                                        <td>Male</td>
                                        <td>60</td>
                                        <td>Father</td>
                                        <td><a data-title="test" data-toggle="modal" data-target="#test" id="link"><img type="image" src="../Common_1/Images/Refer4Dignosis.png" alt="Submit"></a></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <p>
                            Previous site visit:
                            <strong>09-11-2018 10:20:38</strong>
                        </p>
                    </div>
                </div>
                <!--Main Content Ends-->
                <!--Add a Dependent Starts-->
                <div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="add" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="btn-xs close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                <h4 class="modal-title custom_align" id="Heading">Add Dependent</h4>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="blood_pressure">Name</label>
                                    <input type="text" id="name" placeholder="Name" class="form-control" name="name">
                                </div>
                                <div class="form-group">
                                    <label for="blood_pressure">Age</label>
                                    <input type="text" id="age" placeholder="Age" class="form-control" name="age">
                                </div>
                                <div class="form-group">
                                    <label for="blood_pressure">Alternate Phone Number</label>
                                    <input type="text" id="phone" placeholder="Alternate Phone Number" class="form-control" name="phone">
                                </div>
                                <div class="form-group">
                                    <label for="blood_pressure">Gender</label>
                                    <div class="form-check">
                                        <select class="form-control" id="gender" name="gender">
                                            <option data-hidden="true" style="font-weight:700" value="0">Select</option>
                                            <option value="1">Male</option>
                                            <option value="2">Female</option>
                                            <option value="3">Others</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="blood_pressure">Relationship</label>
                                    <div class="form-check">
                                        <select class="form-control" id="relation" name="relation">
                                            <option value="">Select a Relationship</option>
                                            <option value="2">Father</option>
                                            <option value="3">Mother</option>
                                            <option value="7">Brother</option>
                                            <option value="6">Sister</option>
                                            <option value="4">Son</option>
                                            <option value="5">Daughter</option>
                                            <option value="9">Husband</option>
                                            <option value="8">Wife</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="box-footer" style="text-align:center;">
                                    <button type="button" class="btn btn-success">Submit</button>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Add a New Dependent Ends-->
                <!--Select Test category Starts-->
                <div class="modal fade" id="test" tabindex="-1" role="dialog" aria-labelledby="test" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                <h4 class="modal-title custom_align" id="Heading">Type of Diagnosis</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12 form-group">
                                        <label>Select one Option</label>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" data-title="selectTest" data-toggle="modal" data-target="#selectTest" data-dismiss="modal">
                                                Laboratory Tests
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2" data-title="superSpeciality" data-toggle="modal" data-target="#superSpeciality" data-dismiss="modal">
                                                Super Speciality Consultation
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="optionsRadios" id="optionsRadios3" value="option3" data-title="package" data-toggle="modal" data-target="#package" data-dismiss="modal">
                                                Packages
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-footer" style="text-align:center;">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Select Test category Ends-->
                <!--Select Test Type Starts-->
                <div class="modal fade" id="selectTest" tabindex="-1" role="dialog" aria-labelledby="selectTest" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                <h4 class="modal-title custom_align" id="Heading">Available Tests</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="dual-list list-left col-md-5">
                                        <div class="well text-right">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="input-group">
                                                        <input type="text" name="SearchDualList" class="form-control" placeholder="search" />
                                                        <span class="input-group-addon glyphicon glyphicon-search" style="top: 0px;"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <ul class="list-group">
                                                <li class="list-group-item">A</li>
                                                <li class="list-group-item">B</li>
                                                <li class="list-group-item">C</li>
                                                <li class="list-group-item">D</li>
                                                <li class="list-group-item">E</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="list-arrows col-md-1 text-center">
                                        <button class="btn btn-default btn-sm move-left">
                                            <span class="glyphicon glyphicon-chevron-left"></span>
                                        </button>
                                        <button class="btn btn-default btn-sm move-right">
                                            <span class="glyphicon glyphicon-chevron-right"></span>
                                        </button>
                                    </div>
                                    <div class="dual-list list-right col-md-5">
                                        <div class="well">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="input-group">
                                                        <input type="text" name="SearchDualList" class="form-control" placeholder="search" />
                                                        <span class="input-group-addon glyphicon glyphicon-search" style="top: 0px;"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <ul class="list-group">
                                                <li class="list-group-item">F</li>
                                                <li class="list-group-item">G</li>
                                                <li class="list-group-item">H</li>
                                                <li class="list-group-item">I</li>
                                                <li class="list-group-item">J</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-footer" style="text-align:center;">
                                    <a class="btn btn-success" href="LabDetails.html">Submit</a>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Select Test Type Ends-->
                <!--Select Super Speciality Starts-->
                <div class="modal fade" id="superSpeciality" tabindex="-1" role="dialog" aria-labelledby="superSpeciality" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                <h4 class="modal-title custom_align" id="Heading">Super Speciality</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12 form-group">
                                        <div class="form-check">
                                            <select class="form-control" id="gender" name="gender">
                                                <option data-hidden="true" style="font-weight:700" value="0">Select Specialization</option>
                                                <option value="1">Plastic Surgery</option>
                                                <option value="2">Psychiatry</option>
                                                <option value="3">Pulmonology</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 form-group">
                                        <div class="form-check">
                                            <select class="form-control" id="gender" name="gender">
                                                <option data-hidden="true" style="font-weight:700" value="0">Select Specialist</option>
                                                <option value="1">Subrat</option>
                                                <option value="2">Srinibas</option>
                                                <option value="3">Vaidya</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 form-group">
                                        <input type="text" name="firstname" class="form-control" value="" id="firstname" placeholder="Phone" readonly="readonly">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 form-group">
                                        <input type="text" name="firstname" class="form-control" value="" id="firstname" placeholder="Email" readonly="readonly">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 form-group">
                                        <input type="text" name="firstname" class="form-control" value="" id="firstname" placeholder="State" readonly="readonly">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 form-group">
                                        <input type="text" name="firstname" class="form-control" value="" id="firstname" placeholder="City" readonly="readonly">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 form-group">
                                        <input type="date" name="firstname" class="form-control" value="" id="firstname" placeholder="DD-MM-YYYY">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 form-group">
                                        <input type="time" name="firstname" class="form-control" value="" id="firstname" placeholder="DD-MM-YYYY">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 form-group">
                                        <textarea type="text" name="firstname" class="form-control" value="" id="firstname" placeholder="Comments" rows="3"></textarea>
                                    </div>
                                </div>
                                <div class="box-footer" style="text-align:center;">
                                    <button type="button" class="btn btn-success" data-title="selectTest" data-toggle="modal" data-target="#selectTest" data-dismiss="modal">Submit</button>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Select Super Speciality Ends-->
                <!--Select Package Starts-->
                <div class="modal fade" id="package" tabindex="-1" role="dialog" aria-labelledby="test" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                <h4 class="modal-title custom_align" id="Heading">Available Packages</h4>
                            </div>
                            <div class="modal-body">
                                <form id="registrationForm" method="post" class="form-horizontal">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="form-horizontal">
                                                <div class="table-responsive">
                                                    <table id="example1" class="table table-bordered table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th style="width: 10%">#</th>
                                                                <th style="width: 20%">Package name</th>
                                                                <th style="width: 25%">No of test contains packages</th>
                                                                <th>Tests details</th>
                                                                <th style="width: 10%">Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>1</td>
                                                                <td>Sample Package</td>
                                                                <td>7</td>
                                                                <td>Urine Culture & Sensitivity,Urine Electrolytes (Na, K, Cl) Panel,urine osmolality,WBC Count,Weil Felix Test,Widal Test,Zinc Ser/Plas (Quantitative)</td>
                                                                <td><a data-title="selectPackageLab" data-toggle="modal" data-target="#selectPackageLab" data-dismiss="modal" id="link"><img type="image" src="../Common_1/Images/Proceed.png" alt="Submit"></a></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Select Package Ends-->
                <!--Select Package Lab Starts-->
                <div class="modal fade" id="selectPackageLab" tabindex="-1" role="dialog" aria-labelledby="selectPackageLab" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                <h4 class="modal-title custom_align" id="Heading">Available Lab for this package (health package)</h4>
                            </div>
                            <div class="modal-body">
                                <form id="registrationForm" method="post" class="form-horizontal">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="form-horizontal">
                                                <div class="table-responsive">
                                                    <table id="example1" class="table table-bordered table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th style="width: 10%">#</th>
                                                                <th style="width: 20%">Lab name</th>
                                                                <th style="width: 25%">Lab Address</th>
                                                                <th>Package Price</th>
                                                                <th style="width: 10%">Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>1</td>
                                                                <td>Prema Labs</td>
                                                                <td>Avalahalli, Anjanapura Post, Tillak Nagar, Kanakapura Road,Opp to Govt Urdu School, Bangalore, Karnataka</td>
                                                                <td>3</td>
                                                                <td><a data-title="selectedPackage" data-toggle="modal" data-target="#selectedPackage" data-dismiss="modal" id="link"><img type="image" src="../Common_1/Images/Proceed.png" alt="Submit"></a></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Select Package Lab Ends-->
                <!--Select Package Lab Starts-->
                <div class="modal fade" id="selectedPackage" tabindex="-1" role="dialog" aria-labelledby="selectedPackage" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                <h4 class="modal-title custom_align" id="Heading">Package has been added</h4>
                            </div>
                            <div class="modal-body">
                                Package has been added
                            </div>
                        </div>
                    </div>
                </div>
                <!--Select Package Lab Ends-->
  
    <script type="text/javascript">
        $(document).ready(function () {
            $('#example1').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false,
                'lengthMenu': [[5, 10, 15, 25, 50, 100, -1], [5, 10, 15, 25, 50, 100, "All"]],
                'pageLength': 5,
                columnDefs: [{
                    orderable: false,
                    targets: "no-sort"
                }]
            });
        });
    </script>
