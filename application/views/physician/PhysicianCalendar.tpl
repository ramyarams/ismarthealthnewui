﻿        <div class="content-wrapper">
            <section class="content-header">
                <div class="login-logo">
                    <span style="color: #000;"><b>Madhavan</b> Calander</span>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li>Patient Lookup</li>
                </ol>
            </section>
            <section class="content">
                <div class="box box-primary">
                    <div class="box-body no-padding">
                        <div class="row" style="text-align:right; margin-top:1%; margin-right:3px;">
                            <div class="col-md-12">
                                <button class="btn btn-primary" data-toggle="modal" data-target="#adhocmodal">Add</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div id="calendar"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <div class="modal fade" id="adhocmodal" tabindex="-1" role="dialog" aria-labelledby="adhocmodal" aria-hidden="true">
                <div class="modal-dialog modal-md">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                            <h4 class="modal-title custom_align" id="Heading">Doctor Leaves</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group row">
                                <div class="col-xs-6">
                                    <label for="ex1">From date</label>
                                    <input class="form-control" id="adhocfromdate" name="adhocfromdate" type="date">
                                </div>
                                <div class="col-xs-6">
                                    <label for="ex2">To date</label>
                                    <input class="form-control" id="adhoctodate" name="adhoctodate" type="date">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-xs-10">
                                    <input type="checkbox" class="forlimitedtime" id="forlimitedtime" name="forlimitedtime">
                                    <label for="forlimitedtime">But available for sometime?</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-xs-6">
                                    <label for="ex1">From time</label>
                                    <input class="form-control" type="time">
                                </div>
                                <div class="col-xs-6">
                                    <label for="ex2">To time</label>
                                    <input class="form-control" type="time">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-xs-12">
                                    <label for="ex1">Reason</label>
                                    <textarea class="form-control reasonforadhoc" id="reasonforadhoc"></textarea>
                                </div>
                            </div>
                            <div class="text-center">
                                <button class="btn btn-success">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       
    <script src="<?=base_url(); ?>Common_1/Js/jquery.min.js"></script>
    <script src="<?=base_url(); ?>Common_1/Js/moment.js"></script>
    <script src="<?=base_url(); ?>Common_1/Js/fullcalendar.min.js"></script>
    <script>
        $(function () {
            function init_events(ele) {
                ele.each(function () {
                    var eventObject = {
                        title: $.trim($(this).text())
                    }
                    $(this).data('eventObject', eventObject)
                    $(this).draggable({
                        zIndex: 1070,
                        revert: true,
                        revertDuration: 0
                    })

                })
            }
            init_events($('#external-events div.external-event'))
            var date = new Date()
            var d = date.getDate(),
                m = date.getMonth(),
                y = date.getFullYear()
            $('#calendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                buttonText: {
                    today: 'today',
                    month: 'month',
                    week: 'week',
                    day: 'day'
                },
            })
        })
    </script>
</body>
</html>