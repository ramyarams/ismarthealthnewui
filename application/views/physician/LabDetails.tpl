﻿<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>iSmart Health</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="../Common_1/Css/bootstrap.min.css">
    <link rel="stylesheet" href="../Common_1/Css/font-awesome.min.css">
    <link rel="stylesheet" href="../Common_1/Css/ionicons.min.css">
    <link rel="stylesheet" href="../Common_1/Css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="../Common_1/Css/AdminLTE.min.css">
    <link rel="stylesheet" href="../Common_1/Css/_all-skins.min.css">
    <link rel="shortcut icon" href="../Common_1/images/Favicon.ico" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <style>
        @media screen and (max-width: 992px) {
            .main-header .sidebar-toggle {
                float: left;
                background-color: transparent;
                background-image: none;
                padding: 15px 15px;
                font-family: fontAwesome;
            }
        }

        .close {
            font-size: 12px;
        }

        .skin-blue .sidebar-menu > li.header {
            color: #fff;
            background: #004a75;
            font-size: 14px;
        }

        .skin-blue .sidebar a {
            color: #3f51ae;
        }

        .skin-blue .sidebar-menu > li > a:hover {
            background-color: #3c8dbc !important;
        }

        .skin-blue .sidebar-menu .treeview-menu > li > a {
            color: #3f51ae;
        }

        .skin-blue .sidebar-menu > li > .treeview-menu {
            margin: 0 1px;
            background: #fff;
        }

        .skin-blue .sidebar-menu > li.menu-open > a {
            color: #fff;
            background: #3c8dbc;
        }

        .skin-blue .sidebar-menu > li > .treeview-menu > li > a:hover {
            background-color: #3c8dbc !important;
        }

        .no-sort::after {
            display: none !important;
        }

        .no-sort {
            pointer-events: none !important;
            cursor: default !important;
        }

        table#example1 {
            font-size: 13px !important;
        }

        .table > tbody > tr > td {
            padding: 8px;
        }

        .dual-list .list-group {
            margin-top: 8px;
        }

        .list-left li, .list-right li {
            cursor: pointer;
        }

        .list-arrows {
            padding-top: 100px;
        }

            .list-arrows button {
                margin-bottom: 20px;
            }

        .list-group-item {
            border-radius: 0px !important;
        }
    </style>
</head>
<body class="hold-transition skin-blue fixed sidebar-mini">
    <div class="wrapper">
        <header class="main-header">
            <a href="#" class="logo" style="border-bottom: 1px solid #e5e9f1;">
                <span class="logo-mini" style="margin-top: 6px;"><b>i</b>SH</span>
                <span class="logo-lg"><img src="../Common_1/Images/logo.png" alt="Logo"></span>
            </a>
            <nav class="navbar navbar-static-top">
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <li class="dropdown user user-menu" style="top: 5px;">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="../Common_1/Images/Subrat.png" class="user-image" alt="User Image">
                                <span class="hidden-xs">Dr. Subrat Pal</span>
                            </a>
                            <ul class="dropdown-menu" style="width: 200px;">
                                <li class="user-header">
                                    <img src="../Common_1/Images/Subrat.png" class="img-circle" alt="User Image">
                                    <p>
                                        Dr. Subrat Pal
                                        <small>General Physian</small>
                                    </p>
                                </li>
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="#" class="btn btn-success btn-flat">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="#" class="btn btn-danger btn-flat">Logout</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <aside class="main-sidebar" style="background-color: #f9fafc;">
            <section class="sidebar">
                <div class="user-panel">
                    <div class="pull-left image">
                        <img src="../Common_1/Images/Subrat.png" class="img-circle" alt="User Image">
                    </div>
                    <div class="pull-left info" style="color: #3F51B5;">
                        <p>Dr. Subrat Pal</p><small>General Physician</small>
                    </div>
                </div>
                <ul class="sidebar-menu" data-widget="tree">
                    <li class="header">Main Navigation</li>
                    <li><a href="PatientSearch.html"><i class="fa fa-th"></i> <span>Patient Look-Up</span></a></li>
                    <li><a href="PrevSupSpecCons.html"><i class="fa fa-th"></i> <span>Previous Super Speciality <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Consultation</span></a></li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-th"></i><span>Quick Links</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="PreLabInvest.html"><i class="fa fa-circle-o"></i> Previous Lab Investigation</a></li>
                            <li><a href="PhysicianTiming.html"><i class="fa fa-circle-o"></i> Physician Timings</a></li>
                            <li><a href="PhysicianCalendar.html"><i class="fa fa-circle-o"></i> Physician Calendar</a></li>
                            <li><a href="AdminRegister.html"><i class="fa fa-circle-o"></i> Add Admin</a></li>
                            <li><a href="ChangePassword.html"><i class="fa fa-circle-o"></i> Change Password</a></li>
                            <li><a href="#"><i class="fa fa-circle-o"></i> Tech Support</a></li>
                        </ul>
                    </li>
                </ul>
            </section>
        </aside>
        <div class="content-wrapper">
            <section class="content-header">
                <div class="login-logo">
                    <span style="color: #000;"><b>Laboratory</b> Details</span>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li>Patient Lookup</li>
                </ol>
            </section>
            <section class="content">
                <!--Main Content Starts-->
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th class="no-sort">Lab Name</th>
                                        <th class="no-sort">Phone</th>
                                        <th class="no-sort">Address</th>
                                        <th class="no-sort">Test Name</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <a href="TestDateSelection.html">Inet labs</a>
                                        </td>
                                        <td>9036809357</td>
                                        <td>Jayanagar</td>
                                        <td>MRI BRAIN PLAIN</td>
                                     </tr>
                                    <tr>
                                        <td>
                                            <a href="TestDateSelection.html">Prema labs</a>
                                        </td>
                                        <td>8970194916</td>
                                        <td>Avalahalli, Anjanapura Post, Tillak Nagar, Kanakapura Road,Opp to Govt Urdu School	</td>
                                        <td>MRI BRAIN PLAIN</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                     </div>
                </div>
            </section>
        </div>
        <footer class="main-footer">
            <div class="row">
                <div class="col-md-6">
                    Copyright 2018 © <strong><a href="#">iSmartHealth</a></strong> All rights reserved.
                </div>
                <div class="col-md-6" style="text-align:right;">
                    <span class="glyphicon glyphicon-phone" aria-hidden="true"></span>&nbsp;&nbsp;+91 7618702727&nbsp;&nbsp;&nbsp;&nbsp;
                    <span class="glyphicon glyphicon-phone-alt" aria-hidden="true"></span>&nbsp;&nbsp;080 26971320&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="mailto:sales@inetframe.com?subject=iSmartClinic.in"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>&nbsp;&nbsp;sales@inetframe.com</a>
                </div>
            </div>
        </footer>
        <div class="control-sidebar-bg"></div>
    </div>
    <script src="../Common_1/Js/jquery.min.js"></script>
    <script src="../Common_1/Js/bootstrap.min.js"></script>
    <script src="../Common_1/Js/jquery.dataTables.min.js"></script>
    <script src="../Common_1/Js/dataTables.bootstrap.min.js"></script>
    <script src="../Common_1/Js/jquery.slimscroll.min.js"></script>
    <script src="../Common_1/Js/adminlte.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#example1').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false,
                'lengthMenu': [[5, 10, 15, 25, 50, 100, -1], [5, 10, 15, 25, 50, 100, "All"]],
                'pageLength': 5,
                columnDefs: [{
                    orderable: false,
                    targets: "no-sort"
                }]
            });
        });
    </script>
</body>
</html>