﻿        <div class="content-wrapper">
            <section class="content-header">
                <div class="login-logo">
                    <span style="color: #000;"><b>Dr. Madhavan</b> working time details</span>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li>Patient Lookup</li>
                </ol>
            </section>
            <section class="content">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">Dr. Madhavan working time details</div>
                                    <div class="panel-body">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th> Day </th>
                                                    <th> Work Type &amp; Timings </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Sunday</td>
                                                    <td>
                                                        <div class="row">
                                                            <div class="col-md-10">
                                                                <select class="sunday_working_type form-control" rel="sunday" onchange="checkholiday('sunday')">
                                                                    <option value="WORKING_DAY" class="workingday" rel="WORKING_DAY">Working day</option>
                                                                    <option value="HOLI_DAY" class="holiday" rel="HOLI_DAY" selected="">Holiday</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <button class="btn btn-primary btn-xs" data-title="Add New" data-toggle="modal" data-target="#add">
                                                                    <span class="glyphicon glyphicon-plus"></span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <br>
                                                        <div class="row" style="margin-bottom: 5px;">
                                                            <div class="col-md-4">
                                                                <input type="text" placeholder="Clinic name" class="form-control">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <input type="time" placeholder="Opening time" class="form-control">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <input type="time" placeholder="Closing time" class="form-control">
                                                            </div>
                                                            <div class="col-md-2">
                                                                <button class="btn btn-success btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit">
                                                                    <span class="glyphicon glyphicon-pencil"></span>
                                                                </button>
                                                                <button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete">
                                                                    <span class="glyphicon glyphicon-trash"></span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Monday</td>
                                                    <td>
                                                        <div class="row">
                                                            <div class="col-md-10">
                                                                <select class="sunday_working_type form-control" rel="sunday" onchange="checkholiday('sunday')">
                                                                    <option value="WORKING_DAY" class="workingday" rel="WORKING_DAY">Working day</option>
                                                                    <option value="HOLI_DAY" class="holiday" rel="HOLI_DAY" selected="">Holiday</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <button class="btn btn-primary btn-xs" data-title="Add New" data-toggle="modal" data-target="#add">
                                                                    <span class="glyphicon glyphicon-plus"></span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <br>
                                                        <div class="row" style="margin-bottom: 5px;">
                                                            <div class="col-md-4">
                                                                <input type="text" placeholder="Clinic name" class="form-control">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <input type="time" placeholder="Opening time" class="form-control">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <input type="time" placeholder="Closing time" class="form-control">
                                                            </div>
                                                            <div class="col-md-2">
                                                                <button class="btn btn-success btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit">
                                                                    <span class="glyphicon glyphicon-pencil"></span>
                                                                </button>
                                                                <button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete">
                                                                    <span class="glyphicon glyphicon-trash"></span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Tuesday</td>
                                                    <td>
                                                        <div class="row">
                                                            <div class="col-md-10">
                                                                <select class="sunday_working_type form-control" rel="sunday" onchange="checkholiday('sunday')">
                                                                    <option value="WORKING_DAY" class="workingday" rel="WORKING_DAY">Working day</option>
                                                                    <option value="HOLI_DAY" class="holiday" rel="HOLI_DAY" selected="">Holiday</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <button class="btn btn-primary btn-xs" data-title="Add New" data-toggle="modal" data-target="#add">
                                                                    <span class="glyphicon glyphicon-plus"></span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <br>
                                                        <div class="row" style="margin-bottom: 5px;">
                                                            <div class="col-md-4">
                                                                <input type="text" placeholder="Clinic name" class="form-control">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <input type="time" placeholder="Opening time" class="form-control">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <input type="time" placeholder="Closing time" class="form-control">
                                                            </div>
                                                            <div class="col-md-2">
                                                                <button class="btn btn-success btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit">
                                                                    <span class="glyphicon glyphicon-pencil"></span>
                                                                </button>
                                                                <button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete">
                                                                    <span class="glyphicon glyphicon-trash"></span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Wednesday</td>
                                                    <td>
                                                        <div class="row">
                                                            <div class="col-md-10">
                                                                <select class="sunday_working_type form-control" rel="sunday" onchange="checkholiday('sunday')">
                                                                    <option value="WORKING_DAY" class="workingday" rel="WORKING_DAY">Working day</option>
                                                                    <option value="HOLI_DAY" class="holiday" rel="HOLI_DAY" selected="">Holiday</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <button class="btn btn-primary btn-xs" data-title="Add New" data-toggle="modal" data-target="#add">
                                                                    <span class="glyphicon glyphicon-plus"></span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <br>
                                                        <div class="row" style="margin-bottom: 5px;">
                                                            <div class="col-md-4">
                                                                <input type="text" placeholder="Clinic name" class="form-control">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <input type="time" placeholder="Opening time" class="form-control">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <input type="time" placeholder="Closing time" class="form-control">
                                                            </div>
                                                            <div class="col-md-2">
                                                                <button class="btn btn-success btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit">
                                                                    <span class="glyphicon glyphicon-pencil"></span>
                                                                </button>
                                                                <button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete">
                                                                    <span class="glyphicon glyphicon-trash"></span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Thursday</td>
                                                    <td>
                                                        <div class="row">
                                                            <div class="col-md-10">
                                                                <select class="sunday_working_type form-control" rel="sunday" onchange="checkholiday('sunday')">
                                                                    <option value="WORKING_DAY" class="workingday" rel="WORKING_DAY">Working day</option>
                                                                    <option value="HOLI_DAY" class="holiday" rel="HOLI_DAY" selected="">Holiday</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <button class="btn btn-primary btn-xs" data-title="Add New" data-toggle="modal" data-target="#add">
                                                                    <span class="glyphicon glyphicon-plus"></span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <br>
                                                        <div class="row" style="margin-bottom: 5px;">
                                                            <div class="col-md-4">
                                                                <input type="text" placeholder="Clinic name" class="form-control">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <input type="time" placeholder="Opening time" class="form-control">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <input type="time" placeholder="Closing time" class="form-control">
                                                            </div>
                                                            <div class="col-md-2">
                                                                <button class="btn btn-success btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit">
                                                                    <span class="glyphicon glyphicon-pencil"></span>
                                                                </button>
                                                                <button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete">
                                                                    <span class="glyphicon glyphicon-trash"></span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Friday</td>
                                                    <td>
                                                        <div class="row">
                                                            <div class="col-md-10">
                                                                <select class="sunday_working_type form-control" rel="sunday" onchange="checkholiday('sunday')">
                                                                    <option value="WORKING_DAY" class="workingday" rel="WORKING_DAY">Working day</option>
                                                                    <option value="HOLI_DAY" class="holiday" rel="HOLI_DAY" selected="">Holiday</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <button class="btn btn-primary btn-xs" data-title="Add New" data-toggle="modal" data-target="#add">
                                                                    <span class="glyphicon glyphicon-plus"></span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <br>
                                                        <div class="row" style="margin-bottom: 5px;">
                                                            <div class="col-md-4">
                                                                <input type="text" placeholder="Clinic name" class="form-control">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <input type="time" placeholder="Opening time" class="form-control">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <input type="time" placeholder="Closing time" class="form-control">
                                                            </div>
                                                            <div class="col-md-2">
                                                                <button class="btn btn-success btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit">
                                                                    <span class="glyphicon glyphicon-pencil"></span>
                                                                </button>
                                                                <button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete">
                                                                    <span class="glyphicon glyphicon-trash"></span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Saturday</td>
                                                    <td>
                                                        <div class="row">
                                                            <div class="col-md-10">
                                                                <select class="sunday_working_type form-control" rel="sunday" onchange="checkholiday('sunday')">
                                                                    <option value="WORKING_DAY" class="workingday" rel="WORKING_DAY">Working day</option>
                                                                    <option value="HOLI_DAY" class="holiday" rel="HOLI_DAY" selected="">Holiday</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <button class="btn btn-primary btn-xs" data-title="Add New" data-toggle="modal" data-target="#add">
                                                                    <span class="glyphicon glyphicon-plus"></span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <br>
                                                        <div class="row" style="margin-bottom: 5px;">
                                                            <div class="col-md-4">
                                                                <input type="text" placeholder="Clinic name" class="form-control">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <input type="time" placeholder="Opening time" class="form-control">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <input type="time" placeholder="Closing time" class="form-control">
                                                            </div>
                                                            <div class="col-md-2">
                                                                <button class="btn btn-success btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit">
                                                                    <span class="glyphicon glyphicon-pencil"></span>
                                                                </button>
                                                                <button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete">
                                                                    <span class="glyphicon glyphicon-trash"></span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" class="text-center">
                                                        <b>Average time span for each patient</b> <br>
                                                        <select class="timespan form-control" rel="timespan">
                                                            <option value="600">10 Minutes</option>
                                                            <option value="900" selected="">15 Minutes</option>
                                                            <option value="1200">20 Minutes</option>
                                                            <option value="1800">30 Minutes</option>
                                                            <option value="2700">45 Minutes</option>
                                                            <option value="3600">1 Hour</option>
                                                            <option value="4500">1 hour and 15 minutes</option>
                                                            <option value="5400">1 hour and 30 minutes</option>
                                                            <option value="6300">1 hour and 45 minutes</option>
                                                            <option value="7200">2 Hours</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                <h4 class="modal-title custom_align" id="Heading">Edit Clinic and Time for Tuesday</h4>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label>Clinic Name</label>
                                    <input class="form-control" type="text" placeholder="Madhavan Clinic">
                                </div>
                                <div class="form-group">
                                    <label>Opening Time</label>
                                    <input class="form-control" type="time" placeholder="Madhavan Clinic">
                                </div>
                                <div class="form-group">
                                    <label>Closing Time</label>
                                    <input class="form-control" type="time" placeholder="Madhavan Clinic">
                                </div>
                                <div class="modal-footer ">
                                    <button type="button" class="btn btn-success pull-right">Update</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="add" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                <h4 class="modal-title custom_align" id="Heading">Add new clinic and time for Saturday</h4>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label>Clinic Name</label>
                                    <input class="form-control" type="text" placeholder="Madhavan Clinic">
                                </div>
                                <div class="form-group">
                                    <label>Opening Time</label>
                                    <input class="form-control" type="time" placeholder="Madhavan Clinic">
                                </div>
                                <div class="form-group">
                                    <label>Closing Time</label>
                                    <input class="form-control" type="time" placeholder="Madhavan Clinic">
                                </div>
                                <div class="modal-footer ">
                                    <button type="button" class="btn btn-success pull-right">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                <h4 class="modal-title custom_align" id="Heading">Delete this entry</h4>
                            </div>
                            <div class="modal-body">
                                <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Are you sure you want to delete this Record?</div>
                            </div>
                            <div class="modal-footer ">
                                <button type="button" class="btn btn-success">Yes</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
       