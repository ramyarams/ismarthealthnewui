﻿       <div class="content-wrapper">
            <section class="content-header">
                <div class="login-logo">
                    <span style="color: #000;"><b>Previous</b> Investigation</span>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li>Patient Lookup</li>
                </ol>
            </section>
            <section class="content">
                <div class="box box-primary">
                    <div class="box-body">
                        <a href="PatientSearch.html" id="link"><img type="image" src="../Common_1/Images/Back.png" alt="Submit"></a>
                        <div class="table-responsive">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Referral ID</th>
                                        <th>Patient Name</th>
                                        <th>Email Address</th>
                                        <th>Age</th>
                                        <th>Relationship</th>
                                        <th>Tests</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1000000</td>
                                        <td>Subrat Kumar Pal</td>
                                        <td>pal.subratkumar@gmail.com</td>
                                        <td>30</td>
                                        <td>Self</td>
                                        <td><img type="image" src="../Common_1/Images/Eye.png" alt="Submit" data-title="Add" data-toggle="modal" data-target="#add"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <p>
                            Previous site visit:
                            <strong>09-11-2018 10:20:38</strong>
                        </p>
                    </div>
                </div>
                <div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="add" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                <h4 class="modal-title custom_align" id="Heading">Test Details</h4>
                            </div>
                            <div class="modal-body">
                                <form id="registrationForm" method="post" class="form-horizontal">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="form-horizontal">
                                                <div class="table-responsive">
                                                    <table id="example1" class="table table-bordered table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th style="width: 10px">#</th>
                                                                <th>Tests</th>
                                                                <th>Date</th>
                                                                <th>Status</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>1</td>
                                                                <td>Doppler & Ultrasound scrotum</td>
                                                                <td>12-11-2018</td>
                                                                <td><a href="<?=base_url(); ?>Physician/ViewTestReports" id="link"><img type="image" src="../Common_1/Images/Eye.png" alt="Submit"></a></td>
                                                            </tr>
                                                            <tr>
                                                                <td>1</td>
                                                                <td>Doppler & Ultrasound scrotum</td>
                                                                <td>12-11-2018</td>
                                                                <td><a href="<?=base_url(); ?>Physician/ViewTestReports" id="link"><img type="image" src="../Common_1/Images/Eye.png" alt="Submit"></a></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
  
    <script type="text/javascript">
        $(document).ready(function () {
            $('#example1').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false,
                'lengthMenu': [[5, 10, 15, 25, 50, 100, -1], [5, 10, 15, 25, 50, 100, "All"]],
                'pageLength': 5,
                columnDefs: [{
                    orderable: false,
                    targets: "no-sort"
                }]
            });
        });
    </script>
