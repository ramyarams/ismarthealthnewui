﻿        <div class="content-wrapper">
            <section class="content-header">
                <span style="color: #000;"><b style="font-size: 25px;">Welcome</b> Dr. Subrat Pal</span>
            </section>
            <section class="content">
                <div class="row">
                    <div class="col-md-4">
                        <div class="box box-primary">
                            <div class="box-body box-profile">
                                <div class="image-upload">
                                    <label for="file-input">
                                        <img src="../Common_1/Images/Camera.png" style="position: absolute; top: 20%; right: 30%; width:40px;" />
                                    </label>
                                    <input id="file-input" type="file" />
                                </div>
                                <img class="profile-user-img img-responsive img-circle" style="width:114px;border: 1px solid #004a75;" src="../Common_1/images/Subrat.png" alt="User profile picture">
                                <h3 class="profile-username text-center">Subrat Pal</h3>
                                <p class="text-muted text-center">General Physician</p>
                                <ul class="list-group list-group-unbordered">
                                    <li class="list-group-item">
                                        <b>Registration Number</b> <a class="pull-right">1234567</a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Phone Number</b> <a class="pull-right">8970194916</a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Email</b> <a class="pull-right">Subrat.Pal@inetframe.com</a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Specialization</b> <a class="pull-right">Cardiology/Cardiac Science</a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Degree</b> <a class="pull-right">D.D.S. - Doctor of Dental Surgery</a>
                                    </li>
                                </ul>
                                <div class="col-md-12" style="text-align: center;">
                                    <a href="<?=base_url(); ?>Physician/PatientSearch" id="link"><img type="image" src="../Common_1/Images/Proceed.png" alt="Submit"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#profile" data-toggle="tab">Update Profile</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="active tab-pane" id="profile">
                                    <form class="form-horizontal">
                                        <div class="form-group">
                                            <label for="inputName" class="col-sm-4 control-label">Alternate Phone Number</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="inputName" value="1234567890">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputEmail" class="col-sm-4 control-label">Clinic Name</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="inputEmail" value="Namma clinic">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputName" class="col-sm-4 control-label">Clinic Phone Number</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="inputName" value="584578584">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputExperience" class="col-sm-4 control-label">Clinic Alternate Phone Number</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="inputName" value="5426895475">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputSkills" class="col-sm-4 control-label">Clinic Address</label>
                                            <div class="col-sm-8">
                                                <textarea class="form-control" id="inputExperience"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputSkills" class="col-sm-4 control-label">Clinic Manager</label>
                                            <div class="col-sm-8">
                                                <div class="radio">
                                                    <label style="padding-right: 10px;">
                                                        <input type="radio" name="r2" class="minimal-red" checked>
                                                        Yes
                                                    </label>
                                                    <label>
                                                        <input type="radio" name="r2" class="minimal-red">
                                                        No
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputSkills" class="col-sm-4 control-label">Update Photo</label>
                                            <div class="col-sm-8">
                                                <input type="file" class="form-control" id="inputName">
                                            </div>
                                        </div>
                                        <div class="box-footer">
                                            <div class="row">
                                                <div class="col-md-12" style="text-align: center;margin-top: 6px;">
                                                    <input type="image" src="../Common_1/Images/Update.png" alt="Submit">
                                                    <input type="image" src="../Common_1/Images/Cancel.png" alt="Submit">
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>