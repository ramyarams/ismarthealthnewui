<footer class="main-footer">
            <div class="row">
                <div class="col-md-6">
                    Copyright 2018 © <strong><a href="#">iSmartHealth</a></strong> All rights reserved.
                </div>
                <div class="col-md-6" style="text-align:right;">
                    <span class="glyphicon glyphicon-phone" aria-hidden="true"></span>&nbsp;&nbsp;+91 7618702727&nbsp;&nbsp;&nbsp;&nbsp;
                    <span class="glyphicon glyphicon-phone-alt" aria-hidden="true"></span>&nbsp;&nbsp;080 26971320&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="mailto:sales@inetframe.com?subject=iSmartClinic.in"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>&nbsp;&nbsp;sales@inetframe.com</a>
                </div>
            </div>
        </footer>
        <div class="control-sidebar-bg"></div>
    </div>
    <script src="<?=base_url(); ?>Common_1/Js/jquery.min.js"></script>
    <script src="<?=base_url(); ?>Common_1/Js/bootstrap.min.js"></script>
    <script src="<?=base_url(); ?>Common_1/Js/jquery.dataTables.min.js"></script>
    <script src="<?=base_url(); ?>Common_1/Js/dataTables.bootstrap.min.js"></script>
    <script src="<?=base_url(); ?>Common_1/Js/jquery.slimscroll.min.js"></script>
    <script src="<?=base_url(); ?>Common_1/Js/adminlte.min.js"></script>
     <script src="<?=base_url(); ?>Common_1/Js/moment.js"></script>
    <script src="<?=base_url(); ?>Common_1/Js/fullcalendar.min.js"></script>
	<script type="text/javascript">
        $(document).ready(function () {
            $('#example1').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': true,
                'ordering': true,
                'info': true,
                'autoWidth': false,
                'lengthMenu': [[5, 10, 15, 25, 50, 100, -1], [5, 10, 15, 25, 50, 100, "All"]],
                'pageLength': 5,
                columnDefs: [{
                    orderable: false,
                    targets: "no-sort"
                }]
            });
        });</script>
</body>
</html>