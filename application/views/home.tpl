	<section class="site-padding" style="margin-top: -4%!important;">
        <div class="slider">
            <div id="fawesome-carousel" class="carousel slide" data-ride="carousel" data-interval="5000">
                <ol class="carousel-indicators indicatior2 image">
                    <li data-target="#fawesome-carousel" data-slide-to="0" class="active"></li>
                    <li data-target="#fawesome-carousel" data-slide-to="1"></li>
                    <li data-target="#fawesome-carousel" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="<?=base_url(); ?>Common/img/Doctor.png" alt="Doctor" class="img-responsive">
                        <div class="carousel-caption">
                            <h1 class="wow fadeInLeft" style="font-size: 25px;">Doctor</h1>
                            <div class="slider-btn wow fadeIn" style="margin-top: 6px;">
                                <a href="#" class="btn btn-learn">Deliver Care</a>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <img src="<?=base_url(); ?>Common/img/patient.png" alt="Patient" class="img-responsive">
                        <div class="carousel-caption">
                            <h1 class="wow fadeInLeft" style="font-size: 25px;">Patient</h1>
                            <div class="slider-btn wow fadeIn" style="margin-top: 6px;">
                                <a href="#" class="btn btn-learn">Receive Care</a>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <img src="<?=base_url(); ?>Common/img/Lab.png" alt="Laboratory" class="img-responsive">
                        <div class="carousel-caption">
                            <h1 class="wow fadeInLeft" style="font-size: 25px;">Laboratory</h1>
                            <div class="slider-btn wow fadeIn" style="margin-top: 6px;">
                                <a href="#" class="btn btn-learn">Enable Care</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="about" class="site-padding">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="title">
                        <h3>About <span>Us</span></h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="about-image wow fadeInLeft">
                        <img src="<?=base_url(); ?>Common/img/our_vision.png" alt="About Us" />
                    </div>
                </div>
                <div class="col-sm-6">
                    <div>
                        <p style="font-family: arial;text-align: justify;">iSmartHealth.in is a comprehensive, fully integrated healthcare management suite delivered on a cloud-based platform providing a robust and secure solution for healthcare needs. It is easy to adopt and use so that Doctors and Laboratories can focus on providing quality care for their patients. iSmartHealth.in provides secure online access for Patients, Physicians & Laboratories anytime, anywhere. Manage and inform appointments, follow-up status of Patients to Doctors by E-mail and SMS.</p>
                        <a href="#" class="btn btn-learn">Read More</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="clients" class="site-padding">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="title">
                        <h3>Our <span>Clients</span></h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="container" style="text-align: center;">
            <div class="row">
                <div class="col-sm-3">
                    <img src="<?=base_url(); ?>Common/img/abeam.jpg" />
                </div>
                <div class="col-sm-3">
                    <img src="<?=base_url(); ?>Common/img/account_agility.jpg" />
                </div>
                <div class="col-sm-3">
                    <img src="<?=base_url(); ?>Common/img/aws.jpg" />
                </div>
                <div class="col-sm-3">
                    <img src="<?=base_url(); ?>Common/img/ctrls.jpg" />
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <img src="<?=base_url(); ?>Common/img/intellicrop.png" />
                </div>
                <div class="col-sm-3">
                    <img src="<?=base_url(); ?>Common/img/microsoft_partner.jpg" />
                </div>
                <div class="col-sm-3">
                    <img src="<?=base_url(); ?>Common/img/oracle.jpg" />
                </div>
                <div class="col-sm-3">
                    <img src="<?=base_url(); ?>Common/img/pega.jpg" />
                </div>
            </div>
        </div>
    </section>
    <section id="contact-us">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="title">
                        <h3>Contact <span>Us</span></h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="contact">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-sm-6">
                            <div class="map" style="margin-top:-5px!important;">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3888.74592162646!2d77.56393121425148!3d12.924044990887126!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae15810d11718d%3A0x2f153216f66a836e!2siNetFrame+Technologies!5e0!3m2!1sen!2sin!4v1492422969116" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <h4>Contact Us For Any Kind Of Information</h4>
                            <form id="contactform">
                                <div class="row">
                                    <div class="col-md-12 field">
                                        <input type="text" name="name" id="name" placeholder="Your Name" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 field">
                                        <input type="text" name="email" id="email" placeholder="Your Email" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 field">
                                        <input type="text" name="subject" id="subject" placeholder="Subject" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <textarea rows="4" name="message" id="message" class="input-block-level" placeholder="Your message here..."></textarea>
                                        <p>
                                            <button class="btn btn-theme margintop10 pull-left" type="submit">Submit</button>
                                        </p>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>