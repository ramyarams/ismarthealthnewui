﻿        <div class="content-wrapper">
            <section class="content-header">
                <div class="login-logo">
                    <span style="color: #000;"><b>Report</b> Upload</span>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li>Patient Lookup</li>
                </ol>
            </section>
            <section class="content">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-4">
                                <a href="PatientQueue.html" id="link"><img type="image" src="../Common_1/Images/Back.png" alt="Submit" style="margin-bottom:3%;"></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">Report Upload - MRI</div>
                                    <div class="panel-body">
                                        <label for="seldate">MRI BRAIN PLAIN</label>
                                        <input type="file" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">Report Upload - MRI</div>
                                    <div class="panel-body">
                                        <label for="seldate">MRI BRAIN PLAIN</label>
                                        <input type="file" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">Report Upload - MRI</div>
                                    <div class="panel-body">
                                        <label for="seldate">MRI BRAIN PLAIN</label>
                                        <input type="file" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer" style="text-align:right;">
                            <button type="button" class="btn btn-success">Submit</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </section>
        </div>
       