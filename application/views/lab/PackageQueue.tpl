﻿        <div class="content-wrapper">
            <section class="content-header">
                <div class="login-logo">
                    <span style="color: #000;"><b>Patient</b> List</span>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li>Patient List</li>
                </ol>
            </section>
            <section class="content">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Referal ID</th>
                                        <th>Package Name</th>
                                        <th>Patient Name</th>
                                        <th>Patient Number</th>
                                        <th>Doctor</th>
                                        <th class="no-sort" width="10%">Test/Reports</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>AP001</td>
                                        <td>Sample Package</td>
                                        <td>Niranjan N</td>
                                        <td>0001001100</td>
                                        <td>Madhavan PH</td>
                                        <td style="text-align:center;"><img type="image" src="../Common_1/Images/Eye.png" alt="Submit" data-title="Add" data-toggle="modal" data-target="#add"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="add" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                <h4 class="modal-title custom_align" id="Heading">Package Details</h4>
                            </div>
                            <div class="modal-body">
                                <div id="alert"><h3> Sample Package <span class="pull-right text text-primary">15000Rs.</span></h3><br></div>

                                <div class="table-responsive">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <tbody>
                                            <tr>
                                                <th>Sl No.</th>
                                                <th>Tests</th>
                                                <th>Status</th>
                                            </tr>
                                            <tr>
                                                <td>1</td>
                                                <td>
                                                    Zinc Ser/Plas (Quantitative)
                                                </td>
                                                <td>
                                                    <img type="image" src="../Common_1/Images/Approve.png" alt="Submit" id="show-button">
                                                    <img type="image" src="../Common_1/Images/checked.png" alt="Submit" id="hide-button">
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
  
    <script type="text/javascript">
        $(document).ready(function () {
            $('#example1').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': true,
                'ordering': true,
                'info': true,
                'autoWidth': false,
                'lengthMenu': [[5, 10, 15, 25, 50, 100, -1], [5, 10, 15, 25, 50, 100, "All"]],
                'pageLength': 5,
                columnDefs: [{
                    orderable: false,
                    targets: "no-sort"
                }]
            });
        });
        $(document).ready(function () {
            $("#show-button").click(function () {
                $("#hide-button").show()
                $("#show-button").hide()
            });
            //$("#hide-button").click(function () {
            //    $("#show-button").show()
            //    $("#hide-button").hide()
            //});
        });
    </script>
</body>
</html>