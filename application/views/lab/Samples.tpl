﻿        <div class="content-wrapper">
            <section class="content-header">
                <div class="login-logo">
                    <span style="color: #000;"><b>Test</b> Details</span>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li>Patient List</li>
                </ol>
            </section>
            <section class="content">
                <div class="box box-primary">
                    <div class="box-body">
                        <a href="<?=base_url(); ?>Lab/PatientQueue" id="link"><img type="image" src="<?=base_url(); ?>Common_1/Images/Back.png" alt="Submit" style="margin-left:3%;"></a>
                        <section class="invoice">
                            <img src="<?=base_url(); ?>Common_1/Images/iNet.png" alt="Logo" />
                            <hr class="style-two hidden-print">
                            <div class="row invoice-info">
                                <div class="col-md-9 invoice-col">
                                    <address>
                                        <strong>Inet labs.</strong><br>
                                        Jayanagar<br>
                                        Phone: 9880407058<br>
                                        Email: info@inetlab.com
                                    </address>
                                </div>
                                <div class="col-md-3 invoice-col">
                                    <b>Subrat Kumar Pal</b><br>
                                    <b>Phone Number:</b> 9036809357<br>
                                    <b>Sex/Age:</b> Male/28<br />
                                    <b>Referred Dr:</b> Srinibas<br />
                                    <b>Date:</b> 13-11-2018 @ 3:53:7<br />
                                </div>
                            </div>
                            <hr class="style-two hidden-print">
                            <div class="row" style="margin-top:5%;">
                                <div class="col-xs-12 table-responsive">
                                    <table class="table table-user-information table-bordered table-striped" id="mytable">
                                        <tbody id="testbody">
                                            <tr>
                                                <th>#</th>
                                                <th>Test Name</th>
                                                <th width="15%">Test Date</th>
                                                <th width="10%">Samples</th>
                                            </tr>
                                            <tr>
                                                <td>1</td>
                                                <td>Doppler &amp; Ultrasound scrotum</td>
                                                <td>12-11-2018</td>
                                                <td>
                                                    <img type="image" src="<?=base_url(); ?>Common_1/Images/Approve.png" alt="Submit" id="show-button">
                                                    <img type="image" src="<?=base_url(); ?>Common_1/Images/checked.png" alt="Submit" id="hide-button">
                                                    <a href="<?=base_url(); ?>Lab/AddReport"><img type="image" src="../Common_1/Images/Plus.png" alt="Submit"></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>Doppler &amp; Ultrasound scrotum</td>
                                                <td>12-11-2018</td>
                                                <td>
                                                    <img type="image" src="<?=base_url(); ?>Common_1/Images/Approve.png" alt="Submit" id="show-button">
                                                    <img type="image" src="<?=base_url(); ?>Common_1/Images/checked.png" alt="Submit" id="hide-button">
                                                    <a href="<?=base_url(); ?>Lab/AddReport"><img type="image" src="../Common_1/Images/Plus.png" alt="Submit"></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>Doppler &amp; Ultrasound scrotum</td>
                                                <td>12-11-2018</td>
                                                <td>
                                                    <img type="image" src="<?=base_url(); ?>Common_1/Images/Approve.png" alt="Submit" id="show-button">
                                                    <img type="image" src="<?=base_url(); ?>Common_1/Images/checked.png" alt="Submit" id="hide-button">
                                                    <a href="<?=base_url(); ?>Lab/AddReport"><img type="image" src="../Common_1/Images/Plus.png" alt="Submit"></a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </section>
        </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#example1').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': true,
                'ordering': true,
                'info': true,
                'autoWidth': false,
                'lengthMenu': [[5, 10, 15, 25, 50, 100, -1], [5, 10, 15, 25, 50, 100, "All"]],
                'pageLength': 5,
                columnDefs: [{
                    orderable: false,
                    targets: "no-sort"
                }]
            });
        });
        $(document).ready(function () {
            $("#show-button").click(function () {
                $("#hide-button").show()
                $("#show-button").hide()
            });
            //$("#hide-button").click(function () {
            //    $("#show-button").show()
            //    $("#hide-button").hide()
            //});
        });
    </script>
</body>
</html>