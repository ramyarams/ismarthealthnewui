﻿        <div class="content-wrapper">
            <section class="content-header">
                <div class="login-logo">
                    <span style="color: #000;"><b>Enhance</b> Test Capabilities</span>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li>Patient List</li>
                </ol>
            </section>
            <section class="content">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="dual-list list-left col-md-4">
                                    <div class="well text-right">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="input-group">
                                                    <input type="text" name="SearchDualList" class="form-control" placeholder="search" />
                                                    <span class="input-group-addon glyphicon glyphicon-search" style="top: 0px;"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <ul class="list-group">
                                            <li class="list-group-item">A</li>
                                            <li class="list-group-item">B</li>
                                            <li class="list-group-item">C</li>
                                            <li class="list-group-item">D</li>
                                            <li class="list-group-item">E</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="list-arrows col-md-1 text-center">
                                    <button class="btn btn-default btn-sm move-left">
                                        <span class="glyphicon glyphicon-chevron-left"></span>
                                    </button>
                                    <button class="btn btn-default btn-sm move-right">
                                        <span class="glyphicon glyphicon-chevron-right"></span>
                                    </button>
                                </div>
                                <div class="dual-list list-right col-md-4">
                                    <div class="well">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="input-group">
                                                    <input type="text" name="SearchDualList" class="form-control" placeholder="search" />
                                                    <span class="input-group-addon glyphicon glyphicon-search" style="top: 0px;"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <ul class="list-group">
                                            <li class="list-group-item">F</li>
                                            <li class="list-group-item">G</li>
                                            <li class="list-group-item">H</li>
                                            <li class="list-group-item">I</li>
                                            <li class="list-group-item">J</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-xs-12" style="text-align: right;">
                                <button class="btn btn-success">Submit</button>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </section>
        </div>
        