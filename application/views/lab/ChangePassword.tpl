	<div class="content-wrapper">
            <section class="content-header">
                <div class="login-logo">
                    <span style="color: #000;"><b>Change</b> Password</span>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li>Patient Lookup</li>
                </ol>
            </section>
            <section class="content">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">Change Password</div>
                                    <div class="panel-body">
                                        <label for="seldate">Username</label>
                                        <input type="text" class="form-control">
                                        <label for="sel1">Current Password</label>
                                        <input type="text" class="form-control">
                                        <label for="seldate">New Password</label>
                                        <input type="text" class="form-control">
                                        <label for="sel1">Confirm Password</label>
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="alert alert-danger">
                                    <strong><h4>Kindly Note!</h4></strong> <br>
                                    <ol>
                                        <li>To protect your account don't reveal your password.</li>
                                        <li> For security reasons, keep changing your password at regular intervals.</li>
                                        <br>
                                    </ol>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer" style="text-align:right;">
                            <a class="btn btn-success" href="">Submit</a>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </section>
        </div>