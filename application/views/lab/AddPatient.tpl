﻿        <div class="content-wrapper">
            <section class="content-header">
                <div class="login-logo">
                    <span style="color: #000;"><b>Add</b> Patient</span>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li>Patient List</li>
                </ol>
            </section>
            <section class="content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#addPatient" data-toggle="tab" aria-expanded="true">Add Patient</a></li>
                                <li class=""><a href="#docDetails" data-toggle="tab" aria-expanded="false">Doctor Details</a></li>
                                <li class=""><a href="#testsSel" data-toggle="tab" aria-expanded="false">Tests Selection</a></li>
                                <li class=""><a href="#confirm" data-toggle="tab" aria-expanded="false">Confirm</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="active tab-pane" id="addPatient">
                                    <h4 class="info-text alert alert-danger" style="background-color: #29caf1!important;border-color: #29caf1;"> Personal Information</h4>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label>First Name</label>
                                            <input type="text" name="firstname" class="form-control" value="" id="firstname" placeholder="First Name">
                                        </div>
                                        <div class="col-md-4">
                                            <label>Last Name</label>
                                            <input type="text" name="lastname" class="form-control" value="" id="lastname" placeholder="Last Name">
                                        </div>
                                        <div class="col-md-4">
                                            <label>Gender</label>
                                            <div class="form-check">
                                                <select class="form-control" id="gender" name="gender">
                                                    <option data-hidden="true" style="font-weight:700" value="0">Select</option>
                                                    <option value="1">Male</option>
                                                    <option value="2">Female</option>
                                                    <option value="3">Others</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div><br />
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label>Email Address</label>
                                            <input type="text" name="email" class="form-control" value="" id="email" placeholder="Email Address">
                                        </div>
                                        <div class="col-md-4">
                                            <label>Phone Number</label>
                                            <input type="text" name="phone" class="form-control" value="" id="phone" placeholder="Phone Number">
                                        </div>
                                        <div class="col-md-4">
                                            <label>Age</label>
                                            <input type="text" name="age" class="form-control" value="" id="age" placeholder="Age">
                                        </div>
                                    </div><br />
                                    <h4 class="info-text alert alert-danger" style="background-color: #29caf1!important;border-color: #29caf1;"> Address Information</h4>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label>Address</label>
                                            <input type="text" name="address" class="form-control" value="" id="address" placeholder="Address">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="current_password" class="control-label">State</label>
                                            <div class="form-check">
                                                <select class="form-control" id="state" name="state">
                                                    <option data-hidden="true" style="font-weight:700" value="0">Select</option>
                                                    <option value="1">Karnataka</option>
                                                    <option value="2">Tamilnadu</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label>City</label>
                                            <div class="form-check">
                                                <select class="form-control" id="city" name="city">
                                                    <option data-hidden="true" style="font-weight:700" value="0">Select</option>
                                                    <option value="1">Bangalore</option>
                                                    <option value="2">Chennai</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div><br />
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="current_password" class="control-label">Postal Code</label>
                                            <input type="text" name="zip" class="form-control" value="" id="zip" placeholder="Postal Code">
                                        </div>
                                    </div><br />
                                    <hr style="margin-top: 2px;" />
                                    <div class="row">
                                        <div class="col-xs-12" style="text-align: right;">
                                            <a href="#" id="link"><img type="image" src="<?=base_url(); ?>Common_1/Images/Forward_32.png" alt="Submit"></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="docDetails">
                                    <h4 class="info-text alert alert-danger" style="background-color: #29caf1!important;border-color: #29caf1;"> Doctor Information</h4>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="table-responsive">
                                                <table id="example1" class="table table-bordered table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>Name</th>
                                                            <th>Phone Number</th>
                                                            <th>Email Address</th>
                                                            <th>Specialization</th>
                                                            <th>Clinic Address</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>Rakesh Kumar Pattnaik</td>
                                                            <td>9036809357</td>
                                                            <td>Subrat.Pal@gmail.com</td>
                                                            <td>General physician</td>
                                                            <td>Marathalli, Bangalore</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div><br />
                                    <hr style="margin-top: 2px;" />
                                    <div class="row">
                                        <div class="col-xs-6" style="text-align: left;">
                                            <a href="#" id="link"><img type="image" src="<?=base_url(); ?>Common_1/Images/Back.png" alt="Submit"></a>
                                        </div>
                                        <div class="col-xs-6" style="text-align: right;">
                                            <a href="#" id="link"><img type="image" src="<?=base_url(); ?>Common_1/Images/Forward_32.png" alt="Submit"></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="testsSel">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="dual-list list-left col-md-4">
                                                <div class="well text-right">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="input-group">
                                                                <input type="text" name="SearchDualList" class="form-control" placeholder="search" />
                                                                <span class="input-group-addon glyphicon glyphicon-search" style="top: 0px;"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <ul class="list-group">
                                                        <li class="list-group-item">A</li>
                                                        <li class="list-group-item">B</li>
                                                        <li class="list-group-item">C</li>
                                                        <li class="list-group-item">D</li>
                                                        <li class="list-group-item">E</li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="list-arrows col-md-1 text-center">
                                                <button class="btn btn-default btn-sm move-left">
                                                    <span class="glyphicon glyphicon-chevron-left"></span>
                                                </button>
                                                <button class="btn btn-default btn-sm move-right">
                                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                                </button>
                                            </div>
                                            <div class="dual-list list-right col-md-4">
                                                <div class="well">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="input-group">
                                                                <input type="text" name="SearchDualList" class="form-control" placeholder="search" />
                                                                <span class="input-group-addon glyphicon glyphicon-search" style="top: 0px;"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <ul class="list-group">
                                                        <li class="list-group-item">F</li>
                                                        <li class="list-group-item">G</li>
                                                        <li class="list-group-item">H</li>
                                                        <li class="list-group-item">I</li>
                                                        <li class="list-group-item">J</li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <h5 class="info-text alert alert-danger" style="background-color: #29caf1!important;border-color: #29caf1;"> Click on the common date you would like to choose for the Selected Tests</h5>
                                                <label>Select Date</label>
                                                <input type="date" name="firstname" class="form-control" value="" id="firstname" placeholder="First Name">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-7">
                                            <h4 class="info-text alert alert-danger" style="background-color: #29caf1!important;border-color: #29caf1;"> The Selected Tests are displayed below:</h4>
                                            <div class="table-responsive">
                                                <table id="example1" class="table table-bordered table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>Test Name</th>
                                                            <th width="155px">Select Date</th>
                                                            <th width="120px">Select Time</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>MRI BRAIN PLAIN</td>
                                                            <td><input type="date" class="form-control" placeholder="yyyy-mm-dd" style="width:155px;"></td>
                                                            <td>
                                                                <select name="time" class="form-control" style="width:120px;">
                                                                    <option value="09:30">09:30 AM</option>
                                                                    <option value="10:30">10:30 AM</option>
                                                                    <option value="11:30">11:30 AM</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>MRI BRAIN PLAIN</td>
                                                            <td><input type="date" class="form-control" placeholder="yyyy-mm-dd" style="width:155px;"></td>
                                                            <td>
                                                                <select name="time" class="form-control" style="width:120px;">
                                                                    <option value="09:30">09:30 AM</option>
                                                                    <option value="10:30">10:30 AM</option>
                                                                    <option value="11:30">11:30 AM</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>MRI BRAIN PLAIN</td>
                                                            <td><input type="date" class="form-control" placeholder="yyyy-mm-dd" style="width:155px;"></td>
                                                            <td>
                                                                <select name="time" class="form-control" style="width:120px;">
                                                                    <option value="09:30">09:30 AM</option>
                                                                    <option value="10:30">10:30 AM</option>
                                                                    <option value="11:30">11:30 AM</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>MRI BRAIN PLAIN</td>
                                                            <td><input type="date" class="form-control" placeholder="yyyy-mm-dd" style="width:155px;"></td>
                                                            <td>
                                                                <select name="time" class="form-control" style="width:120px;">
                                                                    <option value="09:30">09:30 AM</option>
                                                                    <option value="10:30">10:30 AM</option>
                                                                    <option value="11:30">11:30 AM</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>MRI BRAIN PLAIN</td>
                                                            <td><input type="date" class="form-control" placeholder="yyyy-mm-dd" style="width:155px;"></td>
                                                            <td>
                                                                <select name="time" class="form-control" style="width:120px;">
                                                                    <option value="09:30">09:30 AM</option>
                                                                    <option value="10:30">10:30 AM</option>
                                                                    <option value="11:30">11:30 AM</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <label for="seldate">Brief History</label>
                                            <textarea type="text" class="form-control" rows="6"></textarea>
                                            <label for="sel1">Special Advise</label>
                                            <textarea type="text" class="form-control" rows="6"></textarea>
                                        </div>
                                    </div>
                                    <hr style="margin-top: 2px;" />
                                    <div class="row">
                                        <div class="col-xs-6" style="text-align: left;">
                                            <a href="#" id="link"><img type="image" src="<?=base_url(); ?>Common_1/Images/Back.png" alt="Submit"></a>
                                        </div>
                                        <div class="col-xs-6" style="text-align: right;">
                                            <a href="#" id="link"><img type="image" src="<?=base_url(); ?>Common_1/Images/Forward_32.png" alt="Submit"></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="confirm">
                                    <h4 class="info-text alert alert-danger" style="background-color: #29caf1!important;border-color: #29caf1;"> Personal Information</h4>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label>First Name</label>
                                            <input type="text" name="firstname" class="form-control" value="" id="firstname" placeholder="First Name" disabled>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Last Name</label>
                                            <input type="text" name="lastname" class="form-control" value="" id="lastname" placeholder="Last Name" disabled>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Gender</label>
                                            <input type="text" name="address" class="form-control" value="" id="address" placeholder="Address" disabled>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Email Address</label>
                                            <input type="text" name="email" class="form-control" value="" id="email" placeholder="Email Address" disabled>
                                        </div>
                                    </div><br />
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label>Phone Number</label>
                                            <input type="text" name="phone" class="form-control" value="" id="phone" placeholder="Phone Number" disabled>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Age</label>
                                            <input type="text" name="age" class="form-control" value="" id="age" placeholder="Age" disabled>
                                        </div>
                                    </div>
                                    <h4 class="info-text alert alert-danger" style="background-color: #29caf1!important;border-color: #29caf1;"> Address Information</h4>
                                    <div class="row">
                                        <div class="col-md-9">
                                            <label>Address</label>
                                            <input type="text" name="address" class="form-control" value="" id="address" placeholder="Address" disabled>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Postal Code</label>
                                            <input type="text" name="address" class="form-control" value="" id="address" placeholder="Address" disabled>
                                        </div>
                                    </div>
                                    <h4 class="info-text alert alert-danger" style="background-color: #29caf1!important;border-color: #29caf1;"> Doctor Information</h4>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label>Name</label>
                                            <input type="text" name="address" class="form-control" value="" id="address" placeholder="Address" disabled>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Email Address</label>
                                            <input type="text" name="address" class="form-control" value="" id="address" placeholder="Address" disabled>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Phone Number</label>
                                            <input type="text" name="address" class="form-control" value="" id="address" placeholder="Address" disabled>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Specialization</label>
                                            <input type="text" name="address" class="form-control" value="" id="address" placeholder="Address" disabled>
                                        </div>
                                    </div><br />
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label>Clinic Address</label>
                                            <input type="text" name="address" class="form-control" value="" id="address" placeholder="Address" disabled>
                                        </div>
                                    </div>
                                    <h4 class="info-text alert alert-danger" style="background-color: #29caf1!important;border-color: #29caf1;"> Tests Selected</h4>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="table-responsive">
                                                <table id="example1" class="table table-bordered table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>Name</th>
                                                            <th>Date</th>
                                                            <th>Time</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>Mri Brain</td>
                                                            <td>20-05-1989</td>
                                                            <td>9.30AM</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <h4 class="info-text alert alert-danger" style="background-color: #29caf1!important;border-color: #29caf1;"> Tests Selected</h4>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label>Brief History</label>
                                            <input type="text" name="address" class="form-control" value="" id="address" placeholder="Address" disabled>
                                        </div>
                                    </div><br />
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label>Special Advise</label>
                                            <input type="text" name="address" class="form-control" value="" id="address" placeholder="Address" disabled>
                                        </div>
                                    </div><br />
                                    <hr style="margin-top: 1%;" />
                                    <div class="row">
                                        <div class="col-xs-6" style="text-align: left;">
                                            <a href="#" id="link"><img type="image" src="<?=base_url(); ?>Common_1/Images/Back.png" alt="Submit"></a>
                                        </div>
                                        <div class="col-xs-6" style="text-align: right;">
                                            <button class="btn btn-success">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#example1').DataTable({
                'paging': true,
                'lengthChange': true,
                'searching': true,
                'ordering': true,
                'info': true,
                'autoWidth': true,
                'lengthMenu': [[5, 10, 15, 25, 50, 100, -1], [5, 10, 15, 25, 50, 100, "All"]],
                'pageLength': 5,
                columnDefs: [{
                    orderable: false,
                    targets: "no-sort"
                }]
            });
        });
        $(function () {

            $('body').on('click', '.list-group .list-group-item', function () {
                $(this).toggleClass('active');
            });
            $('.list-arrows button').click(function () {
                var $button = $(this), actives = '';
                if ($button.hasClass('move-left')) {
                    actives = $('.list-right ul li.active');
                    actives.clone().appendTo('.list-left ul');
                    actives.remove();
                } else if ($button.hasClass('move-right')) {
                    actives = $('.list-left ul li.active');
                    actives.clone().appendTo('.list-right ul');
                    actives.remove();
                }
            });
            $('.dual-list .selector').click(function () {
                var $checkBox = $(this);
                if (!$checkBox.hasClass('selected')) {
                    $checkBox.addClass('selected').closest('.well').find('ul li:not(.active)').addClass('active');
                    $checkBox.children('i').removeClass('glyphicon-unchecked').addClass('glyphicon-check');
                } else {
                    $checkBox.removeClass('selected').closest('.well').find('ul li.active').removeClass('active');
                    $checkBox.children('i').removeClass('glyphicon-check').addClass('glyphicon-unchecked');
                }
            });
            $('[name="SearchDualList"]').keyup(function (e) {
                var code = e.keyCode || e.which;
                if (code == '9') return;
                if (code == '27') $(this).val(null);
                var $rows = $(this).closest('.dual-list').find('.list-group li');
                var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
                $rows.show().filter(function () {
                    var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
                    return !~text.indexOf(val);
                }).hide();
            });

        });
    </script>
</body>
</html>