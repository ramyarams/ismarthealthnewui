﻿        <div class="content-wrapper">
            <section class="content-header">
                <div class="login-logo">
                    <span style="color: #000;"><b>Available</b> Packages</span>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li>Patient List</li>
                </ol>
            </section>
            <section class="content">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Sl No</th>
                                        <th>Package Name</th>
                                        <th>No of Test contains packages</th>
                                        <th>Select This Package</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Sample Package</td>
                                        <td>7</td>
                                        <td>
                                            <img type="image" src="<?=base_url(); ?>Common_1/Images/Approve.png" alt="Submit" id="show-button" data-toggle="modal" data-target="#selectPackage">
                                            <img type="image" src="<?=base_url(); ?>Common_1/Images/checked.png" alt="Submit" id="hide-button" data-toggle="modal" data-target="#selectPackage">
                                            <img type="image" src="<?=base_url(); ?>Common_1/Images/Decline.png" alt="Submit">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="selectPackage" tabindex="-1" role="dialog" aria-labelledby="selectPackage" aria-hidden="true">
                    <div class="modal-dialog modal-md">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                <h4 class="modal-title custom_align" id="Heading">Package details</h4>
                            </div>
                            <div class="modal-body">
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <h4><strong>Package contains</strong></h4>
                                        <ul>
                                            <li>Urine Culture &amp; Sensitivity</li>
                                            <li>Urine Electrolytes (Na, K, Cl) Panel</li>
                                            <li>urine osmolality</li>
                                        </ul>
                                        <h4><strong>Not supported tests</strong></h4>
                                        <ul>
                                            <li>Urine Culture &amp; Sensitivity</li>
                                            <li>Urine Electrolytes (Na, K, Cl) Panel</li>
                                            <li>urine osmolality</li>
                                        </ul>
                                        <p class="text-success text-center">Already added to your lab package</p><br>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-success" data-dismiss="modal">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#example1').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': true,
                'ordering': true,
                'info': true,
                'autoWidth': false,
                'lengthMenu': [[5, 10, 15, 25, 50, 100, -1], [5, 10, 15, 25, 50, 100, "All"]],
                'pageLength': 5,
                columnDefs: [{
                    orderable: false,
                    targets: "no-sort"
                }]
            });
        });
        $(document).ready(function () {
            $("#show-button").click(function () {
                $("#hide-button").show()
                $("#show-button").hide()
            });
            //$("#hide-button").click(function () {
            //    $("#show-button").show()
            //    $("#hide-button").hide()
            //});
        });
    </script>
</body>
</html>