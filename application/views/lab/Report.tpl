﻿        <div class="content-wrapper">
            <section class="content-header">
                <div class="login-logo">
                    <span style="color: #000;"><b>Patient</b> Details</span>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li>Patient List</li>
                </ol>
            </section>
            <section class="content">
                <div class="box box-primary">
                    <div class="box-body">
                        <a href="<?=base_url(); ?>lab/PatientQueue" id="link"><img type="image" src="<?=base_url(); ?>Common_1/Images/Back.png" alt="Submit" style="margin-left:3%;"></a>
                        <section class="invoice">
                            <img src="<?=base_url(); ?>Common_1/Images/iNet.png" alt="Logo" />
                            <hr class="style-two hidden-print">
                            <div class="row invoice-info">
                                <div class="col-md-12 invoice-col">
                                    <b>Subrat Kumar Pal</b><br>
                                    <b>Phone Number:</b> 9036809357<br>
                                    <b>Sex/Age:</b> Male/28<br />
                                    <b>Referred Dr:</b> Srinibas<br />
                                    <b>Date:</b> 13-11-2018 @ 3:53:7<br />
                                </div>
                            </div>
                            <hr class="style-two hidden-print">
                            <div class="row" style="margin-top:5%;">
                                <div class="col-xs-12 table-responsive">
                                    <table class="table table-user-information table-bordered table-striped" id="mytable">
                                        <tbody id="testbody">
                                            <tr>
                                                <th>Sl No.</th>
                                                <th>Tests</th>
                                                <th>Date</th>
                                                <th>Time</th>
                                                <th>Sample Results</th>
                                                <th>Unit</th>
                                                <th>Reference Range</th>
                                                <th colspan="2" style="text-align:center;">Review &amp; Approve / View</th>
                                            </tr>
                                            <tr>
                                                <td>1</td>
                                                <td>MRI BRAIN PLAIN</td>
                                                <td>21-11-2018</td>
                                                <td>4:00</td>
                                                <td>90</td>
                                                <td>mg/dL</td>
                                                <td>10-140</td>
                                                <td style="text-align:center;"><a href="<?=base_url(); ?>lab/ViewReport"><img type="image" src="<?=base_url(); ?>Common_1/Images/Eye.png" alt="Submit"></a></td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>
                                                    24 Hours Urine Protine
                                                </td>
                                                <td>21-11-2018</td>
                                                <td>5:00</td>
                                                <td>90</td>
                                                <td>mg/dL</td>
                                                <td>10-140</td>
                                                <td style="text-align:center;"><a href="<?=base_url(); ?>lab/DownloadReport"><img type="image" src="<?=base_url(); ?>Common_1/Images/Download_24.png" alt="Submit"></a></td>
                                            </tr>
                                             <tr>
                                                <td>3</td>
                                                <td>
                                                    Absolute Eosinophil Count
                                                </td>
                                                <td>21-11-2018</td>
                                                <td>6:00</td>
                                                <td>350</td>
                                                <td>pc</td>
                                                <td>30-350</td>
                                                <td style="text-align:center;"><button class="btn btn-success btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit"><span class="glyphicon glyphicon-pencil"></span></button></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="alert alert-danger" style="background-color: #337ab7!important;border-color: #337ab7;"> <strong>Special Advise : </strong> kmkm</div>
                            <div class="alert alert-danger" style="background-color: #337ab7!important;border-color: #337ab7;"> <strong>Brief History : </strong> kmkm</div>
                        </section>
                        <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
                            <div class="modal-dialog modal-sm">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                        <h4 class="modal-title custom_align" id="Heading">Edit Sample Value</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="blood_pressure">Sample Value</label>
                                            <input type="text" id="Sample_Value" placeholder="Sample Value" class="form-control" name="Sample_Value">
                                        </div>
                                        <div class="form-group">
                                            <label for="blood_pressure">Sample Unit</label>
                                            <input type="number" id="Sample_Unit" placeholder="Sample Unit" class="form-control" name="Sample_Unit">
                                        </div>
                                        <div class="form-group">
                                            <label for="blood_pressure">Reference Range</label>
                                            <input type="number" id="Reference_Range" placeholder="Reference Range" class="form-control" name="Reference_Range">
                                        </div>
                                    </div>
                                    <div class="box-footer" style="text-align:center;">
                                        <button type="button" class="btn btn-success">Submit</button>
                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#example1').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': true,
                'ordering': true,
                'info': true,
                'autoWidth': false,
                'lengthMenu': [[5, 10, 15, 25, 50, 100, -1], [5, 10, 15, 25, 50, 100, "All"]],
                'pageLength': 5,
                columnDefs: [{
                    orderable: false,
                    targets: "no-sort"
                }]
            });
        });
        $(document).ready(function () {
            $("#show-button").click(function () {
                $("#hide-button").show()
                $("#show-button").hide()
            });
            //$("#hide-button").click(function () {
            //    $("#show-button").show()
            //    $("#hide-button").hide()
            //});
        });
    </script>
</body>
</html>