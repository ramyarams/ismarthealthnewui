﻿        <div class="content-wrapper">
            <section class="content-header">
                <div class="login-logo">
                    <span style="color: #000;"><b>Inet labs</b> working time details</span>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li>Patient List</li>
                </ol>
            </section>
            <section class="content">
                <div class="box box-primary">
                    <div class="box-body">
                        <a href="PatientQueue.html" id="link"><img type="image" src="<?=base_url(); ?>Common_1/Images/Back.png" alt="Submit" style="margin-bottom: 1%;"></a>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th class="text-center">
                                                    Sunday<br>
                                                    <select class="sunday_working_type form-control" rel="sunday">
                                                        <option value="WORKING_DAY" class="workingday" rel="WORKING_DAY">Working day</option>
                                                        <option value="HOLI_DAY" class="holiday" rel="HOLI_DAY" selected="">Holiday</option>
                                                    </select>
                                                </th>
                                                <th class="text-center">
                                                    Monday<br>
                                                    <select class="monday_working_type form-control" rel="monday">
                                                        <option value="WORKING_DAY" class="workingday" rel="WORKING_DAY" selected="">Working day</option>
                                                        <option value="HOLI_DAY" class="holiday" rel="HOLI_DAY">Holiday</option>
                                                    </select>
                                                </th>
                                                <th class="text-center">
                                                    Tuesday<br>
                                                    <select class="tuesday_working_type form-control" rel="tuesday">
                                                        <option value="WORKING_DAY" class="workingday" rel="WORKING_DAY" selected="">Working day</option>
                                                        <option value="HOLI_DAY" class="holiday" rel="HOLI_DAY">Holiday</option>
                                                    </select>
                                                </th>
                                                <th class="text-center">
                                                    wednesday
                                                    <br>
                                                    <select class="wednesday_working_type form-control" rel="wednesday">
                                                        <option value="WORKING_DAY" class="workingday" rel="WORKING_DAY" selected="">Working day</option>
                                                        <option value="HOLI_DAY" class="holiday" rel="HOLI_DAY">Holiday</option>
                                                    </select>
                                                </th>
                                                <th class="text-center">
                                                    Thursday<br>
                                                    <select class="thursday_working_type form-control" rel="thursday">
                                                        <option value="WORKING_DAY" class="workingday" rel="WORKING_DAY" selected="">Working day</option>
                                                        <option value="HOLI_DAY" class="holiday" rel="HOLI_DAY">Holiday</option>
                                                    </select>
                                                </th>
                                                <th class="text-center">
                                                    Friday<br>
                                                    <select class="friday_working_type form-control" rel="friday">
                                                        <option value="WORKING_DAY" class="workingday" rel="WORKING_DAY" selected="">Working day</option>
                                                        <option value="HOLI_DAY" class="holiday" rel="HOLI_DAY">Holiday</option>
                                                    </select>
                                                </th>
                                                <th class="text-center">
                                                    Saturday<br>
                                                    <select class="saturday_working_type form-control" rel="saturday">
                                                        <option value="WORKING_DAY" class="workingday" rel="WORKING_DAY" selected="">Working day</option>
                                                        <option value="HOLI_DAY" class="holiday" rel="HOLI_DAY">Holiday</option>
                                                    </select>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><input type="text" placeholder="Opening time" class="form-control" name="openingtime_sunday" id="openingtime_sunday" value="" required="" tabindex="1" style="cursor: pointer;" autocomplete="off"></td>
                                                <td><input type="text" placeholder="Opening time" class="form-control timepicker openingtime_monday" name="openingtime_monday" id="openingtime_monday" value="8:30 AM" required="" tabindex="3" style="cursor: pointer;" onkeydown="return false" autocomplete="off"></td>
                                                <td><input type="text" placeholder="Opening time" class="form-control timepicker openingtime_tuesday" name="openingtime_tuesday" id="openingtime_tuesday" value="9:30 AM" required="" tabindex="5" style="cursor: pointer;" onkeydown="return false" autocomplete="off"></td>
                                                <td><input type="text" placeholder="Opening time" class="form-control timepicker openingtime_wednesday" name="openingtime_wednesday" id="openingtime_wednesday" value="9:00 AM" required="" tabindex="7" style="cursor: pointer;" onkeydown="return false" autocomplete="off"></td>
                                                <td><input type="text" placeholder="Opening time" class="form-control timepicker openingtime_thursday" name="openingtime_thursday" id="openingtime_thursday" value="9:00 AM" required="" tabindex="9" style="cursor: pointer;" onkeydown="return false" autocomplete="off"></td>
                                                <td><input type="text" placeholder="Opening time" class="form-control timepicker openingtime_friday" name="openingtime_friday" id="openingtime_friday" value="9:00 AM" required="" tabindex="11" style="cursor: pointer;" onkeydown="return false" autocomplete="off"></td>
                                                <td><input type="text" placeholder="Opening time" class="form-control timepicker openingtime_saturday" name="openingtime_saturday" id="openingtime_saturday" value="10:30 AM" required="" tabindex="13" style="cursor: pointer;" onkeydown="return false" autocomplete="off"></td>
                                            </tr>
                                            <tr>
                                                <td><input type="text" placeholder="Closing time" class="form-control timepicker closingtime_sunday" name="closingtime_sunday" id="closingtime_sunday" value="" required="" tabindex="2" style="cursor: pointer;" autocomplete="off"></td>
                                                <td><input type="text" placeholder="Closing time" class="form-control timepicker closingtime_monday" name="closingtime_monday" id="closingtime_monday" value="9:30 PM" required="" tabindex="4" style="cursor: pointer;" onkeydown="return false" autocomplete="off"></td>
                                                <td><input type="text" placeholder="Closing time" class="form-control timepicker closingtime_tuesday" name="closingtime_tuesday" id="closingtime_tuesday" value="9:00 PM" required="" tabindex="6" style="cursor: pointer;" onkeydown="return false" autocomplete="off"></td>
                                                <td><input type="text" placeholder="Closing time" class="form-control timepicker closingtime_wednesday" name="closingtime_wednesday" id="closingtime_wednesday" value="9:30 PM" required="" tabindex="8" style="cursor: pointer;" onkeydown="return false" autocomplete="off"></td>
                                                <td><input type="text" placeholder="Closing time" class="form-control timepicker closingtime_thursday" name="closingtime_thursday" id="closingtime_thursday" value="9:30 PM" required="" tabindex="10" style="cursor: pointer;" onkeydown="return false" autocomplete="off"></td>
                                                <td><input type="text" placeholder="Closing time" class="form-control timepicker closingtime_friday" name="closingtime_friday" id="closingtime_friday" value="9:30 PM" required="" tabindex="12" style="cursor: pointer;" onkeydown="return false" autocomplete="off"></td>
                                                <td><input type="text" placeholder="Closing time" class="form-control timepicker closingtime_saturday" name="closingtime_saturday" id="closingtime_saturday" value="2:00 PM" required="" tabindex="14" style="cursor: pointer;" onkeydown="return false" autocomplete="off"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12" style="text-align: right;">
                                <button type="button" class="btn btn-success">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
       
    <script type="text/javascript">
        $(document).ready(function () {
            $("#show-button").click(function () {
                $("#hide-button").show()
                $("#show-button").hide()
            });
            //$("#hide-button").click(function () {
            //    $("#show-button").show()
            //    $("#hide-button").hide()
            //});
        });
    </script>
</body>
</html>