﻿        <div class="content-wrapper">
            <section class="content-header">
                <div class="login-logo">
                    <span style="color: #000;"><b>Lab</b> Report</span>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li>Patient List</li>
                </ol>
            </section>
            <section class="content">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-5" style="text-align: left;">
                                    <a href="PatientQueue.html" id="link"><img type="image" src="../Common_1/Images/Back.png" alt="Submit"></a>
                                </div>
                                <div class="col-md-7" style="text-align: right;">
                                    <a><img type="image" src="<?=base_url(); ?>Common_1/Images/Download.png" alt="Submit"></a>
                                    <a><img type="image" src="<?=base_url(); ?>Common_1/Images/Printer.png" alt="Submit"></a>
                                </div>
                            </div>
                        </div>
                        <section class="invoice">
                            <img src="<?=base_url(); ?>Common_1/Images/iNet.png" alt="Logo" />
                            <hr class="style-two hidden-print">
                            <div class="row invoice-info">
                                <div class="col-md-9 invoice-col">
                                    <address>
                                        <strong>Inet labs.</strong><br>
                                        Jayanagar<br>
                                        Phone: 9880407058<br>
                                        Email: info@inetlab.com
                                    </address>
                                </div>
                                <div class="col-md-3 invoice-col">
                                    <b>Subrat Kumar Pal</b><br>
                                    <b>Phone Number:</b> 9036809357<br>
                                    <b>Sex/Age:</b> Male/28<br />
                                    <b>Referred Dr:</b> Srinibas<br />
                                    <b>Date:</b> 13-11-2018 @ 3:53:7<br />
                                </div>
                            </div>
                            <hr class="style-two hidden-print">
                            <h4 style="text-align:center;"><b>Test Report</b></h4>
                            <div class="row">
                                <div class="col-xs-12 table-responsive">
                                    <table class="table table-user-information table-bordered table-striped" id="mytable">
                                        <tbody id="testbody">
                                            <tr>
                                                <th>Test Name</th>
                                                <th>Result</th>
                                                <th>Unit</th>
                                                <th>Reference Range</th>
                                                <th>Instruction</th>
                                                <th>Test Method</th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    24 Hours Urine Protine
                                                </td>
                                                <td>40</td>
                                                <td>mg/dL</td>
                                                <td>10-140</td>
                                                <td>INS1</td>
                                                <td>TM1</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <hr class="style-two hidden-print">
                            <p><strong>Note: </strong> Value indicated purely based on the sample collected at that time. While Above or Below range is normally indicated, please consult your Physician for further inferences.</p>
                            <div class="row"><div class="col-md-12"><div class="col-md-3 pull-right" style="margin-top:100px;"><hr><strong style="text-align: center;">Authorized Signatory</strong> </div></div></div>
                        </section>
                    </div>
                </div>
            </section>
        </div>
       
    <script type="text/javascript">
        $(document).ready(function () {
            $('#example1').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': true,
                'ordering': true,
                'info': true,
                'autoWidth': false,
                'lengthMenu': [[5, 10, 15, 25, 50, 100, -1], [5, 10, 15, 25, 50, 100, "All"]],
                'pageLength': 5,
                columnDefs: [{
                    orderable: false,
                    targets: "no-sort"
                }]
            });
        });
        $(document).ready(function () {
            $("#show-button").click(function () {
                $("#hide-button").show()
                $("#show-button").hide()
            });
            //$("#hide-button").click(function () {
            //    $("#show-button").show()
            //    $("#hide-button").hide()
            //});
        });
    </script>
</body>
</html>