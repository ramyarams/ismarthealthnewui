﻿  <div class="content-wrapper">
            <section class="content-header">
                <div class="login-logo">
                    <span style="color: #000;"><b>Inet labs</b> test's price details</span>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li>Patient Lookup</li>
                </ol>
            </section>
            <section class="content">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-3">
                                <label>Select Test Category</label>
                                <div class="form-check">
                                    <select class="form-control" id="gender" name="gender">
                                        <option data-hidden="true" style="font-weight:700" value="0">Select</option>
                                        <option value="1">MRI</option>
                                        <option value="2">X Ray</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-top:2%;">
                            <div class="col-md-12">
                                <h4 class="info-text alert alert-danger" style="background-color: #29caf1!important;border-color: #29caf1;">Update Test Price For: X Ray</h4>
                                <div class="table-responsive">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th style="width: 5%;">Sl No</th>
                                                <th style="width: 20%;">Test Name</th>
                                                <th>Price</th>
                                                <th>Custom Name</th>
                                                <th>Instruction</th>
                                                <th>Test Method</th>
                                                <th>Unit</th>
                                                <th>Reference Range</th>
                                                <th style="width: 7%;">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Fetal Echo</td>
                                                <td><input type="number" min="0" class="form-control"></td>
                                                <td><input type="text" class="form-control"></td>
                                                <td><input type="text" class="form-control"></td>
                                                <td><input type="text" class="form-control"></td>
                                                <td>
                                                    <input type="text" class="form-control">
                                                </td>

                                                <td>
                                                    <input type="text" class="form-control">
                                                </td>
                                                <td>
                                                    <button class="btn btn-success btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit"><span class="glyphicon glyphicon-pencil"></span></button>
                                                    <button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete"><span class="glyphicon glyphicon-trash"></span></button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>Fetal Echo</td>
                                                <td><input type="number" min="0" class="form-control"></td>
                                                <td><input type="text" class="form-control"></td>
                                                <td><input type="text" class="form-control"></td>
                                                <td><input type="text" class="form-control"></td>
                                                <td>
                                                    <input type="text" class="form-control">
                                                </td>

                                                <td>
                                                    <input type="text" class="form-control">
                                                </td>
                                                <td>
                                                    <button class="btn btn-success btn-xs"><span class="glyphicon glyphicon-pencil"></span></button>
                                                    <button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-12 text-right">
                                    <button type="submit" name="submit" class="btn btn-success">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>