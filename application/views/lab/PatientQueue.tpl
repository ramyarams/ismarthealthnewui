﻿        <div class="content-wrapper">
            <section class="content-header">
                <div class="login-logo">
                    <span style="color: #000;"><b>Patient</b> List</span>
                </div>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li>Patient List</li>
                </ol>
            </section>
            <section class="content">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Referal ID</th>
                                        <th>Name</th>
                                        <th>Phone Number</th>
                                        <th>Doctor</th>
                                        <th>Refer Date</th>
                                        <th class="no-sort" width="5%">Billing</th>
                                        <th class="no-sort" width="5%">Samples</th>
                                        <th class="no-sort" width="5%">Reports</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>AP001</td>
                                        <td>Rakesh Kumar Pattnaik</td>
                                        <td>9036809357</td>
                                        <td>Dr. Srinibas Rao</td>
                                        <td>05-11-2018</td>
                                        <td style="text-align:center;"><a href="<?=base_url(); ?>Lab/Billing"><img type="image" src="../Common_1/Images/Billing.png" alt="Submit"></a></td>
                                        <td style="text-align:center;"><a href="<?=base_url(); ?>Lab/Samples"><img type="image" src="../Common_1/Images/Samples.png" alt="Submit"></a></td>
                                        <td style="text-align:center;"><a href="<?=base_url(); ?>Lab/Report"><img type="image" src="../Common_1/Images/Reports.png" alt="Submit"></a></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <p>
                            Previous site visit:
                            <strong>09-11-2018 10:20:38</strong>
                        </p>
                    </div>
                </div>
            </section>
        </div>
       
    <script type="text/javascript">
        $(document).ready(function () {
            $('#example1').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': true,
                'ordering': true,
                'info': true,
                'autoWidth': false,
                'lengthMenu': [[5, 10, 15, 25, 50, 100, -1], [5, 10, 15, 25, 50, 100, "All"]],
                'pageLength': 5,
                columnDefs: [{
                    orderable: false,
                    targets: "no-sort"
                }]
            });
        });
    </script>
</body>
</html>