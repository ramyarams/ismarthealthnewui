<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<?=base_url(); ?>Common/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?=base_url(); ?>Common/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=base_url(); ?>Common/css/style.css" rel="stylesheet">
    <link rel="shortcut icon" href="<?=base_url(); ?>Common_1/images/Favicon.ico">
    <style>
        .image {
            transform: rotate(90deg);
            -webkit-transform: rotate(90deg);
            left: 100% !important;
            top: 50% !important;
            bottom: 50%;
        }

        section#product {
            padding-bottom: 0px;
        }

        @media screen and (min-width:400px) {
            section#product {
                margin-top: -9% !important;
            }
        }
    </style>
</head>
<body>

    <header id="home">
        <div class="main-menu">
            <div class="navbar-wrapper">
                <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="sr-only">Toggle Navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a href="#" class="navbar-brand"><img src="<?=base_url(); ?>Common/img/logo.png" alt="Logo" style="margin-top:-20px; height:50px;" /></a>
                        </div>
                        <div>
                        </div>
                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="<?=base_url(); ?>Login" style="color: #5c64d6!important;font-family: arial;">Login</a></li>
                                <li><a href="<?=base_url(); ?>Physician/Physician_EULA" style="color: #5c64d6!important;font-family: arial;">New User</a></li>
                                <li><a href="#contact-us" style="color: #5c64d6!important;font-family: arial;">Contact</a></li>
                                <li><a href="#about" style="color: #5c64d6!important;font-family: arial;">About Us</a></li>
                                <li><a href="#clients" style="color: #5c64d6!important;font-family: arial;">Clients</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>