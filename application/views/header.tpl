<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>iSmart Health</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="<?=base_url(); ?>Common_1/Css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url(); ?>Common_1/Css/font-awesome.min.css">
    <link rel="stylesheet" href="<?=base_url(); ?>Common_1/Css/ionicons.min.css">
    <link rel="stylesheet" href="<?=base_url(); ?>Common_1/Css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url(); ?>Common_1/Css/AdminLTE.min.css">
    <link rel="stylesheet" href="<?=base_url(); ?>Common_1/Css/_all-skins.min.css">
    <link rel="shortcut icon" href="<?=base_url(); ?>Common_1/images/Favicon.ico" />
    <link rel="stylesheet" href="<?=base_url(); ?>Common_1/Css/fullcalendar.min.css">
    <link rel="stylesheet" href="<?=base_url(); ?>Common_1/Css/fullcalendar.print.min.css" media="print">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <style>
        @media screen and (max-width: 992px) {
            .main-header .sidebar-toggle {
                float: left;
                background-color: transparent;
                background-image: none;
                padding: 15px 15px;
                font-family: fontAwesome;
            }
        }

        .skin-blue .sidebar-menu > li.header {
            color: #fff;
            background: #004a75;
            font-size: 14px;
        }

        .skin-blue .sidebar a {
            color: #3f51ae;
        }

        .skin-blue .sidebar-menu > li > a:hover {
            background-color: #3c8dbc !important;
        }

        .skin-blue .sidebar-menu .treeview-menu > li > a {
            color: #3f51ae;
        }

        .skin-blue .sidebar-menu > li > .treeview-menu {
            margin: 0 1px;
            background: #fff;
        }

        .skin-blue .sidebar-menu > li.menu-open > a {
            color: #fff;
            background: #3c8dbc;
        }

        .skin-blue .sidebar-menu > li > .treeview-menu > li > a:hover {
            background-color: #3c8dbc !important;
        }

        .image-upload > input {
            display: none;
        }

        .image-upload img {
            width: 80px;
            cursor: pointer;
        }
		 .no-sort::after {
            display: none !important;
        }

        .no-sort {
            pointer-events: none !important;
            cursor: default !important;
        }

        table#example1 {
            font-size: 13px !important;
        }

        .table > tbody > tr > td {
            padding: 8px;
        }

        .btn:focus, .btn:active, button:focus, button:active {
            outline: none !important;
            box-shadow: none !important;
        }

        #image-gallery .modal-footer {
            display: block;
        }

        .thumb {
            margin-top: 15px;
            margin-bottom: 15px;
        }

        .downloadbtn {
            margin-top: -12%;
            float: right;
            margin-right: -2%;
        }
             .dual-list .list-group {
            margin-top: 8px;
        }

        .list-left li, .list-right li {
            cursor: pointer;
        }

        .list-arrows {
            padding-top: 100px;
        }

            .list-arrows button {
                margin-bottom: 20px;
            }

        .list-group-item {
            border-radius: 0px !important;
        }
    </style>
</head>
<body class="hold-transition skin-blue fixed sidebar-mini">
    <div class="wrapper">
        <header class="main-header">
            <a href="#" class="logo" style="border-bottom: 1px solid #e5e9f1;">
                <span class="logo-mini" style="margin-top: 6px;"><b>i</b>SH</span>
                <span class="logo-lg"><img src="<?=base_url(); ?>Common_1/Images/logo.png" alt="Logo"></span>
            </a>
            <nav class="navbar navbar-static-top">
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <li class="dropdown user user-menu" style="top: 5px;">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="<?=base_url(); ?>Common_1/Images/Subrat.png" class="user-image" alt="User Image">
                                <span class="hidden-xs">Dr. Subrat Pal</span>
                            </a>
                            <ul class="dropdown-menu" style="width: 200px;">
                                <li class="user-header">
                                    <img src="<?=base_url(); ?>Common_1/Images/Subrat.png" class="img-circle" alt="User Image">
                                    <p>
                                        Dr. Subrat Pal
                                        <small>General Physian</small>
                                    </p>
                                </li>
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="<?=base_url(); ?>Physician/Physician_Home" class="btn btn-success btn-flat">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="<?=base_url(); ?>Logout" class="btn btn-danger btn-flat">Logout</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>