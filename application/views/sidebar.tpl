		<aside class="main-sidebar" style="background-color: #f9fafc;">
            <section class="sidebar">
                <div class="user-panel">
                    <div class="pull-left image">
                        <img src="<?=base_url(); ?>Common_1/Images/Subrat.png" class="img-circle" alt="User Image">
                    </div>
                    <div class="pull-left info" style="color: #3F51B5;">
                        <p>Dr. Subrat Pal</p><small>General Physician</small>
                    </div>
                </div>
                <ul class="sidebar-menu" data-widget="tree">
                    <li class="header">Main Navigation</li>

                     <?php if(isset($_SESSION['mail_id']) && ($_SESSION['role_id']==1)){ ?>
                    <li><a href="<?=base_url(); ?>Physician/PatientSearch"><i class="fa fa-th"></i> <span>Patient Look-Up</span></a></li>
                    <li><a href="<?=base_url(); ?>Physician/Speciality_History"><i class="fa fa-th"></i> <span>Previous Super Speciality <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Consultation</span></a></li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-th"></i><span>Quick Links</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?=base_url(); ?>Physician/LabInvestigation_History"><i class="fa fa-circle-o"></i> Previous Lab Investigation</a></li>
                            <li><a href="<?=base_url(); ?>Physician/PhysicianTiming"><i class="fa fa-circle-o"></i> Physician Timings</a></li>
                            <li><a href="<?=base_url(); ?>Physician/PhysicianCalendar"><i class="fa fa-circle-o"></i> Physician Calendar</a></li>
                            <li><a href="<?=base_url(); ?>Physician/AdminRegister"><i class="fa fa-circle-o"></i> Add Admin</a></li>
                            <li><a href="<?=base_url(); ?>Physician/ChangePassword"><i class="fa fa-circle-o"></i> Change Password</a></li>
                            <li><a href="#"><i class="fa fa-circle-o"></i> Tech Support</a></li>
                        </ul>
                    </li>
                     <?php } ?>

                     <?php if(isset($_SESSION['mail_id']) && ($_SESSION['role_id']==2)){ ?>
                    <li><a href="<?=base_url(); ?>Lab/PatientQueue"><i class="fa fa-th"></i> <span>Patient Queue</span></a></li>
                    <li><a href="<?=base_url(); ?>Lab/AddPatient"><i class="fa fa-th"></i> <span>Add Patients & Refer</span></a></li>
                    <li><a href="<?=base_url(); ?>Lab/PackageQueue"><i class="fa fa-th"></i> <span>Package Queue</span></a></li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-th"></i><span>Quick Links</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                     <li><a href="<?=base_url(); ?>Lab/EnhanceLab"><i class="fa fa-circle-o"></i> Enhance Test Capabilities</a></li>
                     <li><a href="<?=base_url(); ?>Lab/UpdatePrice"><i class="fa fa-circle-o"></i> Update Tests Price</a></li>
                     <li><a href="<?=base_url(); ?>Lab/LabTiming"><i class="fa fa-circle-o"></i> Lab timings</a></li>
                     <li><a href="<?=base_url(); ?>Lab/LabCalender"><i class="fa fa-circle-o"></i> Lab calendar</a></li>
                     <li><a href="<?=base_url(); ?>Lab/AvailablePackage"><i class="fa fa-circle-o"></i> Package</a></li>
                     <li><a href="<?=base_url(); ?>Lab/ChangePassword"><i class="fa fa-circle-o"></i> Change Password</a></li>
                     <li><a href="#"><i class="fa fa-th"></i> Tech Support</a></li>
                     <li class="treeview">
                        <a href="#">
                            <i class="fa fa-circle-o"></i> Others
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                             </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="#"><i class="fa fa-circle-o"></i> Invoice EULA</a></li>
                            <li><a href="<?=base_url(); ?>Lab/Read_EULA"><i class="fa fa-circle-o"></i> Read EULA</a></li>
                        </ul>
                    </li>
                     <?php } ?>
                     
                    <?php if(isset($_SESSION['mail_id']) && ($_SESSION['role_id']==3)){ ?>
                    <li><a href="<?=base_url(); ?>Patient/MyHistory"><i class="fa fa-th"></i> <span>View Lab Records</span></a></li>
                    <li><a href="<?=base_url(); ?>Patient/Patient_Home"><i class="fa fa-th"></i> <span>My Profile</span></a></li>
                    <li><a href="<?=base_url(); ?>Patient/ChangePassword"><i class="fa fa-th"></i> <span>Change Password</span></a></li>
                    <li><a href="#"><i class="fa fa-th"></i><span>Tech Support</span></a></li>
                     <?php } ?>
                </ul>
            </section>
        </aside>