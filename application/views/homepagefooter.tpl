	<div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    Copyright 2018 © <strong><a href="#">iSmartHealth</a></strong> All rights reserved.
                </div>
                <div class="col-md-6" style="text-align:right;">
                    <span class="glyphicon glyphicon-phone" aria-hidden="true"></span>&nbsp;&nbsp;+91 7618702727&nbsp;&nbsp;&nbsp;&nbsp;
                    <span class="glyphicon glyphicon-phone-alt" aria-hidden="true"></span>&nbsp;&nbsp;080 26971320&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="mailto:sales@inetframe.com?subject=iSmartClinic.in"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>&nbsp;&nbsp;sales@inetframe.com</a>
                </div>
            </div>
        </div>
    </div>
    <script src="<?=base_url(); ?>Common/js/jquery.min.js"></script>
    <script src="<?=base_url(); ?>Common/js/bootstrap.min.js"></script>
    <script>
        if (window.history.replaceState) {
            window.history.replaceState(null, null, window.location.href);
        }
    </script>
</body>
</html>