<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>iSmartHealth | Log in</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="<?=base_url(); ?>Common_1/Css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url(); ?>Common_1/Css/font-awesome.min.css">
    <link rel="stylesheet" href="<?=base_url(); ?>Common_1/Css/ionicons.min.css">
    <link rel="stylesheet" href="<?=base_url(); ?>Common_1/Css/AdminLTE.min.css">
    <link rel="stylesheet" href="<?=base_url(); ?>Common_1/Css/blue.css">
    <link rel="shortcut icon" href="<?=base_url(); ?>Common_1/images/Favicon.ico" />
    <link rel="stylesheet" href="<?=base_url(); ?>Common_1/Css/FontFamily.css">
    <style>
        .required label:before {
            content: "* ";
            color: red;
        }
    </style>
</head>
<body class="hold-transition login-page" style="background-color:#fff;overflow-y: hidden; height:auto;">
    <div class="login-box">
        <div class="login-box-body" style="border: 1px solid #3c8dbc;box-shadow: 2px 2px #3c8dbc;">
            <div class="login-logo">
               <a href="<?=base_url(); ?>"> <img src="<?=base_url(); ?>Common/img/logo.png" alt="Logo" style="width:175px; height:50px;" /></a>
            </div>
            <hr style="margin-top: 2px;" />
            <form action="<?=base_url(); ?>Login" method="post" autocomplete="off">
                <div class="form-group has-feedback required">
                    <label for="inputName" class="control-label">Email Address</label>
                    <input type="email" class="form-control" placeholder="Email" name="username" required>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback required">
                    <label for="inputName" class="control-label">Password</label>
                    <input type="password" class="form-control" placeholder="Password" name="password" required autocomplete="new-password">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="row">
                    <div class="col-xs-8">
                        <div class="checkbox icheck">
                            <label>
                                <input type="checkbox"> Remember Me
                            </label>
                        </div>
                    </div>
                </div>
                <hr style="margin-top: 2px;" />
                <div class="row">
                    <div class="col-xs-12" style="text-align: right;">
                        <button type="submit" class="btn btn-success" name="submits">Login</button>
                    </div>
                </div>
            </form>
            <p style="font-size:8pt; text-align:center; margin-top:15%;margin-bottom: -10px!important;">By logging in, you agree to our <a href="TermsandCond.html" target="_blank">Terms and Condition</a></p>
        </div>
    </div>
    <script src="<?=base_url(); ?>Common_1/Js/jquery.min.js"></script>
    <script src="<?=base_url(); ?>Common_1/Js/bootstrap.min.js"></script>
    <script src="<?=base_url(); ?>Common_1/Js/icheck.min.js"></script>
    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%'
            });
        });
        history.pushState(null, null, location.href);
        window.onpopstate = function () {
            history.go(1);
        };
    </script>
</body>
</html>
