<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>iSmartHealth | Log in</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="<?=base_url(); ?>Common_1/Css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url(); ?>Common_1/Css/font-awesome.min.css">
    <link rel="stylesheet" href="<?=base_url(); ?>Common_1/Css/ionicons.min.css">
    <link rel="stylesheet" href="<?=base_url(); ?>Common_1/Css/AdminLTE.min.css">
    <link rel="stylesheet" href="<?=base_url(); ?>Common_1/Css/blue.css">
    <link rel="shortcut icon" href="<?=base_url(); ?>Common_1/images/Favicon.ico" />
    <link rel="stylesheet" href="<?=base_url(); ?>Common_1/Css/FontFamily.css">
    <style>
        .required label:before {
            content: "* ";
            color: red;
        }
    </style>
</head>
<body class="hold-transition" style="background-color:#fff;height:auto; font-size: 13px!important;">
    <div class="register-box" style="margin: 5% auto!important;">
        <div class="register-box-body" style="border: 1px solid #3c8dbc;box-shadow: 2px 2px #3c8dbc;">
            <div class="register-logo" style="margin-bottom: 0px!important;">
                <img src="<?=base_url(); ?>Common/img/logo.png" alt="Logo" style="width:175px; height:50px;" />
                <p style="font-size:25px; margin-top:10px;"><b>Doctor</b> Registraion</p>
            </div>
            <hr style="margin-top: 2px;" />
            <div class="row">
                <div class="col-md-4 form-group required">
                    <label>First Name</label>
                    <input type="text" id="txtFirstName" class="form-control" required autofocus>
                </div>
                <div class="col-md-4 form-group">
                    <label>Last Name</label>
                    <input type="text" id="txtLastName" class="form-control" required autofocus>
                </div>
                <div class="col-md-4 form-group required">
                    <label>Phone</label>
                    <input type="tel" id="txtPhone" class="form-control" required autofocus>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 form-group required">
                    <label>Email Address</label>
                    <input type="email" id="txtEmail" class="form-control" required autofocus>
                </div>
                <div class="col-md-4 form-group required">
                    <label>Specilization</label>
                    <input type="text" id="inputEmail" class="form-control" required autofocus>
                </div>
                <div class="col-md-4 form-group">
                    <label>Clinic Name</label>
                    <input type="text" id="txtClinicName" class="form-control" required autofocus>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 form-group">
                    <label>Clinic Phone</label>
                    <input type="text" id="txtClinicPhone" onblur="checkLength(this)" maxlength="10" name="txtClinicPhone" class="form-control" required autofocus>
                </div>
                <div class="col-md-4 form-group required">
                    <label>State</label>
                    <div class="form-check">
                        <select class="form-control" id="ddlState" name="ddlState">
                            <option value="0">Select</option>
                            <option value="1">Karnataka</option>
                            <option value="2">Tamilnadu</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4 form-group required">
                    <label>City</label>
                    <div class="form-check">
                        <select class="form-control" id="ddlCity" name="ddlCity">
                            <option value="0">Select</option>
                            <option value="1">Bangalore</option>
                            <option value="2">Chennai</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 form-group">
                    <label>Pin Code</label>
                    <input type="text" id="txtPostalCode" class="form-control" required autofocus>
                </div>
                <div class="col-md-4 form-group">
                    <label>Land Mark</label>
                    <input type="text" id="txtLandMark" class="form-control" required autofocus>
                </div>
                <div class="col-md-4 form-group">
                    <label>Address</label>
                    <input type="text" id="txtAddress" class="form-control" required autofocus>
                </div>
            </div>
            <hr style="margin-top: 2px;" />
            <div class="row">
                <div class="col-xs-5" style="text-align: left;">
                    <a href="<?=base_url(); ?>Login" id="link"><img type="image" src="<?=base_url(); ?>Common_1/Images/Back.png" alt="Submit"></a>
                </div>
                <div class="col-xs-7" style="text-align: right;">
                    <button type="submit" class="btn btn-success">Submit</button>
                    <button type="submit" class="btn btn-danger">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <script src="<?=base_url(); ?>Common_1/Js/jquery.min.js"></script>
    <script src="<?=base_url(); ?>Common_1/Js/bootstrap.min.js"></script>
    <script src="<?=base_url(); ?>Common_1/Js/icheck.min.js"></script>
    <script>
        $(document).ready(function () {
            $("#txtClinicPhone").keypress(function (e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    alert("Please Enter Only Numbers For Phone!");
                    return false;
                }
            });
        });
        function checkLength(el) {
            if (el.value.length != 10) {
                alert("length must be exactly 10 characters")
            }
        }
    </script>
</body>
</html>