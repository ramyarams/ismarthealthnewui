<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Physician extends CI_Controller 
{

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        //is_logged_in();
        $this->load->helper ('url');
        $this->load->model('physician_model');
        $this->load->model('Physician_appointment_m');
    }
	public function index()
	{
		
	}
	
	function Physician_EULA()
	{
		$this->load->view('authentication/phy_eula.tpl');
	}
	
	function Physician_Register()
	{
		$this->load->view('authentication/PhysicianRegistration.tpl');
	}

	function PatientSearch()
	{
		$this->load->view('header.tpl');
		$this->load->view('sidebar.tpl');
		$this->load->view('physician/PatientSearch.tpl');
		$this->load->view('footer.tpl');
	}
	
	function Speciality_History()
	{
		$this->load->view('header.tpl');
		$this->load->view('sidebar.tpl');
		$this->load->view('physician/PrevSupSpecCons.tpl');
		$this->load->view('footer.tpl');
	}
	
	function LabInvestigation_History()
	{
		$this->load->view('header.tpl');
		$this->load->view('sidebar.tpl');
		$this->load->view('physician/PreLabInvest.tpl');
		$this->load->view('footer.tpl');
	}
	
	function ChangePassword()
	{
		$this->load->view('header.tpl');
		$this->load->view('sidebar.tpl');
		$this->load->view('physician/ChangePassword.tpl');
		$this->load->view('footer.tpl');
	}

	function PhysicianTiming()
	{
		$this->load->view('header.tpl');
		$this->load->view('sidebar.tpl');
		$this->load->view('physician/PhysicianTiming.tpl');
		$this->load->view('footer.tpl');
	}
	
	function PhysicianCalendar()
	{
		$this->load->view('header.tpl');
		$this->load->view('sidebar.tpl');
		$this->load->view('physician/PhysicianCalendar.tpl');
		$this->load->view('footer.tpl');
	}
	
	function AdminRegister()
	{
		$this->load->view('header.tpl');
		$this->load->view('sidebar.tpl');
		$this->load->view('physician/AdminRegister.tpl');
		$this->load->view('footer.tpl');
	}

		function Previous_Investigation()
	{
		$this->load->view('header.tpl');
		$this->load->view('sidebar.tpl');
		$this->load->view('physician/PrevInvestigation.tpl');
		$this->load->view('footer.tpl');
	}
	
	function ViewTestReports()
	{
		$this->load->view('header.tpl');
		$this->load->view('sidebar.tpl');
		$this->load->view('physician/ViewTestReports.tpl');
		$this->load->view('footer.tpl');
	}

		function PatientParameter()
	{
		$this->load->view('header.tpl');
		$this->load->view('sidebar.tpl');
		$this->load->view('physician/PatientParameter.tpl');
		$this->load->view('footer.tpl');
	}

			function LabDetails()
	{
		$this->load->view('header.tpl');
		$this->load->view('sidebar.tpl');
		$this->load->view('physician/LabDetails.tpl');
		$this->load->view('footer.tpl');
	}

    function PatientDependent()
	{
		$this->load->view('header.tpl');
		$this->load->view('sidebar.tpl');
		$this->load->view('physician/PatientDependent.tpl');
		$this->load->view('footer.tpl');
	}

	
	 function Physician_Home()
	{
		$this->load->view('header.tpl');
		$this->load->view('sidebar.tpl');
		$this->load->view('physician/Home.tpl');
		$this->load->view('footer.tpl');
	}
	
		 function DependentParameter()
	{
		$this->load->view('header.tpl');
		$this->load->view('sidebar.tpl');
		$this->load->view('physician/DependentParameter.tpl');
		$this->load->view('footer.tpl');
	}
}
