	<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Login extends CI_Controller 
	{

		function __construct()
		{
			parent::__construct();
			$this->load->library('session');
			$this->load->helper ('url');
			$this->load->model('Physician_appointment_m');

		}
		public $loginvar=false;
		
	public function index()
	{
			if (isset($_POST["submits"])) 
		{			
			$username=$this->input->post("username",true);
			$password=$this->input->post("password",true);
			$pass=md5($password);

			$login=$this->Physician_appointment_m->login_check($username,$pass);
			if($login!=="no content")
			{
			$this->load->library('session');
			$this->session->set_userdata(array('login_id'=>$login[0]->login_id,'firstname'=>$login[0]->firstname,'count'=>$login[0]->count,'physician_id'=>$login[0]->physician_id,'laboratory_id'=>$login[0]->laboratory_id,'username'=>$login[0]->username,'mail_id'=>$login[0]->username,'role_id'=>$login[0]->role_id,'patient_id'=>$login[0]->patient_id,'state'=>$login[0]->state,'city'=>$login[0]->city,'last_login'=>$login[0]->last_login,'phone_num'=>$login[0]->phone_num));
			$this->loginvar=true;
			
				if ($this->session->userdata('role_id') == 1 && $this->session->userdata('count') == 0)
				{
					redirect('../Physician/ChangePassword','refresh');
				}
				elseif($this->session->userdata('role_id') == 1)
				{
					redirect('../Physician/Physician_Home/', 'refresh');
				}
				elseif ($this->session->userdata('role_id') == 2 && $this->session->userdata('count') == 0){
					redirect('../Physician/ChangePassword','refresh');
				}
				elseif ($this->session->userdata('role_id') == 2){
					redirect('../Lab/Lab_Home/', 'refresh');
				}
				elseif ($this->session->userdata('role_id') == 3 && $this->session->userdata('count') == 0){
					redirect('../Patient/ChangePassword','refresh');
				}
				elseif ($this->session->userdata('role_id') == 3){
					redirect('../Patient/Patient_Home/', 'refresh');
				}
				else{
					redirect('../Login/', 'refresh');
				}
			}
			else
			{
				$this->loginvar=false;
				echo " <script language='javascript'>
					alert('User Name and Password didn\'t match. Please try again');
					window.location.href='Login';
						</script>";
			}
		}
			$this->load->view('authentication/login.tpl');
	}
}
