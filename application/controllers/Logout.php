<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends CI_Controller {

	public function index()
	{
         $this->load->library('session');
         $this->session->unset_userdata('mail_id');
         $this->session->sess_destroy();
         //header('location:../Home');
         redirect(base_url());
	}
}
