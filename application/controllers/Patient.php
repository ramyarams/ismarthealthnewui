<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Patient extends CI_Controller 
{

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        //is_logged_in();
        $this->load->helper ('url');
        $this->load->model('physician_model');
        $this->load->model('Physician_appointment_m');
    }
	public function index()
	{
		
	}
	
	function Patient_EULA()
	{
		$this->load->view('authentication/phy_eula.tpl');
	}
	
	function Patient_Register()
	{
		$this->load->view('authentication/PatientRegistration.tpl');
	}

	function Patient_Home()
	{
		$this->load->view('header.tpl');
		$this->load->view('sidebar.tpl');
		$this->load->view('patient/patient_home.tpl');
		$this->load->view('footer.tpl');
	}
	
	function MyHistory()
	{
		$this->load->view('header.tpl');
		$this->load->view('sidebar.tpl');
		$this->load->view('patient/MyHistory.tpl');
		$this->load->view('footer.tpl');
	}	
	
	function ViewTestReports()
	{
		$this->load->view('header.tpl');
		$this->load->view('sidebar.tpl');
		$this->load->view('patient/ViewTestReports.tpl');
		$this->load->view('footer.tpl');
	}
	
	function ChangePassword()
	{
		$this->load->view('header.tpl');
		$this->load->view('sidebar.tpl');
		$this->load->view('patient/ChangePassword.tpl');
		$this->load->view('footer.tpl');
	}
}
