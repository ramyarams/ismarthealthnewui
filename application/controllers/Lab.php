<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lab extends CI_Controller 
{

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        //is_logged_in();
        $this->load->helper ('url');
        $this->load->model('physician_model');
        $this->load->model('Physician_appointment_m');
    }
	public function index()
	{
		
	}

	// Laboratory Home Page
	function Lab_Home()
	{
		$this->load->view('header.tpl');
		$this->load->view('sidebar.tpl');
		$this->load->view('lab/lab_home.tpl');
		$this->load->view('footer.tpl');
    }
	
	
	// Enhance Test Capabilities 
    function EnhanceLab()
	{
        $this->load->view('header.tpl');
		$this->load->view('sidebar.tpl');
		$this->load->view('lab/EnhanceLab.tpl');
		$this->load->view('footer.tpl');
    }

	// Function to update the tests price
     function UpdatePrice()
	{
        $this->load->view('header.tpl');
		$this->load->view('sidebar.tpl');
		$this->load->view('lab/UpdatePrice.tpl');
		$this->load->view('footer.tpl');
    }

	// Function to change the Lab Availability Timings
    function LabTiming()
	{
        $this->load->view('header.tpl');
		$this->load->view('sidebar.tpl');
		$this->load->view('lab/LabTiming.tpl');
		$this->load->view('footer.tpl');
	}
	
	//Function for Adhoc Calender
    function LabCalender()
	{
        $this->load->view('header.tpl');
		$this->load->view('sidebar.tpl');
		$this->load->view('lab/LabCalender.tpl');
		$this->load->view('footer.tpl');
    }

	// Function to add packages which are supported by lab
    function AvailablePackage()
	{
        $this->load->view('header.tpl');
		$this->load->view('sidebar.tpl');
		$this->load->view('lab/AvailablePackage.tpl');
		$this->load->view('footer.tpl');
	}

	// To Change Password
    function ChangePassword()
	{
        $this->load->view('header.tpl');
		$this->load->view('sidebar.tpl');
		$this->load->view('lab/ChangePassword.tpl');
		$this->load->view('footer.tpl');
	}
	
	// Functionality for Lab Adding Patient 
    function AddPatient()
	{
        $this->load->view('header.tpl');
		$this->load->view('sidebar.tpl');
		$this->load->view('lab/AddPatient.tpl');
		$this->load->view('footer.tpl');
    }

	// Patient list in lab referred for tests
      function PatientQueue()
	{
        $this->load->view('header.tpl');
		$this->load->view('sidebar.tpl');
		$this->load->view('lab/PatientQueue.tpl');
		$this->load->view('footer.tpl');
	}
	
	// Patient list referred for package
	   function PackageQueue()
	{
        $this->load->view('header.tpl');
		$this->load->view('sidebar.tpl');
		$this->load->view('lab/PackageQueue.tpl');
		$this->load->view('footer.tpl');
	}
	
	// Billing Functionality
     function Billing()
	{
        $this->load->view('header.tpl');
		$this->load->view('sidebar.tpl');
		$this->load->view('lab/Billing.tpl');
		$this->load->view('footer.tpl');
	}
	
	// EULA
     function Read_EULA()
	{
        $this->load->view('header.tpl');
		$this->load->view('sidebar.tpl');
		$this->load->view('lab/Read_EULA.tpl');
		$this->load->view('footer.tpl');
	}
	
	// Add Samples in Lab    
    function Samples()
	{
        $this->load->view('header.tpl');
		$this->load->view('sidebar.tpl');
		$this->load->view('lab/Samples.tpl');
		$this->load->view('footer.tpl');
	}
	
	// Reports Upload
      function Report()
	{
        $this->load->view('header.tpl');
		$this->load->view('sidebar.tpl');
		$this->load->view('lab/Report.tpl');
		$this->load->view('footer.tpl');
	}

	// To View Test Reports
	 function ViewReport()
	{
        $this->load->view('header.tpl');
		$this->load->view('sidebar.tpl');
		$this->load->view('lab/ViewReport.tpl');
		$this->load->view('footer.tpl');
	}
	
	 function DownloadReport()
	{
        $this->load->view('header.tpl');
		$this->load->view('sidebar.tpl');
		$this->load->view('lab/DownloadReport.tpl');
		$this->load->view('footer.tpl');
    }
}
