<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller 
{

	public function index()
	{
		$this->session->unset_userdata('mail_id');
		$this->session->sess_destroy();
		$this->load->view('homepageheader.tpl');
		$this->load->view('home.tpl');
		$this->load->view('homepagefooter.tpl');
	}
	
}
