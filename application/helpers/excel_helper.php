<?php

function create_excel_export()
    {
        $ci = & get_instance();
        $ci->load->model("users_model");
        $users = $ci->users_model->find_users();

        $headers = array("Employee Name", "Employee Email", "Department");

        $title = "";
        $data = "";
        $filename = 'Users';
        foreach($headers as $value) {
                $title .= $value . "\t";
        }
        $headers = trim($title). "\n";

        if (!empty($users)){
            foreach ($users as $row){
                $line = '';
                $employee_name = $row['full_name'];
                $email = $row['email'];
                $dept_name = $row['dept_name'];

                $line .= $employee_name . "\t";
                $line .= $email . "\t";
                $line .= $dept_name . "\t";

                $data .= trim($line). "\n";
            }
        }

        $export = array(
            'filename' => $filename,
            'headers' => $headers,
            'data' => $data
        );

        return $export;
    } 


    ?>