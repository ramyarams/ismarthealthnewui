﻿function DisableRightClick(event) {
    if (event.button == 2) {
        alert("Right Clicking not allowed!");
    }
}
function DisableCtrlKey(e) {
    var code = (document.all) ? event.keyCode : e.which;
    var message = "Ctrl key functionality disabled!";
    var message1 = "Backspace functionality is disabled!";
    var message2 = "Delete functionality is disabled!";
    var message3 = "Spacebar functionality is disabled!";
    var message4 = "Arrow keys has been disabled!";
    if (parseInt(code) == 17) {
        alert(message);
        window.event.returnValue = false;
    }
    if (parseInt(code) == 8) {
        alert(message1);
        window.event.returnValue = false;
    }
    if (parseInt(code) == 46) {
        alert(message2);
        window.event.returnValue = false;
    }
    if (parseInt(code) == 32) {
        alert(message3);
        window.event.returnValue = false;
    }
    if (parseInt(code) == 37) {
        alert(message4);
        window.event.returnValue = false;
    }
    if (parseInt(code) == 38) {
        alert(message4);
        window.event.returnValue = false;
    }
    if (parseInt(code) == 39) {
        alert(message4);
        window.event.returnValue = false;
    }
    if (parseInt(code) == 40) {
        alert(message4);
        window.event.returnValue = false;
    }
}
function DisableAllKey(e) {
    alert("key functionality has been disabled, please select from calander");
    window.event.returnValue = false;
}
if (window.history.replaceState) {
    window.history.replaceState(null, null, window.location.href);
}